/*!
 * sdkby426bt.js 1.0.0
 * Author: fewensa
 */
const cmder = require('../sdk-api/cmdapi');
const toolkit = require('../lib/toolkit');
const asseter = require('../lib/asseter');
const pako = require('pako');
const is = require('is_js');

const VERSION = '1.4.2-dev';

const FAMILY = 24;
const DEFAULT_FAMILY = 55;

let _env = {
  offset_x: 0,
  offset_y: 0,
  unit: 'px'
};

function toPx(val) {
  switch (_env.unit) {
    case 'px':
      return val;
    case 'mm':
      return val * 8;
    case 'cm':
      return val * 80;
    default:
      return val;
  }
}

function calcX(x) {
  return Math.round(toPx(_env.offset_x + toolkit.def(x, 0)));
}

function calcY(y) {
  return Math.round(toPx(_env.offset_y + toolkit.def(y, 0)));
}

function clear() {
  _env = {offset_x: 0, offset_y: 0, unit: 'px'};
  cmder.clear();
  return this;
}

function safe_arg(arg) {
  if (arg === undefined)
    return {};
  let type = toolkit.typeof(arg);
  if (type !== 'object')
    throw TypeError('Only support object arguments');
  return arg;
}


/**
 * 环境设定, 相对座标, 会为后续的所有座标点加上相同的值
 * @param {Object} arg  {
 * offset_x: x 座标统一偏移值
 * offset_y: y 座标统一偏移值
 * unit: 统一量值单位
 * }
 */
function env({unit = 'px', x = 0, y = 0}) {
  let arg_unit = toolkit.def(unit, 'px').toUpperCase();
  switch (arg_unit) {
    case 'PX':
    case 1:
      unit = 'px';
      break;
    case 'MM':
    case 2:
      unit = 'mm';
      break;
    case 'CM':
    case 3:
      unit = 'cm';
      break;
    default:
      unit = 'px';
      break;
  }
  _env = {
    offset_x: toolkit.def(x, 0),
    offset_y: toolkit.def(y, 0),
    unit
  };
  return this;
}

function wakeup() {
  cmder.push([0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
  return this;
}

/**
 * 页大小
 * @param width 宽度 default 100
 * @param height 高度 default 175
 * @returns {page}
 */
function page({width = 100, height = 175}) {
  cmder.push(`SIZE ${width} mm,${height} mm`);
  return this;
}

/**
 * 打印方向
 * @param x x default 0
 * @param y y default 0
 */
function direction({x = 0, y = 0}) {
  cmder.push(`DIRECTION ${x},${y}`);
  return this;
}

/**
 * 始能切刀, 没打完一张自动切纸
 * @param condition 是否始能切刀
 * @returns {cut}
 */
function cut({condition = true}) {
  const command = condition ?
    'SET CUTTER 1' :
    'SET CUTTER OFF';
  cmder.push(command);
  return this;
}

/**
 * 设置缝隙定位
 * @param m The gap distance between two labels
 * 0 ≤ m ≤1 (inch), 0 ≤ m ≤ 25.4 (mm)
 * 0 ≤ m ≤5 (inch), 0 ≤ m ≤ 127 (mm) / since V6.21 EZ and later firmware
 * @param n The offset distance of the gap
 * n ≤ label length (inch or mm)
 * @param unit Default: English system (inch), mm: Metric system (mm), dot: Dot measurement This command has been supported since V6.27 EZ and later firmware.
 * @param condition
 */
function gap({m = 0, n = 0, unit}) {
  const _unit = unit ? (' ' + unit) : '';
  cmder.push(`GAP ${m}${m > 0 ? _unit : ''},${n}${n > 0 ? _unit : ''}`);
  return this;
}

/**
 * 设置速度
 * @param speed
 */
function speed({speed = 6}) {
  cmder.push(`SPEED ${speed}`);
  return this;
}

/**
 * 设置浓度
 * @param density
 */
function density({density = 5}) {
  cmder.push(`DENSITY ${density}`);
  return this;
}

/**
 * 清除页面缓冲区
 * @returns {cls}
 */
function cls() {
  cmder.push('CLS');
  return this;
}

/**
 *This command draws bitmap images (as opposed to BMP graphic files)
 *
 * @param x Specify the x-coordinate
 * @param y Specify the y-coordinate
 * @param width Image width (in bytes)
 * @param height Image height (in dots)
 * @param compress Is compress iamge data
 * @param modeGraphic modes listed below:
 *   0: OVERWRITE
 *   1: OR
 *   2: XOR
 *   if comparess is true, this field is not required
 * @param context bitmap data Bitmap data
 * @param imageDataHandler get pixel image data handler
 * @returns {image}
 */
function image(
  {
    x = 0,
    y = 0,
    compress = true,
    mode = 0,
    canvas,
  }
) {
  asseter.asset(!!canvas, 'canvas can not be null');
  const width = canvas.width;
  const height = canvas.height;
  asseter.asset(!!width, 'width can not be null', width);
  asseter.asset(!!height, 'height can not be null', height);

  const eWidth = Math.floor((width % 8 === 0) ? (width / 8) : (width / 8 + 1));
  const bytes = _imageData(canvas, width, height);

  if (compress) {
    const u8s = toolkit.bytesToUnit8Array(bytes); // new Uint8Array(bytes);
    const img = pako.deflate(u8s);

    const cmd_left = `BITMAP ${x},${y},${eWidth},${height},3,${img.length},`;
    cmder.push(cmd_left, false);
    cmder.push(img, false);
  } else {
    const cmd_left = `BITMAP ${x},${y},${eWidth},${height},${mode},`;
    cmder.push(cmd_left, false);
    cmder.push(bytes, false);
  }
  newline();
  return this;
}

/**
 * @param canvas
 * @param width
 * @param height
 * @private
 */
function _imageData(canvas, width, height) {
  const ctx = canvas.getContext('2d');

  let eWidth = Math.floor((width % 8 === 0) ? (width / 8) : (width / 8 + 1));
  let currentHeight = 0;
  let index = 0;
  const area = height * eWidth;
  const bytes = []; // the bytes length is equals `area`
  for (let b1 = 0; b1 < area; b1++) {
    bytes[b1] = 0;
  }
  const imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);


  while (currentHeight < height) {
    // let rowData = []; // the row data length is image height
    let eightBitIndex = 0;
    for (let x = 0; x < width; x++) {
      eightBitIndex++;
      if (eightBitIndex > 8) {
        eightBitIndex = 1;
        index++;
      }
      const n = 1 << 8 - eightBitIndex;

      // const pixel = ctx.getImageData(x, currentHeight, 1, 1);
      const pixels = somehowProcessTheData(imgData, x, currentHeight, 1, 1);
      const pixel = pixels[0];
      // const rgba = pixel.data;
      const red = pixel[0];
      const green = pixel[1];
      const blue = pixel[2];
      if ((red + green + blue) / 3 < 128) {
        bytes[index] = (bytes[index] | n);
      }
    }
    index = eWidth * (currentHeight + 1);
    currentHeight += 1;
  }
  return bytes;
}

function somehowProcessTheData(imageData, x, y, w, h) {
  var i, j;
  var result = [];
  var r, g, b, a;
  const data = imageData.data;

  for (j = 0; j < h; j++) {
    var idx = (x + (y + j) * imageData.width) * 4;  // get left most byte index for row at y + j
    for (i = 0; i < w; i++) {
      r = data[idx++];
      g = data[idx++];
      b = data[idx++];
      a = data[idx++];
      // do the processing
      result.push([r, g, b, a]);
    }
  }
  return result;
}

/**
 * 打印
 * @param copies 打印份数
 */
function print({copies = 1}) {
  cmder.push(`PRINT 1,${copies}`);
  return this;
}

/**
 * 换行
 * @returns {newline}
 */
function newline() {
  cmder.push('\r\n');
  return this;
}

/**
 * 执行原始指令
 * @param {Object}  arg {
 * command  原始指令
 * newline 是否换行 默认 true
 * }
 * @returns {origin}
 */
function append({newline = true, command}) {
  asseter.asset(!!command, 'command can not be null');
  cmder.push(command, toolkit.def(newline, true));
  return this;
}

/**
 * 文字指令
 * @param x x
 * @param y y
 * @param font 字体
 * Font name
 * 0
 * Monotype CG Triumvirate Bold Condensed, font
 * width and height is stretchable
 * 1 8 x 12 fixed pitch dot font
 * 2 12 x 20 fixed pitch dot font
 * 3 16 x 24 fixed pitch dot font
 * 4 24 x 32 fixed pitch dot font
 * 5 32 x 48 dot fixed pitch font
 * 6 14 x 19 dot fixed pitch font OCR-B
 * 7 21 x 27 dot fixed pitch font OCR-B
 * 8 14 x25 dot fixed pitch font OCR-A
 * ROMAN.TTF Monotye CG Triumvirate Bold Condensed, font
 * width and height proportion is fixed.
 * Following fonts were supported since V6.80 EZ.
 * 1.EFT EPL2 font 1
 * 2.EFT EPL2 font 2
 * 3.EFT EPL2 font 3
 * 4.EFT EPL2 font 4
 * 5.EFT EPL2 font 5
 * A.FNT ZPL2 font A
 * B.FNT ZPL2 font B
 * D.FNT ZPL2 font D
 * E8.FNT ZPL2 font E8
 * F.FNT ZPL2 font F
 * G.FNT ZPL2 font G
 * H8.FNT ZPL2 font H8
 * GS.FNT ZPL2 font GS
 * @param rotation 旋转角度 = 0
 * The rotation angle of text
 * 0 : No rotation
 * 90: degrees, in clockwise direction
 * 180 : degrees, in clockwise direction
 * 270 : degrees, in clockwise direction
 * @param alignment 对齐方向 = 0
 * Optional. Specify the alignment of text. (V6.73 EZ)
 * 0 : Default (Left)
 * 1 : Left
 * 2 : Center
 * 3 : Right
 * @param xMultiplication = 1
 * Horizontal multiplication, up to 10x
 * Available factors: 1~10
 * For "ROMAN.TTF" true type font, this parameter is ignored.
 * For font "0", this parameter is used to specify the width (point) of true type
 * font. 1 point=1/72 inch.
 * @param yMultiplication = 1
 * Vertical multiplication, up to 10x
 * Available factors: 1~10
 * For true type font, this parameter is used to specify the height (point) of
 * true type font. 1 point=1/72 inch.
 * For *.TTF font, x-multiplication and y-multiplication support floating value.
 * (V6.91 EZ)
 * @param content Content
 */
function text(
  {
    x,
    y,
    font = 0,
    rotation = 0,
    alignment = 'B1',
    xMultiplication = 1,
    yMultiplication = 1,
    content,
  }
) {
  asseter.asset(is.number(x), 'x can not be null', x);
  asseter.asset(is.number(y), 'y can not be null', y);
  // TEXT x,y,"font",rotation,x-multiplication,y-multiplication,[alignment,]"content"
  const _content = content.replace('"', '["]');
  cmder.push(`TEXT ${x},${y},"${font}",${rotation},${xMultiplication},${yMultiplication},${alignment},"${_content}"`);
  return this;
}

/**
 * This command draws a bar on the label format.
 * `BAR x,y,width,height`
 * @param x x The upper left corner x-coordinate (in dots)
 * @param y y The upper left corner y-coordinate (in dots)
 * @param width width Bar width (in dots)
 * @param height height Bar height (in dots)
 * @returns {bar}
 */
function bar({x, y, width, height}) {
  asseter.asset(is.number(x), 'x can not be null', x);
  asseter.asset(is.number(y), 'y can not be null', y);
  asseter.asset(is.number(width), 'width can not be null', width);
  asseter.asset(is.number(height), 'height can not be null', height);
  cmder.push(`BAR ${x},${y},${width},${height}`);
  return this;
}

/**
 * This command draws a bar on the label format.
 * `BAR x,y,width,height`
 * @param x x The upper left corner x-coordinate (in dots)
 * @param y y The upper left corner y-coordinate (in dots)
 * @param width width Bar width (in dots)
 * @param height height Bar height (in dots)
 * @returns {bar}
 */
function line({x, y, width, height}) {
  return bar({x, y, width, height});
}

/***
 * This command prints 1D barcodes. The available barcodes are listed below:
 * `BARCODE X,Y,"code type",height,human readable,rotation,narrow,wide,[alignment,]"content "`
 * @param x Specify the x-coordinate bar code on the label
 * @param y Specify the y-coordinate bar code on the label
 * @param codeType
 * | code type
 * | 128 Code 128, switching code subset A, B, C automatically
 * | 128M Code 128, switching code subset A, B, C manually
 * |              Control code A B C
 * |              096 FNC3 FNC3 NONE
 * |              097 FNC2 FNC2 NONE
 * |              098 SHIFT SHIFT NONE
 * |              099 CODE C CODE C NONE
 * |              100 CODE B FNC4 CODE B
 * |              101 FNC4 CODE A CODE A
 * |              102 FNC1 FNC1 FNC1
 * |              103 Start (CODE A)
 * |              104 Start (CODE B)
 * |              105 Start (CODE C)
 * |              Use “!” as a starting character for the control code followed by three control codes. If the start subset
 * |              is not set, the default starting subset is B.
 * | EAN128 Code 128, switching code subset A, B, C automatically
 * | 25 Interleaved 2 of 5
 * | 25C Interleaved 2 of 5 with check digits
 * | 39 Code 39 full ASCII for TSPL2 printers
 * | Code 39 standard for TSPL printers
 * | Auto switch full ASCII and standard code 39 for PLUS models
 * | 39C Code 39 full ASCII with check digit for TSPL2 printers
 * | Code 39 standard with check digit for TSPL printers
 * | Auto switch full ASCII and standard code 39 for PLUS models
 * | 39S Code 39 standard for TSPL2 printers
 * | 93 Code 93
 * | EAN13 EAN 13
 * | EAN13+2 EAN 13 with 2 digits add-on
 * | EAN13+5 EAN 13 with 5 digits add-on
 * | EAN8 EAN 8
 * | EAN8+2 EAN 8 with 2 digits add-on
 * | EAN8+5 EAN 8 with 5 digits add-on
 * | CODA Codabar
 * | POST Postnet
 * | UPCA UPC-A
 * | UPCA+2 UPC-A with 2 digits add-on
 * | UPCA+5 UPC-A with 5 digits add-on
 * | UPCE UPC-E
 * | UPCE+2 UPC-E with 2 digits add-on
 * | UPCE+5 UPC-E with 5 digits add-on
 * | CPOST China post code
 * | MSI MSI code
 * | MSIC MSI with check digit
 * | PLESSEY PLESSEY code
 * | ITF14 ITF 14 code
 * | EAN14 EAN 14 code
 * | 11 Code 11TSC AUTO ID Technology Co., Ltd. 37 Copyright 2013 All Rights Reserved.
 * | TELEPEN Telepen code
 * | TELEPENN Telepen code. Number only
 * | PLANET Planet code
 * | CODE49 Code 49
 * | DPI Deutsche Post Identcode
 * | DPL Deutsche Post Leitcode
 * @param height Bar code height (in dots)
 * @param humanReadable
 * 0: not readable
 * 1: human readable aligns to left
 * 2: human readable aligns to center
 * 3: human readable aligns to right
 * @param rotation
 * 0 : No rotation
 * 90 : Rotate 90 degrees clockwise
 * 180 : Rotate 180 degrees clockwise
 * 270 : Rotate 270 degrees clockwise
 * @param narrow  Width of narrow element (in dots)
 * @param wide Width of wide element (in dots)
 * @param alignment Specify the alignment of barcode
 * 0 : default (Left)
 * 1 : Left
 * 2 : Center
 * 3 : Right
 * @param content content
 * @returns {barcode}
 */
function barcode(
  {
    x,
    y,
    codeType = '128',
    height,
    humanReadable = 0,
    rotation = 0,
    narrow = 1,
    wide = 1,
    alignment = 'B2',
    content
  }
) {
  asseter.asset(is.number(x), 'x can not be null', x);
  asseter.asset(is.number(y), 'y can not be null', y);
  asseter.asset(is.number(height), 'height can not be null', height);
  asseter.asset(is.truthy(content), 'content can not be null', content);
  const _content = content.replace('"', '["]');
  cmder.push(`BARCODE ${x},${y},"${codeType}",${height},${humanReadable},${rotation},${narrow},${wide},${alignment},"${_content}"`);
  return this;
}

/**
 * This command prints QR code.
 * `QRCODE x,y,ECC Level,cell width,mode,rotation,[model,mask,]"content"`
 * @param x The upper left corner x-coordinate of the QR code
 * @param y The upper left corner y-coordinate of the QR code
 * @param eccLevel Error correction recovery level
 * L : 7%
 * M : 15%
 * Q : 25%
 * H : 30%
 * @param cellWidth 1~10
 * @param mode Auto / manual encode
 * A : Auto
 * M : Manual
 * @param rotation 0 : 0 degree
 * 90 : 90 degree
 * 180 : 180 degree
 * 270 : 270 degree
 * @param model M1: (default), original version
 * M2: enhanced version (Almost smart phone is supported by this version.)
 * @param mask S0~S8, default is S7
 * @param content The encodable character set is described as below,
 * Encodeable character set:
 * 1) Numeric data: (digits 0~9)
 * 2) Alphanumeric data
 * Digits 0-9
 * Upper case letters A-Z
 * Nine other characters: space, $ % * + - . / : )
 * 3) 8-bit byte data
 * JIS 8-bit character set (Latin and Kana) in accordance with JIS X 0201
 * 4) Kanji characters
 * Shift JIS values 8140HEX –9FFCHEX and E040HEX –EAA4 HEX. These are
 * values shifted from those of JIS X 0208. Refer to JIS X 0208 Annex 1
 * Shift Coded Representation for detail.
 * Data characters per symbol (for maximum symbol size):
 * Model 1 (Version 14-L) Model 2 (Version 40-L)
 * Numeric data 1,167 characters 7,089 characters
 * Alphanumeric data 707 characters 4,296 characters
 * 8-bit byte data 486 characters 2,953 characters
 * Kanji data 299 characters 1,817 characters
 * * If "A" is the first character in the data string, then the following data
 * after "A" is alphanumeric data.
 * *If "N" is the first character in the data string, then the following data
 * after "N" is numeric data.
 * *If "B" is the first character in the data string, then the following 4 digits
 * after "B" is used to specify numbers of data. After the 4 digits is theTSC AUTO ID Technology Co., Ltd. 63 Copyright 2013 All Rights Reserved.
 * number of bytes of binary data to be encoded.
 * *If "K" is the first character in the data string, then the following data
 * after "K" is Kanji data.
 * *If "!" is in the data string and follows by "N", "A", "B", "K" then it will be
 * switched to specified encodable character set.
 */
function qrcode(
  {
    x,
    y,
    eccLevel = 'Q',
    cellWidth = 1,
    mode = 'A',
    rotation = 0,
    model = 'M2',
    mask = 'S7',
    content
  }
) {
  asseter.asset(is.number(x), 'x can not be null', x);
  asseter.asset(is.number(y), 'y can not be null', y);
  asseter.asset(is.truthy(content), 'content can not be null', content);
  // QRCODE x,y,ECC Level,cell width,mode,rotation,[model,mask,]"content"
  cmder.push(`QRCODE ${x},${y},${eccLevel},${cellWidth},${mode},${rotation},${model},${mask},"${content}"`);
  return this;
}

/**
 * This command draws rectangles on the label.
 * `BOX x,y,x_end,y_end,line thickness[,radius]`
 * @param xStart x Specify x-coordinate of upper left corner (in dots)
 * @param yStart y Specify y-coordinate of upper left corner (in dots)
 * @param xEnd x_end Specify x-coordinate of lower right corner (in dots)
 * @param yEnd y_end Specify y-coordinate of lower right corner (in dots)
 * @param thickness line thickness Line thickness (in dots)
 * @param radius Optional. Specify the round corner. Default is 0.
 * Since V5.28 EZ
 */
function box(
  {
    xStart,
    yStart,
    xEnd,
    yEnd,
    thickness,
    radius = 0,
  }
) {
  asseter.asset(is.number(xStart), 'x start can not be null', xStart);
  asseter.asset(is.number(yStart), 'y start can not be null', yStart);
  asseter.asset(is.number(xEnd), 'x end can not be null', xEnd);
  asseter.asset(is.number(yEnd), 'y end can not be null', yEnd);
  asseter.asset(is.number(thickness), 'thickness can not be null', thickness);
  cmder.push(`BOX ${xStart},${yStart},${xEnd},${yEnd},${thickness},${radius}`);
  return this;
}

/**
 * This command draws a circle on the label.
 * `CIRCLE X_start,Y_start,diameter,thickness`
 * @param xStart x_start Specify x-coordinate of upper left corner (in dots)
 * @param yStart Y_start Specify y-coordinate of upper left corner (in dots)
 * @param diameter diameter Specify the diameter of the circle (in dots)
 * @param thickness thickness Thickness of the circle (in dots)
 */
function circle({xStart, yStart, diameter, thickness}) {
  asseter.asset(is.number(xStart), 'x start can not be null', xStart);
  asseter.asset(is.number(yStart), 'y start can not be null', yStart);
  asseter.asset(is.number(diameter), 'diameter can not be null', diameter);
  asseter.asset(is.number(thickness), 'thickness can not be null', thickness);
  cmder.push(`CIRCLE ${xStart},${yStart},${diameter},${thickness}`);
  return this;
}

function command() {
  // return encode ? ebase64(commands) : commands;
  return cmder.command();
}


module.exports = {
  version: VERSION,
  env,
  clear,
  wakeup,
  page,
  direction,
  cut,
  gap,
  speed,
  density,
  cls,
  image,
  print,
  newline,
  append,
  text,
  bar,
  barcode,
  qrcode,
  line,
  box,
  circle,
  command,
};

