const cmder = require('../sdk-api/cmdapi');
const asseter = require('../lib/asseter');

const version = '1.4.2-dev';



function clear() {
  cmder.clear();
}

function init() {
  clear();
  cmder.push('! 0 200 200 500 1');
  cmder.push('PW 560');
  cmder.push('GAP-SENSE');
  cmder.push('FORM');
  return this;
}


/*开始打印
  params:
    count:number 打印次数
*/
function print() {
  cmder.push('PRINT');
  return this;
}

/*设置打印纸张大小（打印区域）的大小
  params:
    pageWidth:number 打印区域宽度
    pageHeight:number 打印区域高度
*/
function setup(width, height) {
  cmder.push(`SIZE ${width} mm,${height} mm`);
  return this;
}

/*设置打印方向
  params:
    direction:打印方向 0/1
    mirror:number 0:Print normal image 1:Print mirror image
*/
function direction(direction, mirror) {
  cmder.push(`DIRECTION ${direction},${mirror}`);
  return this;
}

/*
  params:
    enable: true:启用；false:禁用
*/
function gap(enable) {
  cmder.push(`SET GAP ${enable ? 'ON' : 'OFF'}`);
  return this;
}

/*打印速度
  params:
    speed:number 0-15，推荐8
*/
function speed(speed) {
  cmder.push(`SPEED ${speed}`);
  return this;
}

/*打印浓度
  params:
    density:number 0-8，推荐4-8
*/
function density(density) {
  cmder.push(`DENSITY ${density}`);
  return this;
}

/*清除缓存
  params:
*/
function cls() {
  cmder.push(`CLS`);
  return this;
}


/*打印的边框
  params:
    top_left_x:number 矩形框左上角x坐标
    top_left_y:number 矩形框左上角y坐标
    bottom_right_x:number 矩形框右下角x坐标
    bottom_right_y:number 矩形框右下角y坐标
    line_width:number 边框线条宽度
    radius:number 倒角
*/
function box(top_left_x, top_left_y, bottom_right_x, bottom_right_y, line_width, radius) {
  if (top_left_x > 575)
    top_left_x = 575;
  if (bottom_right_x > 575)
    bottom_right_x = 575;
  cmder.push(`BOX ${top_left_x},${top_left_y},${bottom_right_x},${bottom_right_y},${line_width},${radius}`);
  return this;
}

/*打印线条
  params:
  start_x:number 线条起始点x坐标
  start_y:number 线条起始点y坐标
  end_x:number 线条结束点x坐标
  end_y:number 线条结束点y坐标
  width:number 线条宽度
  dottedType:number 点线类型
*/
function line(start_x, start_y, end_x, end_y, width, dotted_type) {
  let cmd = `LINE ${start_x} ${start_y} ${end_x} ${end_y} ${width}`;
  switch (dotted_type) {
    case 0:
      break;
    case 1:
      cmd = cmd + ' M1';
      break;
    case 2:
      cmd = cmd + ' M2';
      break;
    case 3:
      cmd = cmd + ' M3';
      break;
    case 4:
      cmd = cmd + ' M4';
      break;
  }
  cmder.push(cmd);
  return this;
}

/*页模式下打印文本框
  params:
    text_x:number 起始横坐标
    text_y:number 起始纵坐标
    font:string  字体大小
    rotation:number
    xmulti: number 字宽倍数
    ymulti:number 字高倍数
    bold:bool　是否加粗 true:加粗；false:不加粗
    content:string 文字内容
*/
function text(text_x, text_y, font, rotation, xmulti, ymulti, bold, content) {
  let cmd = `TEXT ${text_x},${text_y},"${font}",${rotation},${xmulti},${ymulti}`;
  if (bold)
    cmd = cmd + ',B1';
  cmd = cmd + ',\"' + content + '\"';
  cmder.push(cmd);
  return this;
}


/*一维条码
  params:
    start_x:number 一维码起始横坐标
    start_y:number 一维码起始纵坐标
    barType:number 条码类型
          0：128;    1：39;
          2：93;    3：ITF;
          4：UPCA;      5：UPCE;
          6：CODABAR;       7:EAN8;
          8:EAN13
    height:number 条码高度
    hri:number
    rotation:number
    width:number 条码宽度
    content:string 文字内容
*/
function barcode(start_x, start_y, barType, height, hri, rotation, width,
                 content) {
  let codeType = '128';
  switch (barType) {
    case 0:
      codeType = '128';
      break;
    case 1:
      codeType = '39';
      break;
    case 2:
      codeType = '93';
      break;
    case 3:
      codeType = 'ITF';
      break;
    case 4:
      codeType = 'UPCA';
      break;
    case 5:
      codeType = 'UPCE';
      break;
    case 6:
      codeType = 'CODABAR';
      break;
    case 7:
      codeType = 'EAN8';
      break;
    case 8:
      codeType = 'EAN13';
  }
  // let cmd = "BARCODE " + start_x + "," + start_y + ",\"" + codeType + "\"," + height + "," + hri + "," + rotation + "," + width + "," + width + ",\"" + content + "\"\r\n";
  let cmd = `BARCODE ${start_x},${start_y},"${codeType}",${height},${hri},${rotation},${width},${width},"${content}"`;
  cmder.push(cmd);
  return this;
}


/*二维条码
  params:
    start_x:number 二维码起始横坐标
    start_y:number 二维码起始纵坐标
    ecc:number
    rotation:number
    width:number 宽度
    version::number 版本，范围0～40
    content:string 文字内容
*/
function qrcode(start_x, start_y, ecc, rotation, width, version, content) {
  let strECC = "Q";
  switch (ecc) {
    case 0:
      strECC = "L";
      break;
    case 1:
      strECC = "M";
      break;
    case 2:
      strECC = "Q";
      break;
    case 3:
      strECC = "H";
      break;
  }
  // let cmd = "QRCODE " + start_x + "," + start_y + "," + strECC + "," + width + ",A," + rotation + ",M2,S7,V" + version + ",\"" + content + "\"\r\n";
  let cmd = `QRCODE ${start_x},${start_y},${strECC},${width},A,${rotation},M2,S7,V${version},"${content}"`;
  cmder.push(cmd);
  return this;
}


// function sendImage(x, y, width, height, bitmap) {
//   let bytes = new Array();
//   let cmd = 'BITMAP ' + x + ',' + y + ',' + width + ',' + height + ',1,';
//   mergeTo(toBytes(cmd), bytes);
//   let len = bitmap.length;
//   for (let i = 0; i < len; i+=2) {
//     let ch0 = bitmap.charAt(i);
//     let ch1 = null;
//     if (i + 1 < len)
//       ch1 = bitmap.charAt(i + 1);
//     bytes.push(Number.parseInt(ch1 == null ? (ch0 + 'F') : (ch0 + ch1), 16));
//   }
//   mergeTo(toBytes('\r\n'), bytes);
//   let base64Str = encodeArrayToBase64(bytes);
//   sendToDevice(base64Str);
// }

function image(x, y, width, height, bitmap) {
  let cmd = `BITMAP ${x},${y},${width},${height},1,`;
  cmder.push(cmd, false);

  let len = bitmap.length;
  for (let i = 0; i < len; i += 2) {
    let ch0 = bitmap.charAt(i);
    let ch1 = null;
    if (i + 1 < len)
      ch1 = bitmap.charAt(i + 1);
    // commands.push(Number.parseInt(ch1 == null ? (ch0 + 'F') : (ch0 + ch1), 16));
    cmder.push(Number.parseInt(ch1 == null ? (ch0 + 'F') : (ch0 + ch1), 16))
  }
  cmder.push('\r\n');
  return this;
}

function append(arg) {
  let cmd = arg.command;
  asseter.asset(!!cmd, 'command can not be null');
  cmder.push(cmd, arg.newline == undefined ? !arg.newline : arg.newline);
  return this;
}

function command() {
  return cmder.command();
}

module.exports = {
  version,
  clear,
  command,
  init,
  print,
  setup,
  direction,
  gap,
  speed,
  density,
  cls,
  box,
  line,
  text,
  barcode,
  qrcode,
  image,
  append
};
