const cmder = require('../sdk-api/cmdapi');
const toolkit = require('../lib/toolkit');
const asseter = require('../lib/asseter');

const VERSION = '1.4.2-dev';

const FAMILY = 24;
const DEFAULT_FAMILY = 55;

let _env = {
  offset_x: 0,
  offset_y: 0,
  unit: 'px'
};

function toPx(val) {
  switch (_env.unit) {
    case 'px':
      return val;
    case 'mm':
      return val * 8;
    case 'cm':
      return val * 80;
    default:
      return val;
  }
}

function calcX(x) {
  return Math.round(toPx(_env.offset_x + toolkit.def(x, 0)));
}

function calcY(y) {
  return Math.round(toPx(_env.offset_y + toolkit.def(y, 0)));
}


function clear() {
  // COMMANDS.splice(0, COMMANDS.length);
  _env = {offset_x: 0, offset_y: 0, unit: 'px'};
  cmder.clear();
}

function safe_arg(arg) {
  if (arg === undefined)
    return {};
  let type = toolkit.typeof(arg);
  if (type !== 'object')
    throw TypeError('Only support object arguments');
  return arg;
}

/**
 * 环境设定, 相对座标, 会为后续的所有座标点加上相同的值
 * @param {Object} arg  {
 * offset_x: x 座标统一偏移值
 * offset_y: y 座标统一偏移值
 * unit: 统一量值单位
 * }
 */
function env(arg) {
  let unit = 'px';
  let arg_unit = toolkit.def(arg.unit, 'px').toUpperCase();
  switch (arg_unit) {
    case 'PX':
    case 1:
      unit = 'px';
      break;
    case 'MM':
    case 2:
      unit = 'mm';
      break;
    case 'CM':
    case 3:
      unit = 'cm';
      break;
    default:
      unit = 'px';
      break;
  }
  _env = {
    offset_x: toolkit.def(arg.x, 0),
    offset_y: toolkit.def(arg.y, 0),
    unit
  };
  return this;
}

// function valid(condition, err) {
//   if (condition)
//     return;
//   throw TypeError(err);
// }

// function init(weight, height, sense) {
/**
 * 初始化
 * @param {Object} arg {weight, height}
 * @returns {init}
 */
function init(arg) {
  clear();
  arg = safe_arg(arg);
  let weight = arg['weight'];
  let height = arg['height'];
  let sense = arg['sense'];
  weight = weight ? weight : 592;
  height = height ? height : 475;
  cmder.push(`! 0 200 200 ${height} 1`);
  cmder.push(`PW ${weight}`);
  if (sense === undefined || sense === false || sense === 0)
    return this;

  gapsense();
  return this;
}

/**
 * 走出整页纸
 * @returns {gapsense}
 */
function gapsense() {
  cmder.push('GAP-SENSE');
  cmder.push('FORM');
  return this;
}

/**
 * 开始打印
 * @param (Boolean) horizontal  横向打印
 * @returns {print}
 */
function print(horizontal) {
  if (!horizontal) {
    cmder.push('PRINT');
    return this;
  }
  cmder.push('POPRINT');
  return this;
}

// /**
//  * 打印的边框
//  * @param width:number 边框线条宽度
//  * @param top_left_x:number 矩形框左上角x坐标
//  * @param top_left_y:number 矩形框左上角y坐标
//  * @param bottom_right_x:number 矩形框右下角x坐标
//  * @param bottom_right_y:number 矩形框右下角y坐标
//  * @returns {box}
//  */
// function box(width, top_left_x, top_left_y, bottom_right_x, bottom_right_y) {
/**
 * 打印边框
 * @param {Object} arg {
 * width:number 边框线条宽度,
 * top_left_x:number 矩形框左上角 x 坐标,
 * top_left_y:number 矩形框左上角 y 坐标,
 * bottom_right_x:number 矩形框右下角 x 坐标,
 * bottom_right_y:number 矩形框右下角 y 坐标
 * }
 * @returns {box}
 */
function box(arg) {
  arg = safe_arg(arg);
  let width = arg['width'];
  let top_left_x = arg['top_left_x'];
  let top_left_y = arg['top_left_y'];
  let bottom_right_x = arg['bottom_right_x'];
  let bottom_right_y = arg['bottom_right_y'];
  asseter.asset(!!top_left_x, 'top left x can not be null', arg);
  asseter.asset(!!top_left_y, 'top left y can not be null', arg);
  asseter.asset(!!bottom_right_x, 'bottom right x can not be null', arg);
  asseter.asset(!!bottom_right_y, 'bottom right y can not be null', arg);
  if (top_left_x > 575)
    top_left_x = 575;
  if (bottom_right_x > 575)
    bottom_right_x = 575;
  cmder.push(`BOX ${calcX(top_left_x)} ${calcY(top_left_y)} ${calcX(bottom_right_x)} ${calcY(bottom_right_y)} ${width}`);
  return this;
}


/**
 * 打印线条
 * @param {Object} arg  {
 * width:number 线条宽度,
 * start_x:number 线条起始点x坐标,
 * start_y:number 线条起始点y坐标,
 * end_x:number 线条结束点x坐标,
 * end_y:number 线条结束点y坐标,
 * solid:boolean 是否实线
 * }
 * @returns {line}
 */
function line(arg) {
  arg = safe_arg(arg);
  let width = arg['width'];
  let start_x = arg['start_x'];
  let start_y = arg['start_y'];
  let end_x = arg['end_x'];
  let end_y = arg['end_y'];
  let solid = arg['solid'];
  solid = toolkit.def(solid, true);
  asseter.asset(!!width, 'width can not be null', arg);
  asseter.asset(start_x === 0 || !!start_x, 'start x can not be null', arg);
  asseter.asset(start_y === 0 || !!start_y, 'start y can not be null', arg);
  asseter.asset(end_x === 0 || !!end_x, 'end x can not be null', arg);
  asseter.asset(end_y === 0 || !!end_y, 'end y can not be null', arg);
  if (start_x > 575)
    start_x = 575;
  if (end_x > 575)
    end_x = 575;

  let cmd;
  if (solid) {
    cmd = `L ${calcX(start_x)} ${calcY(start_y)} ${calcX(end_x)} ${calcY(end_y)} ${width}`
  } else {
    cmd = `LP ${calcX(start_x)} ${calcY(start_y)} ${calcX(end_x)} ${calcY(end_y)} ${width}`
  }

  cmder.push(cmd);
  return this;
}

/**
 * 打印文本框
 * @param {Object} arg {
 * x:number 起始横坐标
 * y:number 起始纵坐标
 * text:string 文字内容
 * font_size:number 文字大小
 * rotation:number 旋转
 * bold:number 加粗
 * underline:boolean 下划线
 * auton:boolean 自动换行
 * }
 * @returns {text}
 */
function text(arg) {
  // let cmd = `TEXT ${text_x},${text_y},"${font}",${rotation},${xmulti},${ymulti}`;
  // if (bold)
  //   cmd = cmd + ',B1';
  // cmd = cmd + ',\"' + content + '\"';
  // push(cmd);
  // return this;
  arg = safe_arg(arg);
  let x = arg['x'];
  let y = arg['y'];
  let text = arg['text'];
  let font_size = arg['font_size'];
  let rotation = arg['rotation'];
  let bold = arg['bold'];
  let underline = arg['underline'];
  let auton = arg['auton'];
  asseter.asset(x === 0 || !!x, 'x can not be null', arg);
  asseter.asset(y === 0 || !!y, 'y can not be null', arg);
  asseter.asset(!!text, 'text can not be null => ' + JSON.stringify(arg));
  text = text.toString().replace('\n', ' ');

  if (toolkit.def(auton, true)) {
    text_auton(arg);
    return this;
  }

  cmder.push(underline ? 'UNDERLINE ON' : 'UNDERLINE OFF');
  if (bold) {
    cmder.push(`SETBOLD ${bold}`);
  }
  let size = 0;
  let ex = 1;
  let ey = 1;
  let family;
  switch (font_size) {
    case 1: // 16
      family = 55;
      break;
    case 2: // 24
      family = FAMILY;
      break;
    case 3: // 32
      family = 4;
      break;
    case 4: // 48
      family = FAMILY;
      ex = 2;
      ey = 2;
      break;
    case 5: // 64
      family = 4;
      ex = 2;
      ey = 2;
      break;
    case 6: // 72
      family = FAMILY;
      ex = 3;
      ey = 3;
      break;
    case 7: // 96
      family = 4;
      ex = 3;
      ey = 3;
      break;
    case 8:
      family = 3;
      size = 1;
      ex = 1;
      ey = 1;
      break;
    case 9:
      family = 4;
      size = 3;
      ex = 1;
      ey = 1;
      break;
    default:
      family = DEFAULT_FAMILY;
  }

  cmder.push(`SETMAG ${ex} ${ey}`);

  let c0;
  switch (rotation) {
    case 1:
      c0 = 'T90';
      break;
    case 2:
      c0 = 'T180';
      break;
    case 3:
      c0 = 'T270';
      break;
    default:
      c0 = 'T';
      break;
  }
  // push(`${c0} ${family} ${size} ${text_x} ${text_y} `, false);
  // let bytes = encodingText(text);
  // bytes.forEach(byte => COMMANDS.push(byte));
  // push('\r\n');
  let _cmd = `${c0} ${family} ${size} ${calcX(x)} ${calcY(y)} ${text}`;
  cmder.push(_cmd);
  return this;
}

/**
 * 文字自动换行
 * @param {Object} arg
 */
function text_auton(arg) {
  arg.auton = false;
  return text(arg);
}


/**
 * 条码
 * @param {Object} arg {
 * x:number 一维码起始横坐标
 * y:number 一维码起始纵坐标
 * text:string 文字内容
 * type:number 条码类型
 *            0: 39;        1: 128;
 *            2: 93;        3: CODABAR;
 *            4: EAN8;      5: EAN13;
 *            6: UPCA;      7: UPCE;
 *            8: I2OF5      default: 128
 * rotate:number
 * width:number 条码宽度
 * height:number 条码高度
 * }
 * @returns {barcode}
 */
function barcode(arg) {
  arg = safe_arg(arg);
  let x = arg['x'];
  let y = arg['y'];
  let text = arg['text'];
  let type = arg['type'];
  let rotation = arg['rotation'];
  let width = arg['width'];
  let height = arg['height'];
  asseter.asset(!!x, 'start x can not be null', arg);
  asseter.asset(!!y, 'start y can not be null', arg);
  asseter.asset(!!text, 'text can not be null', arg);
  asseter.asset(!!width, 'width can not be null', arg);
  asseter.asset(!!height, 'height can not be null', arg);

  let cmd = 'B', bartype;
  if (rotation !== 0 && rotation !== 2) {
    cmd = rotation !== 1 && rotation !== 3 ? 'B' : 'VB';
  }

  switch (type) {
    case 0:
      bartype = '39';
      break;
    case 1:
      bartype = '128';
      break;
    case 2:
      bartype = '93';
      break;
    case 3:
      bartype = 'CODABAR';
      break;
    case 4:
      bartype = 'EAN8';
      break;
    case 5:
      bartype = 'EAN13';
      break;
    case 6:
      bartype = 'UPCA';
      break;
    case 7:
      bartype = 'UPCE';
      break;
    case 8:
      bartype = 'I2OF5';
      break;
    default:
      bartype = '128';
  }
  cmder.push(`${cmd} ${bartype} ${width - 1} 2 ${height} ${calcX(x)} ${calcY(y)} ${text}`);
  return this;
}

/**
 * 打印二维码
 * @param {Object} arg {
 * x:number 二维码起始横坐标
 * y:number 二维码起始纵坐标
 * text:string 二维码内容
 * rotate:number 旋转
 * ver:number
 * lel:number
 * }
 * @returns {qrcode}
 */
function qrcode(arg) {
  arg = safe_arg(arg);
  let x = arg['x'];
  let y = arg['y'];
  let text = arg['text'];
  let rotation = arg['rotation'];
  let version = arg['version'];
  let level = arg['level'];
  version = toolkit.def(version, 2);

  asseter.asset(!!x, 'start x can not be null', arg);
  asseter.asset(!!y, 'start y can not be null', arg);
  asseter.asset(!!text, 'text can not be null', arg);

  let cmd = 'B', levelText;
  if (rotation !== 0 && rotation !== 2) {
    cmd = rotation !== 1 && rotation !== 3 ? 'B' : 'VB';
  }

  if (version < 2) {
    version = 2;
  }

  if (version > 6) {
    version = 6;
  }

  switch (level) {
    case 0:
      levelText = 'L';
      break;
    case 1:
      levelText = 'M';
      break;
    case 2:
      levelText = 'Q';
      break;
    case 3:
      levelText = 'H';
      break;
    default:
      levelText = 'M';
      break;
  }
  let _cmd0 = `${cmd} QR ${calcX(x)} ${calcY(y)} M 2 U ${version} ${levelText}A, ${text}`;
  cmder.push(_cmd0);
  cmder.push('ENDQR');
  return this;
}


/**
 *
 * 图片指令
 * @param {Object} arg {
 * x x 座标
 * y y 座标
 * width 宽度
 * height 高度
 * bitmap bitmap
 * }
 * @returns {image}
 */
function image(arg) {
  arg = safe_arg(arg);
  let x = arg['x'];
  let y = arg['y'];
  let width = arg['width'];
  let height = arg['height'];
  let bitmap = arg['bitmap'];
  asseter.asset(!!x, 'x can not be null', arg);
  asseter.asset(!!y, 'y can not be null', arg);
  asseter.asset(!!width, 'width can not be null', arg);
  asseter.asset(!!height, 'height can not be null', arg);
  asseter.asset(!!bitmap, 'bitmap can not be null', arg);
  cmder.push(`EG ${calcX(x)} ${calcY(y)} ${width} ${height} ${bitmap}`);
  return this;
}

/**
 * 执行原始指令
 * @param {Object}  arg {
 * command  原始指令
 * newline 是否换行 默认 true
 * }
 * @returns {origin}
 */
function append(arg) {
  let cmd = arg.command;
  asseter.asset(!!cmd, 'command can not be null', arg);
  cmder.push(cmd, toolkit.def(arg.newline, true));
  return this;
}

function command() {
  // return encode ? ebase64(commands) : commands;
  return cmder.command();
}

module.exports = {
  version: VERSION,
  clear,
  env,
  init,
  gapsense,
  print,
  box,
  line,
  text,
  barcode,
  qrcode,
  image,
  command,
  append,
};
