const assert = require('assert');
const {createCanvas, loadImage} = require('canvas')
const toolkit = require('../lib/toolkit')
const sdkby426bt = require('../sdk-by426bt/sdkby426bt');


describe('sdkby426bt', function () {
  describe('#cls()', function () {
    it('cls command', function () {
      let command = sdkby426bt.clear()
        .cls()
        .command();
      // console.log(command);
    });
  });

  // describe('#image()', function() {
  //   it('image command', function() {
  //     const canvas = createCanvas(200, 200);
  //     const ctx = canvas.getContext('2d');
  //
  //     loadImage('https://avatars.githubusercontent.com/u/37804932?s=80&v=4').then((image) => {
  //       ctx.drawImage(image, 50, 0, 70, 70)
  //       let command = sdkby426bt.clear()
  //         .image({x: 0, y: 0, canvas})
  //         .command();
  //       const binary = command.binary();
  //       const hex = toolkit.hex(binary, {format: true, maxLineLength: 64});
  //       console.log(hex);
  //       console.log(command.string());
  //     });
  //   });
  // });

  describe('#barcode()', () => {
    it('barcode with text', () => {
      const command = sdkby426bt.clear()
        .wakeup()
        .page({width: 100, height: 150})
        .direction({})
        .cut({})
        .gap({m: 3, unit: 'mm'})
        .speed({speed: 6})
        .density({density: 5})
        .cls()
        .barcode({
          x: 20,
          y: 50,
          height: 150,
          narrow: 4,
          wide: 4,
          humanReadable: 2,
          content: '3456789023',
        })
        .text({
          x: 20,
          y: 250,
          font: 2,
          // xMultiplication: 4,
          // yMultiplication: 4,
          content: '中文测试The description content',
        })
        .print({})
        .command();
      console.log(command.hex({format: true, maxLineLength: 64}));
      console.log(command.string());
    });
  });

  describe('#template()', async () => {
    it('full image command', async (done) => {
      /*

            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.unititled);

            ArrayList<byte[]> data = new ArrayList<byte[]>();
            byte[] wakeup = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            BeeprtCommand printer = new BeeprtCommand();
            data.add(wakeup);
            data.add(printer.Beeprt_CreatePage(100, 175));
            // 设置打印方向
            data.add(printer.Beeprt_Direction(0, 0));
            // 始能切刀，没打完一张自动切纸
            data.add(printer.Beeprt_Cut(true));
            // 设置缝隙定位
            data.add(printer.Beeprt_SetGap(true));
            // 设置速度3
            data.add(printer.Beeprt_Speed(6));
            // 设置浓度
            data.add(printer.Beeprt_Density(5));
            // 清除页面缓冲区
            data.add(printer.Beeprt_Cls());

            data.add(printer.Beeprt_DrawPic(0, 0, bitmap));
            data.add(printer.Beeprt_PrintPage(1));
            printPort.portSendCmd(data);

       */

      try {
        const image = await loadImage('https://mio.kahub.in/open/waybill-usps.png');
        // const image = await loadImage('https://cbu01.alicdn.com/img/ibank/O1CN012va3v41LllNvG48w2_!!2574931340-0-cib.jpg');


        const width = image.width;
        const height = image.height;

        const canvas = createCanvas(width, height);
        const ctx = canvas.getContext('2d');
        ctx.drawImage(image, 0, 0, width, height)

        const start = +new Date();
        console.log('start ---->', start);
        const command = sdkby426bt.clear()
          .wakeup()
          .page({width: 100, height: 148})
          .direction({})
          .cut({})
          .gap({})
          .speed({speed: 6})
          .density({density: 5})
          .cls()
          .image({
            canvas,
            compress: true,
          })
          .print({})
          .command();
        console.log(command.hex({}));
        console.log(command.hex({format: true, maxLineLength: 64}));
        console.log(command.string())
        const end = +new Date();
        console.log('end   ---->', end);
        console.log('time  ---->', end - start);


        // loadImage('https://mio.kahub.in/open/waybill-usps.png').then(image => {
        // })
      } catch (e) {
        console.error(e);
      } finally {
        done()
      }
    });
  });

  it('text command', function () {
    try {
      let command = sdkby426bt.clear()
        .wakeup()
        .page({width: 100, height: 150})
        .direction({x: 0, y: 0})
        .cut({})
        .gap({})
        .speed({speed: 6})
        .density({density: 5})
        .cls()
        .text({
          x: 30,
          y: 120,
          rotation: 0,
          xMultiplication: 1,
          yMultiplication: 1,
          content: 'Test content',
          font: 'TSS24.BF2',
          alignment: 'B1',
        })
        .print({})
        .command();
      const binary = command.binary();
      // const hex = toolkit.hex(binary, {format: true, maxLineLength: 64});
      // console.log(command.string());
      // console.log(hex);
    } catch (e) {
      console.error(e);
    }
  });
});


