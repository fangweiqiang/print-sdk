

const base64 = require('../lib/base64');
const iconv = require('iconv-lite');

const chn = require('../sdk-chn/chn');

const e0 = iconv.encode('中','gbk');
const arr = Array.prototype.slice.call(e0, 0);
console.log(arr);

const str=new Buffer(e0).toString('base64');
console.log(str);

const c0 = chn.ebase64('韵达');
console.log("chn.ebase64('韵达') -> ", c0);
const c1 = chn.debase64(c0);
console.log(c1);
