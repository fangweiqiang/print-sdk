'use strict';

function asset(condition, message, arg) {
  if (condition)
    return;
  throw TypeError(message + (arg ? ' => ' + JSON.stringify(arg) : ''));
}

module.exports = {
  asset
};
