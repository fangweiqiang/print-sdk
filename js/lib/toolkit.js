
const base64Arraybuffer = require('base64-arraybuffer');
const typedarray = require('typedarray');

const class2type = {};
"Boolean Number String Function Array Date RegExp Object Error Uint8Array Uint16Array".split(" ").forEach(function (e, i) {
  class2type["[object " + e + "]"] = e.toLowerCase();
});

/**
 * 字符串转 bytes
 * @param text
 * @returns {array[]}
 */
function toBytes(text) {
  let bytes = [];
  let len, c;
  len = text.length;
  for (let i = 0; i < len; i++) {
    c = text.charCodeAt(i);
    if (c >= 0x010000 && c <= 0x10FFFF) {
      bytes.push(((c >> 18) & 0x07) | 0xF0);
      bytes.push(((c >> 12) & 0x3F) | 0x80);
      bytes.push(((c >> 6) & 0x3F) | 0x80);
      bytes.push((c & 0x3F) | 0x80);
    } else if (c >= 0x000800 && c <= 0x00FFFF) {
      bytes.push(((c >> 12) & 0x0F) | 0xE0);
      bytes.push(((c >> 6) & 0x3F) | 0x80);
      bytes.push((c & 0x3F) | 0x80);
    } else if (c >= 0x000080 && c <= 0x0007FF) {
      bytes.push(((c >> 6) & 0x1F) | 0xC0);
      bytes.push((c & 0x3F) | 0x80);
    } else {
      bytes.push(c & 0xFF);
    }
  }
  return bytes;
}

function _typeof(obj) {
  if (obj == null) {
    return String(obj);
  }
  return typeof obj === "object" || typeof obj === "function" ? class2type[class2type.toString.call(obj)] || "object" : typeof obj;
}

function def(obj, def) {
  if (obj === null || obj === undefined) {
    return def;
  }
  return obj;
}

function hexString(bytes, {format = false, spaceBetween = true, maxLineLength = 16}) {
  const hex = Array.from(bytes, function(byte) {
    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
  }).join('');
  if (!format) return hex;
  let outputs = [];
  let cells = [];
  for (let i = 0; i<hex.length; i++) {
    const ch = hex[i];
    cells.push(ch);
    if ((i + 1) % 2 === 0) {
      outputs.push(...cells);
      cells = [];
      if (spaceBetween) {
        outputs.push(' ');
      }
    }
    if ((i + 1) % maxLineLength === 0) {
      outputs.push('\n');
    }
  }
  return outputs.join('');
}

function base64ToArrayBuffer(base64) {
  return base64Arraybuffer.decode(base64);
}

function arrayBufferToBase64(arraybuffer) {
  return base64Arraybuffer.encode(arraybuffer);
}

function bytesToUnit8Array(bytes) {
  const _Uint8Array = Uint8Array ? Uint8Array : typedarray.Uint8Array;
  return new _Uint8Array(bytes);
}


module.exports = {
  bytes: toBytes,
  typeof: _typeof,
  def,
  hex: hexString,
  base64ToArrayBuffer,
  arrayBufferToBase64,
  bytesToUnit8Array,
};
