const iconv = require('iconv-lite');

module.exports = {
  encode: iconv.encode,
  decode: iconv.decode
};
