const iconv = require('iconv-lite');
const base64 = require('../lib/base64');
const toolkit = require('../lib/toolkit');

function binary(text) {
  const buffer = iconv.encode(text, 'gbk');
  const binary = Array.prototype.slice.call(buffer, 0);
  return binary;
}

function text(binary) {
  return iconv.decode(binary, 'gbk');
}

function ebase64(target) {
  const type = toolkit.typeof(target);
  let arraybuffer;
  switch (type) {
    case 'string':
      arraybuffer = binary(target);
      break;
    case 'array':
      arraybuffer = target;
      break;
    default:
      throw TypeError('Not support current value encode base64');
  }
  return base64.encode(arraybuffer);
}

function debase64(target) {
  return base64.decode(target);
}

module.exports = {
  binary,
  text,
  ebase64,
  debase64,
};
