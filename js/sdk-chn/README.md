
# sdk-chn


```javascript
const chn = require('./chn.js')

// encode text to gbk binary (bytes)
const gbkbinary = chn.binary('中文');
// decode gbk binary to text
const text = chn.text(gbkbinary);
// encode text to gbk binary and encode base64
const base64str = chn.ebase64(text);
```

