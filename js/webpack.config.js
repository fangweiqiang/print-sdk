const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    'test': './test/test.js',
    'sdk386': './sdk-qr386/sdk386.js',
    'sdk588': './sdk-qr588/sdk588.js',
    'toolkit': './lib/toolkit.js',
    'base64': './lib/base64.js',
    'iconv': './lib/iconv.js',
    'chn': './sdk-chn/chn.js',
    'sdkby426bt': './sdk-by426bt/sdkby426bt.js',
  },
  output: {
    path: path.resolve(__dirname, 'target'),
    filename: '[name].min.js',

    // library: 'util',
    // libraryTarget: 'commonjs2'

    library: '[name]',
    libraryTarget: 'umd',
    globalObject: "this",
  },
  module: {
    rules: [{
      test: /\.js$/, // 处理以.js结尾的文件
      exclude: /node_modules/, // 处理除了nodde_modules里的js文
      loader: 'babel-loader'// 用babel-loader处理
    }]
  }

};
