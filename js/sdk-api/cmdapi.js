'use strict';

const base64 = require('../lib/base64');
const toolkit = require('../lib/toolkit');
const iconv = require('iconv-lite');

let stacks = [];
let commands = [];

function exists(text) {
  for (let i = stacks.length; i-- > 0;) {
    if (stacks[i] === text)
      return true;
  }
  return false;
}

function push(_c, newline) {
  // 若指令是换行, 直接添加
  if (_c === '\r\n') {
    const _t = toolkit.bytes('\r\n');
    _t.forEach(item => {
      commands.push(item);
    });
    stacks.push(_c);
    return;
  }
  // 鉴定类型
  const type = toolkit.typeof(_c);
  switch (type) {
    // 字符类型, 进行编码转换, 并追加
    case 'string':
      // 因有功能会有多条指令, 多次可能会相同, 不在做校验
      // 如果有相同的指令, 不进行追加
      // if (exists(_c)) {
      //   return this;
      // }
      stacks.push(_c);
      const buffer = iconv.encode(_c, 'gbk');
      const binary = Array.prototype.slice.call(buffer, 0);
      binary.forEach(item => {
        commands.push(item);
      });
      if (newline !== 0 && newline !== false) {
        const crlf = toolkit.bytes('\r\n');
        crlf.forEach(item => {
          commands.push(item);
        });
      }
      break;
    // 数组, 当作 byte[] 看待
    case 'array':
      _c.forEach(item => {
        commands.push(item);
      });
      break;
    // Uint8Array
    case 'uint8array':
      _c.forEach(item => {
        commands.push(item);
      });
      break;
    // Uint16Array
    case 'uint16array':
      _c.forEach(item => {
        commands.push(item);
      });
      break;
    // 数字类型, 当作 byte 看待, 轧入到 commands
    case 'number':
      // 判断是否 -254 ~ 254 之间
      commands.push(_c);
      break;
  }
  return this;
}

function command() {
  return {
    binary: () => commands,
    string: () => iconv.decode(commands, 'gbk'),
    base64: () => base64.encode(commands),
    buffer: () => toolkit.bytesToUnit8Array(commands).buffer,
    hex: ({
            format = false,
            spaceBetween = true,
            maxLineLength = 16
          }) => toolkit.hex(commands, {
      format, spaceBetween, maxLineLength
    }),
  };
}

function clear() {
  // stacks.splice(0, stacks.length);
  // commands.splice(0, commands.length);
  stacks = [];
  commands = [];
  return this;
}

module.exports = {
  push,
  command,
  clear
};

