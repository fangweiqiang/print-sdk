library sdk_esc;

import 'package:archive/archive.dart';
import 'package:image/image.dart';
import 'package:sdk_esc/printer.dart';
import 'package:sdk_print_common/sdk_print_common.dart';

class ESC {
  final Commander _commander = Commander();

  /// Special printer, maybe some printer have custom command
  Printer? _printer;

  /// Set special printer
  ESC printer(Printer printer) {
    _printer = printer;
    return this;
  }

  /// Clear
  ESC clear() {
    _commander.clear();
    return this;
  }

  /// Wakeup
  ESC wakeup() {
    _commander.pushBinary([
      0x00,
      0x00,
      0x00,
      0x00,
      0x00,
      0x00,
      0x00,
      0x00,
      0x00,
      0x00,
      0x00,
      0x00
    ]);
    return this;
  }

  ESC page({width = 100, height = 175}) {
    _commander.pushText('SIZE ${width} mm,${height} mm').newline();
    return this;
  }

  ESC direction({x = 0, y = 0}) {
    _commander.pushText('DIRECTION ${x},${y}').newline();
    return this;
  }

  ESC raw({required String command}) {
    _commander.pushText(command).newline();
    return this;
  }

  ESC cut({condition = true}) {
    var command = condition ? 'SET CUTTER 1' : 'SET CUTTER OFF';
    _commander.pushText(command).newline();
    return this;
  }

  ESC gap({m = 0, n = 0}) {
    _commander.pushText('GAP ${m} mm,${n} mm').newline();
    return this;
  }

  ESC speed({speed = 6}) {
    if (speed < 1) {
      speed = 1;
    }
    if (speed > 8) {
      speed = 8;
    }
    _commander.pushText('SPEED ${speed}').newline();
    return this;
  }

  ESC density({density = 5}) {
    if (density < 1) {
      density = 1;
    }
    if (density > 15) {
      density = 15;
    }
    _commander.pushText('DENSITY ${density}').newline();
    return this;
  }

  ESC cls() {
    _commander.pushText('CLS').newline();
    return this;
  }

  ESC print({copies = 1}) {
    _commander.pushText('PRINT 1,${copies}').newline();
    return this;
  }

  ESC newline() {
    _commander.newline();
    return this;
  }

  ESC text({
    required int x,
    required int y,
    font = 0,
    rotation = 0,
    alignment = 'B1',
    xMultiplication = 1,
    yMultiplication = 1,
    required String content,
  }) {
    // TEXT x,y,"font",rotation,x-multiplication,y-multiplication,[alignment,]"content"
    var _content = content.replaceAll('"', '["]');
    _commander
        .pushText(
            'TEXT ${x},${y},"${font}",${rotation},${xMultiplication},${yMultiplication},${alignment},"${_content}"')
        .newline();
    return this;
  }

  ESC bar(
      {required int x,
      required int y,
      required int width,
      required int height}) {
    _commander.pushText('BAR ${x},${y},${width},${height}').newline();
    return this;
  }

  ESC line(
      {required int x,
      required int y,
      required int width,
      required int height}) {
    return this.bar(x: x, y: y, width: width, height: height);
  }

  ESC barcode(
      {required int x,
      required int y,
      codeType = '128',
      required int height,
      humanReadable = 0,
      rotation = 0,
      narrow = 1,
      wide = 1,
      alignment = 'B2',
      required String content}) {
    var _content = content.replaceAll('"', '["]');
    _commander
        .pushText(
            'BARCODE ${x},${y},"${codeType}",${height},${humanReadable},${rotation},${narrow},${wide},${alignment},"${_content}"')
        .newline();
    return this;
  }

  ESC qrcode(
      {required int x,
      required int y,
      eccLevel = 'Q',
      cellWidth = 1,
      mode = 'A',
      rotation = 0,
      model = 'M2',
      mask = 'S7',
      required String content}) {
    // QRCODE x,y,ECC Level,cell width,mode,rotation,[model,mask,]"content"
    _commander
        .pushText(
            'QRCODE ${x},${y},${eccLevel},${cellWidth},${mode},${rotation},${model},${mask},"${content}"')
        .newline();
    return this;
  }

  ESC box({
    required int xStart,
    required int yStart,
    required int xEnd,
    required int yEnd,
    required int thickness,
    radius = 0,
  }) {
    _commander
        .pushText(
            'BOX ${xStart},${yStart},${xEnd},${yEnd},${thickness},${radius}')
        .newline();
    return this;
  }

  ESC circle(
      {required int xStart,
      required int yStart,
      required int diameter,
      required int thickness}) {
    _commander
        .pushText('CIRCLE ${xStart},${yStart},${diameter},${thickness}')
        .newline();
    return this;
  }

  ESC selfTest() {
    _commander.pushText('SELFTEST').newline();
    return this;
  }

  ESC appendText({newline = true, required String command}) {
    _commander.pushText(command);
    if (newline) {
      _commander.newline();
    }
    return this;
  }

  ESC appendBinary(List<int> binary) {
    _commander.pushBinary(binary);
    return this;
  }

  /**
   *This command draws bitmap images (as opposed to BMP graphic files)
   *
   * @param x Specify the x-coordinate
   * @param y Specify the y-coordinate
   * @param width Image width (in bytes)
   * @param height Image height (in dots)
   * @param compress Is compress iamge data
   * @param modeGraphic modes listed below:
   *   0: OVERWRITE
   *   1: OR
   *   2: XOR
   *   if comparess is true, this field is not required
   * @param context bitmap data Bitmap data
   * @param imageDataHandler get pixel image data handler
   * @returns {image}
   */
  ESC image({
    x = 0,
    y = 0,
    compress = true,
    mode = 0,
    required List<int> image,
  }) {
    var imgObj = decodeImage(image)!;
    var width = imgObj.width;
    var height = imgObj.height;
    var eWidth = ((width % 8 == 0) ? (width / 8) : (width / 8 + 1)).floor();
    var bytes = _imageData(imgObj, width, height);

    if (compress) {
      var img = ZLibEncoder().encode(bytes);
      var cmd_left = 'BITMAP ${x},${y},${eWidth},${height},3,${img.length},';
      _commander.pushText(cmd_left);
      _commander.pushBinary(img);
    } else {
      var cmd_left = 'BITMAP ${x},${y},${eWidth},${height},${mode},';
      _commander.pushText(cmd_left);
      _commander.pushBinary(bytes);
    }
    _commander.newline();
    return this;
  }

  List<int> _imageData(Image image, int width, int height) {
    var eWidth = ((width % 8 == 0) ? (width / 8) : (width / 8 + 1)).floor();
    var currentHeight = 0;
    var index = 0;
    var area = height * eWidth;
    List<int> bytes = []; // the bytes length is equals `area`
    for (var b1 = 0; b1 < area; b1++) {
      bytes.add(0);
    }

    while (currentHeight < height) {
      // let rowData = []; // the row data length is image height
      var eightBitIndex = 0;
      for (var x = 0; x < width; x++) {
        eightBitIndex++;
        if (eightBitIndex > 8) {
          eightBitIndex = 1;
          index++;
        }
        var n = 1 << 8 - eightBitIndex;

        int pixel = image.getPixel(x, currentHeight);
        int red = (pixel >> 16) & 0xFF;
        int green = (pixel >> 8) & 0xFF;
        int blue = pixel & 0xFF;

        if ((red + green + blue) / 3 < 190) {
          bytes[index] = (bytes[index] | n);
        }
      }
      index = eWidth * (currentHeight + 1);
      currentHeight += 1;
    }
    return bytes;
  }

  Command command() {
    return _commander.command();
  }
}
