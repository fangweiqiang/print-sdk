import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:sdk_esc/esc.dart';

void main() {
  // final file = File('test/res/waybill-usps.png');

  test('test generate image command', () async {
    // final List<int> bytes = file.readAsBytesSync();

    Response<List<int>> response = await Dio().get<List<int>>(
      'https://mio.kahub.in/open/waybill-usps.png',
      options: Options(responseType: ResponseType.bytes), // set responseType to `bytes`
    );
    var bytes = response.data;


    var start = DateTime.now().millisecondsSinceEpoch;
    print('start ----> ${start}');
    var command = ESC()
        .clear()
        .wakeup()
        .page(width: 100, height: 148)
        .direction()
        .cut()
        .gap()
        .speed(speed: 6)
        .density(density: 5)
        .cls()
        .image(
          image: bytes!,
          compress: true,
        )
        .print()
        .command();
    print(command.hex());
    print(command.hex(format: true, maxLineLength: 64));
    print(command.string());

    var end = DateTime.now().millisecondsSinceEpoch;
    print('end   ----> ${end}');
    print('time  ----> ${end - start}');

  });
}
