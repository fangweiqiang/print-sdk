import 'package:flutter_test/flutter_test.dart';

import 'package:sdk_print_common/sdk_print_common.dart';

void main() {
  test('test command hex format', () {
    var data = [
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      83,
      73,
      90,
      69,
      32,
      49,
      48,
      48,
      5,
      3,
      39,
      209,
      226,
      160,
      13,
      10,
      80,
      82,
      73,
      78,
      84,
      32,
      50
    ];
    // 00000000000053495a45203130305327d1e2a0da5052494e542032
    // 00 00 00 00 00 00 53 49 5a 45 20 31 30 30 53 27 d1 e2 a0 da 50 52 49 4e 54 20 32
    // 00 00 00 00 00 00 53 49 5a 45 20 31 30 30 53 27 d1 e2 a0 da 50 52 49 4e 54 20 32
    var hex = Commander()
        .pushBinary(data)
        .command()
        .hex(format: true, maxLineLength: 16);
    print(hex);
  });
}
