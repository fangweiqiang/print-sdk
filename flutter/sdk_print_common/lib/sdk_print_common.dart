library sdk_print_common;

import 'dart:convert' as std_convert;

import 'package:enough_convert/enough_convert.dart';
import 'package:hex/hex.dart';

class Commander {
  List<int> _bytes = [];

  Commander pushBinary(List<int> binary) {
    _bytes.addAll(binary);
    return this;
  }

  Commander pushText(String text) {
    final codec = const GbkCodec(allowInvalid: false);
    List<int> list = codec.encode(text);
    _bytes.addAll(list);
    return this;
  }

  Commander newline() {
    return this.pushText('\r\n');
  }

  Commander clear() {
    this._bytes.clear();
    return this;
  }

  Command command() {
    return Command(this._bytes);
  }
}

class Command {
  final List<int> _bytes;

  Command(this._bytes);

  List<int> binary() {
    return this._bytes;
  }

  String base64() {
    return std_convert.base64.encode(this._bytes);
  }

  String hex({format = false, spacing = true, maxLineLength = 64}) {
    // var hex = this._bytes.map((byte) => byte.toRadixString(16)).join('');
    // var hex = this._bytes.map((byte) => (byte & 0xFF).toRadixString(16)).toList().join('');
    var hex = HEX.encode(this._bytes);
    if (!format) {
      return hex;
    }
    List<String> outputs = [];
    final length = hex.length;
    for (var i = 0; i < length; i++) {
      final v = hex[i];
      outputs.add(v.toString());
      var x = i + 1;
      if (x % 2 == 0) {
        outputs.add(' ');
      }
      if (x % maxLineLength == 0) {
        outputs.add('\n');
      }
    }
    return outputs.join('');
  }

  String string() {
    final codec = const GbkCodec(allowInvalid: false);
    return codec.decode(this._bytes, allowInvalid: true);
  }
}
