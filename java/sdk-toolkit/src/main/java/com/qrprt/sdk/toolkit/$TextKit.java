package com.qrprt.sdk.toolkit;

import java.text.MessageFormat;

/**
 * $QRKIT$TextKit.
 */
public class $TextKit {


  public static boolean isBlank(String text) {
    return blanky(text);
  }

  public static boolean isBlank(String... text) {
    return blanky(text);
  }

  public static boolean notBlank(String text) {
    return blankn(text);
  }

  public static boolean notBlank(String... text) {
    return blankn(text);
  }

  /**
   * 字符串为 null 或者内部字符全部为 ' ' '\t' '\n' '\r' 这四类字符时返回 true
   */
  public static boolean blanky(String text) {
    if (text == null) {
      return true;
    }
    int len = text.length();
    if (len == 0) {
      return true;
    }
    for (int i = 0; i < len; i++) {
      switch (text.charAt(i)) {
        case ' ':
        case '\t':
        case '\n':
        case '\r':
          // case '\b':
          // case '\f':
          break;
        default:
          return false;
      }
    }
    return true;
  }

  public static boolean blanky(String... strings) {
    if (strings.length == 0)
      return true;
    boolean pass = true;
    for (String string : strings) {
      if (blanky(string))
        continue;
      pass = false;
      break;
    }
    return pass;
  }

  public static boolean blankn(String str) {
    return !blanky(str);
  }

  public static boolean blankn(String... strings) {
    if (strings.length == 0) {
      return false;
    }
    for (String str : strings) {
      if (blanky(str)) {
        return false;
      }
    }
    return true;
  }

  public static String union(int capacity, String text, Object... union) {
    if (union == null)
      return text;
    StringBuilder ret = new StringBuilder(capacity);
    ret.append(text);
    for (Object u : union) {
      ret.append(u);
    }
    return ret.toString();
  }

  public static String union(String text, Object... union) {
    return union(text.length() + 16, text, union);
  }


  /**
   * 字符串格式化, 格式化方式采用与 MessageFormat 格式相同, 兼容 MessageFormat
   * MessageFormat 使用中如果 formats 中传递有 {} 格式文本, 会抛出异常, 这里的替换方案中不会有此现象
   * <p>
   * Example:
   * This is text from {0} and {1}.
   * arg0 arg1
   *
   * @param message 消息
   * @param formats 格式化
   * @return String
   */
  public static String format(String message, Object... formats) {
    // 如果需要则开启此注释, 使用替换方案
//    if (message == null)
//      return null;
//    StringBuilder msg = new StringBuilder();
//    StringBuilder ixb = new StringBuilder();
//    boolean fillMode = false;
//    for (char c : message.toCharArray()) {
//      if (c == '}') {
//        if (ixb.length() == 0) {
//          msg.append(c);
//          continue;
//        }
//        int _ix = Integer.parseInt(ixb.toString());
//        if (_ix + 1 > formats.length) {
//          msg.append("{").append(_ix).append("}");
//        } else {
//          msg.append(formats[_ix]);
//        }
//        ixb.delete(0, ixb.length());
//        fillMode = false;
//        continue;
//      }
//
//      if (!fillMode) {
//        if (c == '{') {
//          fillMode = true;
//          continue;
//        } else {
//          msg.append(c);
//          continue;
//        }
//      }
//
//      if (!Integer.parseInt(String.valueOf(c), false)) {
//        msg.append('{').append(c);
//        ixb.delete(0, ixb.length());
//        fillMode = false;
//        continue;
////        throw new IllegalArgumentException(EnoaTipKit.message("eo.tip.toolkit.text_format_cant_parse_arg", message));
//      }
//      ixb.append(c);
//    }
//    ixb.delete(0, ixb.length());
//    return msg.toString();
    return MessageFormat.format(message, formats);
  }


}
