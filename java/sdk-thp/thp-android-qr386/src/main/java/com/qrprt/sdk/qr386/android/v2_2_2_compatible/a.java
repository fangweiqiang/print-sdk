//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.qrprt.sdk.qr386.android.v2_2_2_compatible;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.SystemClock;
import android.util.Log;
import com.qrprt.sdk.qr386.android.QRBluetoothPort;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

class a implements QRBluetoothPort {
  private BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
  private BluetoothSocket mmBtSocket;
  private byte[] _cmd = new byte[]{0};
  public boolean isOpen = false;
  private String f;
  private OutputStream g = null;
  private InputStream h = null;
  private String i = "";

  public a() {
  }

  public String a() {
    return this.i;
  }

  protected void finalize() throws Throwable {
    super.finalize();
    this.b();
  }

  public boolean a(int var1) {
    for (int var2 = var1 / 50; var2 > 0; --var2) {
      int var3 = this.btAdapter.getState();
      if (var3 == 12) {
        return true;
      }

      try {
        Thread.sleep(50L);
      } catch (InterruptedException var5) {
      }
    }

    return false;
  }

  public boolean a(String var1, int var2) {
    this.isOpen = false;
    if (var1 == null) {
      return false;
    } else {
      this.btAdapter = BluetoothAdapter.getDefaultAdapter();
      this.f = var1;
      if (var2 < 1000) {
        var2 = 1000;
      }

      if (var2 > 6000) {
        var2 = 6000;
      }

      long var3 = SystemClock.elapsedRealtime();

      while (12 != this.btAdapter.getState()) {
        if (SystemClock.elapsedRealtime() - var3 > (long) var2) {
          Log.e("PP", "adapter state on timeout");
          return false;
        }

        try {
          Thread.sleep(200L);
        } catch (InterruptedException var13) {
          var13.printStackTrace();
          return false;
        }
      }

      BluetoothSocket var5 = null;

      try {
        BluetoothDevice var6 = this.btAdapter.getRemoteDevice(this.f);
        var5 = var6.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
        this.i = var6.getName();
      } catch (Exception var12) {
        var5 = null;
        Log.e("PP", "createRfcommSocketToServiceRecord exception");
        this.isOpen = false;
        return false;
      }

      this.mmBtSocket = var5;
      var3 = SystemClock.elapsedRealtime();

      try {
        this.mmBtSocket.connect();
      } catch (Exception var14) {
        var14.printStackTrace();
        Log.e("PP", "connect exception");
        if (SystemClock.elapsedRealtime() - var3 > (long) var2) {
          try {
            this.mmBtSocket.close();
          } catch (IOException var8) {
            var8.printStackTrace();
          }

          this.isOpen = false;
          Log.e("PP", "connet timeout");
          return false;
        }

        try {
          Thread.sleep(200L);
        } catch (InterruptedException var9) {
          var9.printStackTrace();
        }

        return false;
      }

      try {
        this.g = this.mmBtSocket.getOutputStream();
      } catch (IOException var11) {
        var11.printStackTrace();
        return false;
      }

      try {
        this.h = this.mmBtSocket.getInputStream();
      } catch (IOException var10) {
        var10.printStackTrace();
        return false;
      }

      this.isOpen = true;
      Log.e("PP", "connect ok");
      return true;
    }
  }

  public boolean b() {
    if (this.mmBtSocket == null) {
      this.isOpen = false;
      Log.e("PP", "mmBtSocket null");
      return false;
    } else {
      if (this.isOpen) {
        try {
          if (this.g != null) {
            this.g.close();
            this.g = null;
          }

          if (this.h != null) {
            this.h.close();
            this.g = null;
          }

          this.mmBtSocket.close();
          Log.e("PP", "close success");
        } catch (Exception var2) {
          this.isOpen = false;
          Log.e("PP", "close exception");
          return false;
        }
      }

      this.isOpen = false;
      this.mmBtSocket = null;
      return true;
    }
  }

  public boolean c() {
    byte[] var1 = new byte[64];
    if (!this.isOpen) {
      return false;
    } else {
      while (true) {
        boolean var2 = false;

        try {
          int var3 = this.h.available();
          if (var3 == 0) {
            return true;
          }

          if (var3 > 0) {
            if (var3 > 64) {
              var3 = 64;
            }

            this.h.read(var1, 0, var3);
          }
        } catch (IOException var5) {
        }

        try {
          Thread.sleep(10L);
        } catch (InterruptedException var4) {
        }
      }
    }
  }

  public boolean a(byte[] var1, int var2, int var3) {
    if (!this.isOpen) {
      return false;
    } else if (this.mmBtSocket == null) {
      Log.e("PP", "mmBtSocket null");
      return false;
    } else if (this.g == null) {
      Log.e("PP", "mmOutStream null");
      return false;
    } else {
      try {
        this.g.write(var1, var2, var3);
        return true;
      } catch (Exception var5) {
        return false;
      }
    }
  }

  public boolean d() {
    this._cmd[0] = 0;
    return this.a(this._cmd, 0, 1);
  }

  public boolean a(char[] var1, int var2, int var3, int var4) {
    int var5;
    if (var4 == 1) {
      for (var5 = var2; var5 < var2 + var3; ++var5) {
        this._cmd[0] = (byte) var1[var5];
        this.a(this._cmd, 0, 1);
      }
    } else {
      for (var5 = var2; var5 < var2 + var3; ++var5) {
        this._cmd[0] = (byte) var1[var5];
        this._cmd[1] = (byte) (var1[var5] >> 8);
        this.a(this._cmd, 0, 2);
      }
    }

    return true;
  }

  public boolean a(String var1) {
    Object var2 = null;

    byte[] var3;
    try {
      var3 = var1.getBytes("GBK");
    } catch (UnsupportedEncodingException var5) {
      Log.e("PP", "Sting getBytes('GBK') failed");
      return false;
    }

    return !this.a(var3, 0, var3.length) ? false : this.d();
  }

  public boolean a(byte[] var1) {
    byte[] var2 = new byte[4];
    boolean var3 = true;
    int var4 = 0;

    for (int var5 = 0; var5 < 72; ++var5) {
      var4 += (short) var1[var5];
    }

    var2[0] = 31;
    var2[1] = -103;
    var2[2] = (byte) (var4 & 255);
    var2[3] = (byte) ((var4 & '\uff00') >> 8);
    var3 = this.a(var2, 0, 4);
    if (!var3) {
      return false;
    } else {
      var3 = this.a(var1, 0, 72);
      if (!var3) {
        return false;
      } else {
        byte[] var6 = new byte[3];
        return this.b(var6, 3, 500) ? (var6[0] == 31 && var6[1] == 153 && var6[2] == 0 ? true : (var6[0] == 31 && var6[1] == 153 && var6[2] == 1 ? false : false)) : false;
      }
    }
  }

  public boolean a(byte[] var1, int var2, int var3, int var4) {
    if (!this.isOpen) {
      return false;
    } else {
      if (var4 < 200) {
        var4 = 200;
      }

      if (var4 > 5000) {
        var4 = 5000;
      }

      try {
        long var5 = SystemClock.elapsedRealtime();
        long var7 = 0L;
        int var9 = var3;
        boolean var10 = false;

        while (true) {
          if (this.h.available() > 0) {
            int var12 = this.h.read(var1, var2, var9);
            var2 += var12;
            var9 -= var12;
          }

          if (var9 == 0) {
            return true;
          }

          var7 = SystemClock.elapsedRealtime();
          if (var7 - var5 > (long) var4) {
            return false;
          }

          Thread.sleep(20L);
        }
      } catch (Exception var11) {
        return false;
      }
    }
  }

  public boolean b(byte[] var1, int var2, int var3) {
    return this.a((byte[]) var1, 0, var2, var3);
  }

  public int e() {
    try {
      return this.h.available();
    } catch (IOException var2) {
      Log.e("PP", "read exception");
      this.b();
      return 0;
    }
  }
}
