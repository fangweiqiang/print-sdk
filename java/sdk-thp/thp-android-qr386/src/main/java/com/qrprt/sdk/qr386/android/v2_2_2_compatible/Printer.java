package com.qrprt.sdk.qr386.android.v2_2_2_compatible;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.util.Log;
import com.qrprt.sdk.api.ARaw;
import com.qrprt.sdk.qr386.android.QR386Android;

import java.io.UnsupportedEncodingException;

class Printer {
  private final int c = 19;
  private QR386Android qr386;
  private a d;
  private int e = 3000;
  private int f = 24;
  private int g = 55;
  private int h = 40;
  private boolean i = false;
  private int j = 0;
  private static int k;
  public static final String b = "2.2.2_20180320";

  public Printer(QR386Android qr386) {
    this.qr386 = qr386;
    this.d = (a) qr386.bluetoothPort();
  }

  public boolean a(String var1) {
    this.i = false;
    this.j = 0;
    String var2 = this.d.a();
    if (!a((CharSequence) var1)) {
      if (this.d.isOpen) {
        this.d.b();
      }

      if (!this.d.a(var1, this.e)) {
        return false;
      } else {
        var2 = this.d.a();
        if (a((CharSequence) var2)) {
          this.i = false;
          if (this.d.isOpen) {
            this.d.b();
          }

          return false;
        } else if (!var2.contains("380") && this.g() > 19) {
          if (this.qr386._CheckPrinter(var2)) {
            if (this.c(var1)) {
              this.i = true;
              return true;
            } else {
              this.i = false;
              if (this.d.isOpen) {
                this.d.b();
              }

              return false;
            }
          } else {
            this.i = false;
            if (this.d.isOpen) {
              this.d.b();
            }

            return false;
          }
        } else if (this.d(var1)) {
          this.i = true;
          this.j = 1;
          return true;
        } else {
          this.i = false;
          if (this.d.isOpen) {
            this.d.b();
          }

          return false;
        }
      }
    } else {
      this.i = false;
      return false;
    }
  }

  public boolean a(String var1, String var2) {
    this.i = false;
    this.j = 0;
    if (!a((CharSequence) var2) && !a((CharSequence) var1)) {
      if (this.d.isOpen) {
        this.d.b();
      }

      this.d.a(var2, this.e);
      if (!var1.contains("380") && this.g() > 19) {
        if (this.qr386._CheckPrinter(var1)) {
          if (this.c(var2)) {
            this.i = true;
            return true;
          } else {
            this.i = false;
            if (this.d.isOpen) {
              this.d.b();
            }

            return false;
          }
        } else {
          this.i = false;
          if (this.d.isOpen) {
            this.d.b();
          }

          return false;
        }
      } else if (this.d(var2)) {
        this.i = true;
        this.j = 1;
        return true;
      } else {
        this.i = false;
        if (this.d.isOpen) {
          this.d.b();
        }

        return false;
      }
    } else {
      this.i = false;
      return false;
    }
  }

  public boolean b(String var1, String var2) {
    if (!a((CharSequence) var2) && !a((CharSequence) var1)) {
      if (this.d.isOpen) {
        this.d.b();
      }

      this.d.a(var2, this.e);
      if (this.g() <= 19) {
        this.i = true;
        return true;
      } else if (this.qr386._CheckPrinter(var1)) {
        if (this.c(var2)) {
          this.i = true;
          return true;
        } else {
          this.i = false;
          return false;
        }
      } else {
        this.i = false;
        return false;
      }
    } else {
      this.i = false;
      return false;
    }
  }

  private static boolean a(CharSequence var0) {
    return var0 == null || var0.length() == 0;
  }

  public void a() {
    if (this.d.isOpen) {
      try {
        Thread.sleep(100L);
      } catch (InterruptedException var2) {
        var2.printStackTrace();
      }

      this.d.b();
    }

  }

  public boolean b() {
    return this.d.isOpen;
  }

  private Bitmap f() {
    return null;
  }

//  boolean b(String var1) {
//    if (var1 != null) {
//      byte[] var2;
//      try {
//        var2 = var1.getBytes("GB2312");
//      } catch (UnsupportedEncodingException var4) {
//        return false;
//      }
//
//      return this.a(var2);
//    } else {
//      return false;
//    }
//  }
//
//  boolean a(byte[] var1) {
//    if (this.d.a) {
//      int var2 = var1.length;
//
//      while (true) {
//        try {
//          Thread.sleep(1L);
//        } catch (InterruptedException var4) {
//          var4.printStackTrace();
//        }
//
//        if (var2 <= 10) {
//          return this.d.a(var1, var1.length - var2, var2);
//        }
//
//        if (!this.d.a(var1, var1.length - var2, 10)) {
//          return false;
//        }
//
//        var2 -= 10;
//      }
//    } else {
//      return false;
//    }
//  }

  boolean portSendCmd() {
    try {
      if (this.d.isOpen) {
        byte[] data1 = this.qr386.command().binary();
        int len = data1.length;
        while (true) {
          try {
            Thread.sleep(1L);
          } catch (InterruptedException var4) {
            var4.printStackTrace();
          }

          if (len <= 10) {
            return this.d.a(data1, data1.length - len, len);
          }

          if (!this.d.a(data1, data1.length - len, 10)) {
            return false;
          }

          len -= 10;
        }
      }
      return false;
    } finally {
      this.qr386.clear();
    }
  }

  public String a(int var1, int var2) {
    if (!this.i) {
      return "Invalid Device";
    } else {
      String var3;
      if (this.j == 1) {
        if (var2 > 0) {
        }
      } else if (var2 > 0) {
        var3 = "GAP-SENSE\r\nFORM\r\n";
//        this.b(var3);
        this.qr386.raw(ARaw.builder().command(var3).newline(false).build());
      }

      if (var1 == 0) {
        var3 = "PRINT\r\n";
//        this.b(var3);
        this.qr386.raw(ARaw.builder().command(var3).newline(false).build());
      } else {
        var3 = "POPRINT\r\n";
//        this.b(var3);
        this.qr386.raw(ARaw.builder().command(var3).newline(false).build());
      }

      if (this.j == 1 && var2 > 0) {
        byte[] var4 = new byte[]{14};
//        this.a(var4);
        this.qr386.raw(ARaw.builder().command(var4).newline(false).build());
      }

      return "Ok";
    }
  }

  public void b(int var1, int var2) {
    String var3 = "! 0 200 200 " + String.valueOf(var2) + " 1\r\nPAGE-WIDTH " + var1 + "\r\n";
//    this.b(var3);
    this.qr386.raw(ARaw.builder().command(var3).newline(false).build());
  }

  public void a(int var1, int var2, int var3, int var4, int var5) {
    if (var2 > 575) {
      var2 = 575;
    }

    if (var4 > 575) {
      var4 = 575;
    }

    String var6 = "BOX " + String.valueOf(var2) + " " + var3 + " " + var4 + " " + var5 + " " + var1 + "\r\n";
//    this.b(var6);
    this.qr386.raw(ARaw.builder().command(var6).newline(false).build());
  }

  public void a(int var1, int var2, int var3, int var4, int var5, boolean var6) {
    if (var2 > 575) {
      var2 = 575;
    }

    if (var4 > 575) {
      var4 = 575;
    }

    String var7;
    if (var6) {
      var7 = "LINE " + String.valueOf(var2) + " " + var3 + " " + var4 + " " + var5 + " " + var1 + "\r\n";
    } else {
      var7 = "LPLINE " + String.valueOf(var2) + " " + var3 + " " + var4 + " " + var5 + " " + var1 + "\r\n";
    }

//    this.b(var7);
    this.qr386.raw(ARaw.builder().command(var7).newline(false).build());
  }

  public void a(int var1, int var2, String var3, int var4, int var5, int var6, boolean var7, boolean var8) {
    String var9;
    if (var8) {
      var9 = "UNDERLINE ON\r\n";
    } else {
      var9 = "UNDERLINE OFF\r\n";
    }

//    this.b(var9);
    this.qr386.raw(ARaw.builder().command(var9).newline(false).build());
    var9 = "SETBOLD " + var6 + "\r\n";
//    this.b(var9);
    this.qr386.raw(ARaw.builder().command(var9).newline(false).build());
    boolean var10 = false;
    byte var11 = 0;
    byte var12 = 1;
    byte var13 = 1;
    byte var14 = 24;
    int var23;
    if (this.j == 1) {
      switch (var4) {
        case 1:
          var23 = 55;
          break;
        case 2:
          var23 = this.f;
          break;
        case 3:
          var23 = 4;
          break;
        case 4:
          var23 = this.f;
          var12 = 2;
          var13 = 2;
          break;
        case 5:
          var23 = 4;
          var12 = 2;
          var13 = 2;
          break;
        case 6:
          var23 = this.f;
          var12 = 3;
          var13 = 3;
          break;
        case 7:
          var23 = 4;
          var12 = 3;
          var13 = 3;
          break;
        case 8:
          var23 = 3;
          var11 = 1;
          var12 = 1;
          var13 = 1;
          break;
        case 9:
          var23 = 4;
          var11 = 3;
          var12 = 1;
          var13 = 1;
          break;
        default:
          var23 = this.g;
      }
    } else {
      switch (var4) {
        case 1:
          var23 = 55;
          var14 = 8;
          break;
        case 2:
          var23 = this.f;
          var14 = 24;
          break;
        case 3:
          var23 = 4;
          var14 = 32;
          break;
        case 4:
          var23 = this.f;
          var14 = 48;
          var12 = 2;
          var13 = 2;
          break;
        case 5:
          var23 = 4;
          var14 = 64;
          var12 = 2;
          var13 = 2;
          break;
        case 6:
          var23 = this.f;
          var14 = 72;
          var12 = 3;
          var13 = 3;
          break;
        case 7:
          var23 = 4;
          var14 = 96;
          var12 = 3;
          var13 = 3;
          break;
        case 8:
          var23 = 6;
          var14 = 20;
          var12 = 1;
          var13 = 1;
          break;
        case 9:
          var23 = 7;
          var14 = 56;
          var11 = 3;
          var12 = 1;
          var13 = 1;
          break;
        default:
          var23 = this.g;
      }
    }

    var9 = "SETMAG " + var12 + " " + var13 + "\r\n";
//    this.b(var9);
    this.qr386.raw(ARaw.builder().command(var9).newline(false).build());
    String var15 = "TR";
    switch (var5) {
      case 1:
        var9 = "TEXT90 ";
        var15 = "TR90 ";
        break;
      case 2:
        var9 = "TEXT180 ";
        var15 = "TR180 ";
        break;
      case 3:
        var9 = "TEXT270 ";
        var15 = "TR270 ";
        break;
      default:
        var9 = "TEXT ";
        var15 = "TR ";
    }

    if (var7) {
      if (this.j == 1) {
        if (var4 == 1) {
          var9 = var15 + var23 + " " + var11 + " " + var1 + " " + var2 + " " + var3 + "\r\n";
        }

        if (var4 == 2) {
          var9 = var15 + var23 + " " + var11 + " " + var1 + " " + var2 + " " + var3 + "\r\n";
        }

        if (var4 == 3) {
          var9 = var15 + var23 + " " + var11 + " " + var1 + " " + var2 + " " + var3 + "\r\n";
        }

        if (var4 == 4) {
          var9 = var15 + var23 + " " + var11 + " " + var1 + " " + var2 + " " + var3 + "\r\n";
        }

        if (var4 == 5) {
          var9 = var15 + var23 + " " + var11 + " " + var1 + " " + var2 + " " + var3 + "\r\n";
        }

        if (var4 == 6) {
          var9 = var15 + var23 + " " + var11 + " " + var1 + " " + var2 + " " + var3 + "\r\n";
        }

        if (var4 == 7) {
          var9 = var15 + var23 + " " + var11 + " " + var1 + " " + var2 + " " + var3 + "\r\n";
        }

//        this.b(var9);
        this.qr386.raw(ARaw.builder().command(var9).newline(false).build());
      } else {
        var9 = var9 + var23 + " " + var11 + " " + var1 + " " + var2 + " " + var3 + "\r\n";
//        this.b(var9);
        this.qr386.raw(ARaw.builder().command(var9).newline(false).build());
        int var16 = 0;

        try {
          var16 = var3.getBytes("GB2312").length;
        } catch (UnsupportedEncodingException var22) {
          var22.printStackTrace();
        }

        boolean var21 = false;
        boolean var20 = false;
        boolean var19 = false;
        boolean var18 = false;
        boolean var17 = false;
        int var24;
        int var25;
        int var26;
        int var27;
        int var28;
        switch (var5) {
          case 1:
            var24 = var1;
            var25 = var2 - var16 * (var14 / 2);
            var26 = var1 + var14;
            var27 = var2 - var16 * (var14 / 2);
            var28 = var16 * (var14 / 2);
            break;
          case 2:
            var24 = var1 - var16 * (var14 / 2);
            var25 = var2 - var14;
            var26 = var1;
            var27 = var2 - var14;
            var28 = var14;
            break;
          case 3:
            var24 = var1 - var14;
            var25 = var2;
            var26 = var1;
            var27 = var2;
            var28 = var16 * (var14 / 2);
            break;
          default:
            var24 = var1;
            var25 = var2;
            var26 = var1 + var16 * (var14 / 2);
            var27 = var2;
            var28 = var14;
        }

        var9 = "INVERSE-LINE " + String.valueOf(var24) + " " + var25 + " " + var26 + " " + var27 + " " + var28 + "\r\n";
//        this.b(var9);
        this.qr386.raw(ARaw.builder().command(var9).newline(false).build());
      }
    } else {
      var9 = var9 + var23 + " " + var11 + " " + var1 + " " + var2 + " " + var3 + "\r\n";
//      this.b(var9);
      this.qr386.raw(ARaw.builder().command(var9).newline(false).build());
    }

  }

  public void a(int var1, int var2, int var3, int var4, String var5, int var6, int var7, int var8, boolean var9, boolean var10) {
    String var11;
    if (var9) {
      var11 = "UNDERLINE ON\r\n";
    } else {
      var11 = "UNDERLINE OFF\r\n";
    }

//    this.b(var11);
    this.qr386.raw(ARaw.builder().command(var11).newline(false).build());
    var11 = "SETBOLD " + var8 + "\r\n";
//    this.b(var11);
    this.qr386.raw(ARaw.builder().command(var11).newline(false).build());
    boolean var12 = false;
    byte var13 = 0;
    byte var14 = 1;
    byte var15 = 1;
    boolean var16 = false;
    int var17 = 0;
    int var22;
    byte var23;
    if (this.j == 1) {
      switch (var6) {
        case 1:
          var22 = 55;
          var23 = 16;
          break;
        case 2:
          var22 = this.f;
          var23 = 24;
          break;
        case 3:
          var22 = 4;
          var23 = 32;
          break;
        case 4:
          var22 = this.f;
          var23 = 48;
          var14 = 2;
          var15 = 2;
          break;
        case 5:
          var22 = 4;
          var23 = 64;
          var14 = 2;
          var15 = 2;
          break;
        case 6:
          var22 = this.f;
          var23 = 72;
          var14 = 3;
          var15 = 3;
          break;
        case 7:
          var22 = 4;
          var23 = 96;
          var14 = 3;
          var15 = 3;
          break;
        case 8:
          var22 = 3;
          var23 = 20;
          var13 = 1;
          var14 = 1;
          var15 = 1;
          break;
        case 9:
          var22 = 4;
          var23 = 56;
          var13 = 3;
          var14 = 1;
          var15 = 1;
          break;
        default:
          var22 = this.g;
          var23 = 16;
          var13 = 0;
          var14 = 1;
          var15 = 1;
      }
    } else {
      switch (var6) {
        case 1:
          var22 = 55;
          var23 = 16;
          break;
        case 2:
          var22 = this.f;
          var23 = 24;
          break;
        case 3:
          var22 = 4;
          var23 = 32;
          break;
        case 4:
          var22 = this.f;
          var23 = 48;
          var14 = 2;
          var15 = 2;
          break;
        case 5:
          var22 = 4;
          var23 = 64;
          var14 = 2;
          var15 = 2;
          break;
        case 6:
          var22 = this.f;
          var23 = 72;
          var14 = 3;
          var15 = 3;
          break;
        case 7:
          var22 = 4;
          var23 = 96;
          var14 = 3;
          var15 = 3;
          break;
        case 8:
          var22 = 6;
          var23 = 20;
          var13 = 1;
          var14 = 1;
          var15 = 1;
          break;
        case 9:
          var22 = 7;
          var23 = 56;
          var13 = 3;
          var14 = 1;
          var15 = 1;
          break;
        default:
          var22 = this.g;
          var23 = 16;
          var13 = 0;
          var14 = 1;
          var15 = 1;
      }
    }

    var11 = "SETMAG " + var14 + " " + var15 + "\r\n";
//    this.b(var11);
    this.qr386.raw(ARaw.builder().command(var11).newline(false).build());
    switch (var7) {
      case 1:
        var11 = "TEXT90 ";
        break;
      case 2:
        var11 = "TEXT180 ";
        break;
      case 3:
        var11 = "TEXT270 ";
        break;
      default:
        var11 = "TEXT ";
    }

    char[] var18 = var5.toCharArray();
    String var19 = "";
    String var20 = "";

    for (int var21 = 0; var21 < var18.length; ++var21) {
      if ((char) ((byte) var18[var21]) != var18[var21]) {
        var17 += var23;
        if (var17 > var3) {
          var20 = var11 + var22 + " " + var13 + " " + var1 + " " + var2 + " " + var19 + "\r\n";
//          this.b(var20);
          this.qr386.raw(ARaw.builder().command(var20).newline(false).build());
          var2 += var23 + 6;
          var17 = var23;
          var19 = String.valueOf(var18[var21]);
        } else {
          var19 = var19 + String.valueOf(var18[var21]);
        }
      } else {
        var17 += var23 / 2;
        if (var17 > var3) {
          var20 = var11 + var22 + " " + var13 + " " + var1 + " " + var2 + " " + var19 + "\r\n";
//          this.b(var20);
          this.qr386.raw(ARaw.builder().command(var20).newline(false).build());
          var2 += var23 + 6;
          var17 = var23;
          var19 = String.valueOf(var18[var21]);
        } else {
          var19 = var19 + String.valueOf(var18[var21]);
        }
      }
    }

    var20 = var11 + var22 + " " + var13 + " " + var1 + " " + var2 + " " + var19 + "\r\n";
//    this.b(var20);
    this.qr386.raw(ARaw.builder().command(var20).newline(false).build());
    if (var10) {
      var11 = "INVERSE-LINE " + String.valueOf(var1) + " " + var2 + " " + (var1 + var3) + " " + (var2 + var23) + " " + var4 + "\r\n";
//      this.b(var11);
      this.qr386.raw(ARaw.builder().command(var11).newline(false).build());
    }

  }

  public void a(int var1, int var2, String var3, int var4, int var5, int var6, int var7) {
    String var8 = "";
    String var9 = "";
    if (var5 != 0 && var5 != 2) {
      if (var5 != 1 && var5 != 3) {
        var8 = "B";
      } else {
        var8 = "VB";
      }
    } else {
      var8 = "B";
    }

    switch (var4) {
      case 0:
        var9 = "39";
        break;
      case 1:
        var9 = "128";
        break;
      case 2:
        var9 = "93";
        break;
      case 3:
        var9 = "CODABAR";
        break;
      case 4:
        var9 = "EAN8";
        break;
      case 5:
        var9 = "EAN13";
        break;
      case 6:
        var9 = "UPCA";
        break;
      case 7:
        var9 = "UPCE";
        break;
      case 8:
        var9 = "I2OF5";
        break;
      default:
        var9 = "128";
    }

    String var10 = var8 + " " + var9 + " " + (var6 - 1) + " 2 " + var7 + " " + var1 + " " + var2 + " " + var3 + "\r\n";
//    this.b(var10);
    this.qr386.raw(ARaw.builder().command(var10).newline(false).build());
  }

  public void a(int var1, int var2, String var3, int var4, int var5, int var6) {
    String var7 = "SETQRVER " + var6 + "\n";
//    this.b(var7);
    this.qr386.raw(ARaw.builder().command(var7).newline(false).build());
    String var8 = "";
    String var9 = "";
    if (var4 != 0 && var4 != 2) {
      if (var4 != 1 && var4 != 3) {
        var8 = "B";
      } else {
        var8 = "VB";
      }
    } else {
      var8 = "B";
    }

    if (var5 < 2) {
      var5 = 2;
    }

    if (var5 > 20) {
      var5 = 20;
    }

    switch (var6) {
      case 0:
        var9 = "L";
        break;
      case 1:
        var9 = "M";
        break;
      case 2:
        var9 = "Q";
        break;
      default:
        var9 = "H";
    }

    String var10 = var8 + " QR " + var1 + " " + var2 + " M 2 U " + var5 + "\n" + var9 + "A," + var3 + "\nENDQR\r\n";
//    this.b(var10);
    this.qr386.raw(ARaw.builder().command(var10).newline(false).build());
  }

  private String b(byte[] var1) {
    String var2 = "";

    for (int var3 = 0; var3 < var1.length; ++var3) {
      String var4 = Integer.toHexString(var1[var3] & 255);
      if (var4.length() == 1) {
        var4 = '0' + var4;
      }

      var2 = var2 + var4;
    }

    return var2;
  }

  public void a(int var1, int var2, int var3, int var4, Bitmap var5) {
    k = 0;
    int var7 = (var3 - 1) / 8 + 1;
    int var8 = var4;
    String var9 = "";
    byte[] var10 = new byte[var7 * var4];
    int var11 = 0;
    int var12 = 0;
    int var13 = 0;
    int var14 = 0;

    while (true) {
      do {
        if (var14 >= var4) {
          return;
        }

        for (int var15 = 0; var15 < var3; ++var15) {
          var12 = var14 * var7 + var15 / 8;
          if ((var5.getPixel(var15, var14) & 16777215) < 3092271) {
            var10[var12] = (byte) (var10[var12] | 128 >> var15 % 8);
          } else {
            var10[var12] = (byte) (var10[var12] & ~(128 >> var15 % 8));
          }
        }

        ++var14;
      } while ((var14 - var11) * var7 < 1024 && var14 != var8);

      var9 = this.b(this.a(var10, var13, var12 - var13 + 1));
      var13 = var12 + 1;
      String var18 = "EG " + var7 + " " + (var14 - var11) + " " + var1 + " " + (var2 + var11) + " " + var9 + "\r\n";
//      this.b(var18);
      this.qr386.raw(ARaw.builder().command(var18).newline(false).build());
      var11 = var14;
      ++k;
    }
  }

  public void b(int var1, int var2, int var3, int var4, Bitmap var5) {
    String var7 = "";
    int var6;
    if (var3 % 8 == 0) {
      var6 = var3 / 8;
    } else {
      var6 = var3 / 8 + 1;
    }

    if (!(var6 > 999 | var4 > 65535)) {
      byte[] var8 = this.a(var5, var3, var4);
      var7 = "CG " + var6 + " " + var4 + " " + var1 + " " + var2 + " ";
      String var9 = "\r\n\r\n";
//      this.b(var7);
//      this.a(var8);
//      this.b(var9);
      this.qr386.raw(ARaw.builder().command(var7).newline(false).build());
      this.qr386.raw(ARaw.builder().command(var8).newline(false).build());
      this.qr386.raw(ARaw.builder().command(var9).newline(false).build());
    }
  }

  private byte[] a(Bitmap var1, int var2, int var3) {
    boolean var4 = false;
    int var5 = 0;
    int var6 = 0;

    try {
      int var18 = var2 % 8 == 0 ? var2 / 8 : var2 / 8 + 1;
      int var7 = var3 * var18;
      byte[] var8 = new byte[var7];

      for (int var9 = 0; var9 < var7; ++var9) {
        var8[var9] = 0;
      }

      while (var5 < var3) {
        int[] var19 = new int[var2];
        var1.getPixels(var19, 0, var2, 0, var5, var2, 1);
        int var10 = 0;

        for (int var11 = 0; var11 < var2; ++var11) {
          ++var10;
          int var12 = var19[var11];
          if (var10 > 8) {
            var10 = 1;
            ++var6;
          }

          if (var12 != -1) {
            int var13 = 1 << 8 - var10;
            int var14 = Color.red(var12);
            int var15 = Color.green(var12);
            int var16 = Color.blue(var12);
            if ((var14 + var15 + var16) / 3 < 128) {
              var8[var6] = (byte) (var8[var6] | var13);
            }
          }
        }

        var6 = var18 * (var5 + 1);
        ++var5;
      }

      return var8;
    } catch (Exception var17) {
      var17.printStackTrace();
      return null;
    }
  }

  private byte[] a(byte[] var1, int var2, int var3) {
    byte[] var4 = new byte[var3];

    for (int var5 = 0; var5 < var3; ++var5) {
      var4[var5] = var1[var2 + var5];
    }

    return var4;
  }

  public String c() {
    if (this.d.isOpen) {
      byte[] var1 = new byte[]{16, 4, 5};
      this.d.c();
      if (!this.d.a(var1, 0, 3)) {
        return "Print Write Error";
      } else {
        byte[] var2 = new byte[2];
        if (!this.d.b(var2, 2, 2000)) {
          return "Print Read Error";
        } else if (var2[0] == 0) {
          return "OK";
        } else if (var2[0] == 79 && var2[1] == 75) {
          return "OK";
        } else if ((var2[0] & 16) != 0) {
          return "CoverOpened";
        } else if ((var2[0] & 1) != 0) {
          return "NoPaper";
        } else if ((var2[0] & 8) != 0) {
          return "Printing";
        } else {
          return (var2[0] & 4) != 0 ? "BatteryLow" : "OK";
        }
      }
    } else {
      return "Printer is disconnect";
    }
  }

  public String d() {
    return "QR";
  }

  public void e() {
    if (this.d.isOpen) {
      String var1 = "! 0 200 200 0 1\r\nPAGE-WIDTH 576\r\nGAP-SENSE\r\nFORM\r\nPRINT\r\n";
//      this.b(var1);
      this.qr386.raw(ARaw.builder().command(var1).newline(false).build());
    }

  }

  private String a(Byte var1) {
    return String.format("%02x", var1).toUpperCase();
  }

  private String c(byte[] var1) {
    StringBuilder var2 = new StringBuilder();
    int var3 = var1.length;

    for (int var4 = 0; var4 < var3; ++var4) {
      var2.append(this.a(var1[var4]));
      var2.append(" ");
    }

    return var2.toString();
  }

  private boolean c(String var1) {
    boolean var2 = false;
    if (this.d.isOpen) {
      if (this.g() <= 19) {
        return true;
      }

      byte[] var3 = new byte[4];
      this.d.c();
      int[] var4 = this.qr386._CheckValidPrinter(77175792);
      byte[] var5 = new byte[8];

      for (int var6 = 0; var6 < 8; ++var6) {
        var5[var6] = (byte) (var4[var6] & 255);
      }

      byte[] var10 = new byte[]{30, 101, 0, 0, 0, 0};

      for (int var7 = 0; var7 < 4; ++var7) {
        var10[2 + var7] = (byte) var4[var7];
      }

      boolean var11 = this.d.a(var10, 0, var10.length);
      if (var11 && this.d.b(var3, 4, 3000)) {
        int[] var8 = new int[4];

        for (int var9 = 0; var9 < 4; ++var9) {
          var8[var9] = var3[var9] & 255;
          if (var4[4 + var9] != var8[var9]) {
            Log.e("checkValidPrinter", "false");
            return false;
          }
        }

        Log.e("checkValidPrinter", "success");
        var2 = true;
      } else {
        Log.e("checkValidPrinter", "false");
      }
    }

    return var2;
  }

  private int g() {
    return VERSION.SDK_INT;
  }

  private boolean d(String var1) {
    boolean var2 = false;
    if (this.d.isOpen) {
      byte[] var3 = new byte[1];
      this.d.c();
      byte[] var4 = new byte[]{30, 100, 67};
      boolean var5 = this.d.a(var4, 0, var4.length);
      if (var5 && this.d.b(var3, 1, 3000) && var3[0] >= 48 && var3[0] <= 57) {
        var2 = true;
      }
    }

    return var2;
  }
}
