package com.qrprt.sdk.qr386.android.v2_2_2_compatible;

import android.graphics.Bitmap;
import com.qrprt.sdk.api.ARaw;
import com.qrprt.sdk.qr386.android.QR386Android;

/**
 * 原有 SDK v2.2.2 兼容方案, 是原 SDK 升级, 不想做太多改动, 可以使用此类
 * 此类如果没有大的问题将不会再做升级
 * 因此建议使用 QR386Android 类, 使用更加便捷高效的 SDK
 */
@Deprecated
public class QR386CompatibleV222 {

  private QR386Android qr386;
  private PrintPP_CPCL printer;

  private QR386CompatibleV222(QR386Android qr386) {
    this.qr386 = qr386;
    this.printer = new PrintPP_CPCL(qr386);
  }

  public static QR386CompatibleV222 use() {
    return new QR386CompatibleV222(QR386Android.with(new a()));
  }


  public boolean connect(String var1) {
    return this.printer.connect(var1);
  }

  public boolean connect(String var1, String var2) {
    return this.printer.connect(var1, var2);
  }

  public boolean connectEX(String var1, String var2) {
    return this.printer.connectEX(var1, var2);
  }

  public void disconnect() {
    this.printer.disconnect();
  }

  public boolean isConnected() {
    return this.printer.isConnected();
  }

  public boolean send() {
    return this.printer.send();
  }

  public String print(int var1, int var2) {
    return this.printer.print(var1, var2);
  }

  public void pageSetup(int var1, int var2) {
    this.printer.pageSetup(var1, var2);
  }

  public void drawBox(int var1, int var2, int var3, int var4, int var5) {
    this.printer.drawBox(var1, var2, var3, var4, var5);
  }

  public void drawLine(int var1, int var2, int var3, int var4, int var5, boolean var6) {
    this.printer.drawLine(var1, var2, var3, var4, var5, var6);
  }

  public void drawText(int var1, int var2, String var3, int var4, int var5, int var6, boolean var7, boolean var8) {
    this.printer.drawText(var1, var2, var3, var4, var5, var6, var7, var8);
  }

  public void drawText(int var1, int var2, int var3, int var4, String var5, int var6, int var7, int var8, boolean var9, boolean var10) {
    this.printer.drawText(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
  }

  public void drawBarCode(int var1, int var2, String var3, int var4, int var5, int var6, int var7) {
    this.printer.drawBarCode(var1, var2, var3, var4, var5, var6, var7);
  }

  public void drawQrCode(int var1, int var2, String var3, int var4, int var5, int var6) {
    this.printer.drawQrCode(var1, var2, var3, var4, var5, var6);
  }

  public void drawGraphic(int var1, int var2, int var3, int var4, Bitmap var5) {
    this.printer.drawGraphic(var1, var2, var3, var4, var5);
  }

  public void drawGraphic2(int var1, int var2, int var3, int var4, Bitmap var5) {
    this.printer.drawGraphic2(var1, var2, var3, var4, var5);
  }

  public String printerStatus() {
    return this.printer.printerStatus();
  }

  public String printerType() {
    return this.printer.printerType();
  }

  public void feed() {
    this.printer.feed();
  }

  public QR386CompatibleV222 variable(String name, Object value) {
    this.qr386.variable(name, value);
    return this;
  }

  public QR386CompatibleV222 append(ARaw raw) {
    this.qr386.raw(raw);
    return this;
  }

}
