package com.qrprt.sdk.qr386.android;

import com.qrprt.sdk.api.ARaw;
import com.qrprt.sdk.api.Command;
import com.qrprt.sdk.qr386.QR386;
import com.qrprt.sdk.qr386.arg.*;
import com.qrprt.sdk.qr386.arg.ii.BinaryImage;
import com.qrprt.sdk.qr386.arg.ii.HexImage;

abstract class AbstractQR386Android<T extends AbstractQR386Android> extends QR386 {

  protected AbstractQR386Android() {
    super();
  }

  @Override
  public T init(AInit arg) {
    super.init(arg);
    return (T) this;
  }

  @Override
  public T gapsense() {
    super.gapsense();
    return (T) this;
  }

  @Override
  public T print() {
    super.print();
    return (T) this;
  }

  @Override
  public T print(boolean horizontal) {
    super.print(horizontal);
    return (T) this;
  }

  @Override
  public T box(ABox arg) {
    super.box(arg);
    return (T) this;
  }

  @Override
  public T line(ALine arg) {
    super.line(arg);
    return (T) this;
  }

  @Override
  public T text(AText arg) {
    super.text(arg);
    return (T) this;
  }

  @Override
  public T barcode(ABarcode arg) {
    super.barcode(arg);
    return (T) this;
  }

  @Override
  public T qrcode(AQrCode arg) {
    super.qrcode(arg);
    return (T) this;
  }

  @Override
  public T image(AImage arg, HexImage bitmap) {
    super.image(arg, bitmap);
    return (T) this;
  }

  @Override
  public T image(AImage arg, BinaryImage bitmap) {
    super.image(arg, bitmap);
    return (T) this;
  }

  @Override
  public T raw(ARaw raw) {
    super.raw(raw);
    return (T) this;
  }

  @Override
  public T variable(String name, Object value) {
    super.variable(name, value);
    return (T) this;
  }

  @Override
  public Command command() {
    return super.command();
  }

  @Override
  public void clear() {
    super.clear();
  }

  @Override
  public String version() {
    return null;
  }
}
