package com.qrprt.sdk.qr386.android;

class QR386SO {

  public QR386SO() {}

  public static native int PrinterInit();

  public static native boolean CheckPrinter(String var0);

  public static native int[] CheckValidPrinter(int var0);

  static {
    System.loadLibrary("QR386SDK");
  }
}
