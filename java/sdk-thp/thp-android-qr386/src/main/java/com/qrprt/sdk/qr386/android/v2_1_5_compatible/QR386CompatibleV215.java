package com.qrprt.sdk.qr386.android.v2_1_5_compatible;

import android.graphics.Bitmap;
import com.qrprt.sdk.api.ARaw;
import com.qrprt.sdk.qr386.android.QR386Android;

/**
 * 原有 SDK v2.1.5 兼容方案, 是原 SDK 升级, 不想做太多改动, 可以使用此类
 * 此类如果没有大的问题将不会再做升级
 * 因此建议使用 QR386Android 类, 使用更加便捷高效的 SDK
 */
@Deprecated
public class QR386CompatibleV215 {

  private QR386Android qr386;
  private Printer printer;

  private QR386CompatibleV215(QR386Android qr386) {
    this.printer = new Printer(qr386);
  }

  public static QR386CompatibleV215 use() {
    return new QR386CompatibleV215(QR386Android.with(new V215BluetoothPort()));
  }

  public boolean connect(String name, String address) {
    return this.printer.connect(name, address);
  }

  public void disconnect() {
    this.printer.disconnect();
  }

  public boolean isConnected() {
    return this.printer.isConnected();
  }

  public Bitmap GetBitmap() {
    return this.printer.GetBitmap();
  }

  public boolean send() {
    return this.printer.portSendCmd();
  }

  public String print(int horizontal, int skip) {
    return this.printer.print(horizontal, skip);
  }

  public void pageSetup(int pageWidth, int pageHeight) {
    this.printer.pageSetup(pageWidth, pageHeight);
  }

  public void drawBox(int lineWidth, int top_left_x, int top_left_y, int bottom_right_x, int bottom_right_y) {
    this.printer.drawBox(lineWidth, top_left_x, top_left_y, bottom_right_x, bottom_right_y);
  }

  public void drawLine(int lineWidth, int start_x, int start_y, int end_x, int end_y, boolean fullline) {
    this.printer.drawLine(lineWidth, start_x, start_y, end_x, end_y, fullline);
  }

  public void drawText(int text_x, int text_y, String text, int fontSize, int rotate, int bold, boolean reverse, boolean underline) {
    this.printer.drawText(text_x, text_y, text, fontSize, rotate, bold, reverse, underline);
  }

  public void drawText(int text_x, int text_y, int width, int height, String text, int fontSize, int rotate, int bold, boolean underline, boolean reverse) {
    this.printer.drawText(text_x, text_y, width, height, text, fontSize, rotate, bold, underline, reverse);
  }

  public void drawBarCode(int start_x, int start_y, String text, int type, int rotate, int linewidth, int height) {
    this.printer.drawBarCode(start_x, start_y, text, type, rotate, linewidth, height);
  }

  public void drawQrCode(int start_x, int start_y, String text, int rotate, int ver, int lel) {
    this.printer.drawQrCode(start_x, start_y, text, rotate, ver, lel);
  }

  public void drawGraphic(int start_x, int start_y, int bmp_size_x, int bmp_size_y, Bitmap bmp) {
    this.printer.drawGraphic(start_x, start_y, bmp_size_x, bmp_size_y, bmp);
  }

  public void drawGraphic2(int start_x, int start_y, int bmp_size_x, int bmp_size_y, Bitmap bmp) {
    this.printer.drawGraphic2(start_x, start_y, bmp_size_x, bmp_size_y, bmp);
  }

  public String printerStatus() {
    return this.printer.printerStatus();
  }

  public String printerType() {
    return this.printer.printerType();
  }

  public void feed() {
    this.printer.feed();
  }

  public static String Byte2Hex(Byte inByte) {
    return Printer.Byte2Hex(inByte);
  }

  public static String ByteArrToHex(byte[] inBytArr) {
    return Printer.ByteArrToHex(inBytArr);
  }

  public boolean checkValidPrinter(String address) {
    return this.printer.checkValidPrinter(address);
  }

  public boolean checkValidPrinter2(String address) {
    return this.printer.checkValidPrinter2(address);
  }

  public QR386CompatibleV215 variable(String name, Object value) {
    this.qr386.variable(name, value);
    return this;
  }

  public QR386CompatibleV215 append(ARaw raw) {
    this.qr386.raw(raw);
    return this;
  }

}
