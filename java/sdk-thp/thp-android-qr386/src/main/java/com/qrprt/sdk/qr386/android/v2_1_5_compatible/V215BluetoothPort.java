package com.qrprt.sdk.qr386.android.v2_1_5_compatible;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.SystemClock;
import android.util.Log;
import com.qrprt.sdk.qr386.android.QRBluetoothPort;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

class V215BluetoothPort implements QRBluetoothPort {
  private BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
  private BluetoothSocket mmBtSocket;
  private byte[] _cmd = new byte[1];
  private boolean isOpen = false;
  private String btDeviceString;
  private OutputStream mmOutStream = null;
  private InputStream mmInStream = null;

  V215BluetoothPort() {
  }

  protected void finalize() throws Throwable {
    super.finalize();
    this.close();
  }

  public boolean isOpen() {
    return this.isOpen;
  }

  public boolean getBluetoothStateON(int timeout) {
    for (int loop = timeout / 50; loop > 0; --loop) {
      int r = this.btAdapter.getState();
      if (r == 12) {
        return true;
      }

      try {
        Thread.sleep(50L);
      } catch (InterruptedException ignored) {
      }
    }

    return false;
  }

  public boolean open(String strBtAddr, int timeout) {
    this.isOpen = false;
    if (strBtAddr == null) {
      return false;
    }

    this.btAdapter = BluetoothAdapter.getDefaultAdapter();
    this.btDeviceString = strBtAddr;
    if (timeout < 1000) {
      timeout = 1000;
    }

    if (timeout > 6000) {
      timeout = 6000;
    }

    long start_time = SystemClock.elapsedRealtime();

    while (12 != this.btAdapter.getState()) {
      if (SystemClock.elapsedRealtime() - start_time > (long) timeout) {
        Log.e("PP", "adapter state on timeout");
        return false;
      }

      try {
        Thread.sleep(200L);
      } catch (InterruptedException var13) {
        var13.printStackTrace();
      }
    }

    BluetoothSocket TmpSock = null;

    try {
      BluetoothDevice device = this.btAdapter.getRemoteDevice(this.btDeviceString);
      TmpSock = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
    } catch (Exception var12) {
      TmpSock = null;
      Log.e("PP", "createRfcommSocketToServiceRecord exception");
      this.isOpen = false;
      return false;
    }

    this.mmBtSocket = TmpSock;
    start_time = SystemClock.elapsedRealtime();

    while (true) {
      try {
        this.mmBtSocket.connect();
        break;
      } catch (Exception var14) {
        var14.printStackTrace();
        Log.e("PP", "connect exception");
        if (SystemClock.elapsedRealtime() - start_time > (long) timeout) {
          try {
            this.mmBtSocket.close();
          } catch (IOException var8) {
            var8.printStackTrace();
          }

          this.isOpen = false;
          Log.e("PP", "connet timeout");
          return false;
        }

        try {
          Thread.sleep(200L);
        } catch (InterruptedException var11) {
          var11.printStackTrace();
        }
      }
    }

    try {
      this.mmOutStream = this.mmBtSocket.getOutputStream();
    } catch (IOException var10) {
      var10.printStackTrace();
    }

    try {
      this.mmInStream = this.mmBtSocket.getInputStream();
    } catch (IOException var9) {
      var9.printStackTrace();
    }

    this.isOpen = true;
    Log.e("PP", "connect ok");
    return true;
  }

  public boolean close() {
    if (this.mmBtSocket == null) {
      this.isOpen = false;
      Log.e("PP", "mmBtSocket null");
      return false;
    }

    if (this.isOpen) {
      try {
        if (this.mmOutStream != null) {
          this.mmOutStream.close();
          this.mmOutStream = null;
        }

        if (this.mmInStream != null) {
          this.mmInStream.close();
          this.mmOutStream = null;
        }

        this.mmBtSocket.close();
        Log.e("PP", "close success");
      } catch (Exception var2) {
        this.isOpen = false;
        Log.e("PP", "close exception");
        return false;
      }
    }

    this.isOpen = false;
    this.mmBtSocket = null;
    return true;
  }

  public boolean flushReadBuffer() {
    byte[] buffer = new byte[64];
    if (!this.isOpen) {
      return false;
    }


    while (true) {
      boolean var2 = false;

      try {
        int r1 = this.mmInStream.available();
        if (r1 == 0) {
          return true;
        }

        if (r1 > 0) {
          if (r1 > 64) {
            r1 = 64;
          }

          this.mmInStream.read(buffer, 0, r1);
        }
      } catch (IOException ignored) {
      }

      try {
        Thread.sleep(10L);
      } catch (InterruptedException ignored) {
      }
    }
  }

  public boolean write(byte[] buffer, int offset, int length) {
    if (!this.isOpen) {
      return false;
    }
    if (this.mmBtSocket == null) {
      Log.e("PP", "mmBtSocket null");
      return false;
    }
    if (this.mmOutStream == null) {
      Log.e("PP", "mmOutStream null");
      return false;
    }

    try {
      this.mmOutStream.write(buffer, offset, length);
      return true;
    } catch (Exception var5) {
      return false;
    }
  }

  public boolean writeNULL() {
    this._cmd[0] = 0;
    return this.write(this._cmd, 0, 1);
  }

  public boolean write(char[] data, int offset, int length, int size) {
    int i;
    if (size == 1) {
      for (i = offset; i < offset + length; ++i) {
        this._cmd[0] = (byte) data[i];
        this.write(this._cmd, 0, 1);
      }
    } else {
      for (i = offset; i < offset + length; ++i) {
        this._cmd[0] = (byte) data[i];
        this._cmd[1] = (byte) (data[i] >> 8);
        this.write(this._cmd, 0, 2);
      }
    }

    return true;
  }

  public boolean write(String text) {
    Object var2 = null;

    byte[] data1;
    try {
      data1 = text.getBytes("GBK");
    } catch (UnsupportedEncodingException var5) {
      Log.e("PP", "Sting getBytes('GBK') failed");
      return false;
    }

    return this.write(data1, 0, data1.length) && this.writeNULL();
  }

  public boolean Write(byte[] buffer) {
    byte[] Cmd = new byte[4];
    boolean Result = true;
    int Sum = 0;

    for (int Rep = 0; Rep < 72; ++Rep) {
      Sum += (short) buffer[Rep];
    }

    Cmd[0] = 31;
    Cmd[1] = -103;
    Cmd[2] = (byte) (Sum & 255);
    Cmd[3] = (byte) ((Sum & '\uff00') >> 8);
    Result = this.write(Cmd, 0, 4);
    if (!Result) {
      return false;
    } else {
      Result = this.write(buffer, 0, 72);
      if (!Result) {
        return false;
      } else {
        byte[] temp = new byte[3];
        return this.read(temp, 3, 500) ?
          (temp[0] == 31 && temp[1] == 153 && temp[2] == 0 ? true : (temp[0] == 31 && temp[1] == 153 && temp[2] == 1 ? false : false)) :
          false;
      }
    }
  }

  public boolean read(byte[] buffer, int offset, int length, int timeout_read) {
    if (!this.isOpen) {
      return false;
    }

    if (timeout_read < 200) {
      timeout_read = 200;
    }

    if (timeout_read > 5000) {
      timeout_read = 5000;
    }

    try {
      long start_time = SystemClock.elapsedRealtime();
      long cur_time = 0L;
      int need_read = length;
      boolean var10 = false;

      while (true) {
        if (this.mmInStream.available() > 0) {
          int cur_readed = this.mmInStream.read(buffer, offset, need_read);
          offset += cur_readed;
          need_read -= cur_readed;
        }

        if (need_read == 0) {
          return true;
        }

        cur_time = SystemClock.elapsedRealtime();
        if (cur_time - start_time > (long) timeout_read) {
          return false;
        }

        Thread.sleep(20L);
      }
    } catch (Exception var11) {
      return false;
    }
  }

  public boolean read(byte[] buffer, int length, int timeout_read) {
    return this.read(buffer, 0, length, timeout_read);
  }

  public int readLength() {
    try {
      return this.mmInStream.available();
    } catch (IOException var2) {
      Log.e("PP", "read exception");
      this.close();
      return 0;
    }
  }
}
