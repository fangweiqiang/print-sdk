//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.qrprt.sdk.qr386.android.v2_1_5_compatible;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import com.qrprt.sdk.api.ARaw;
import com.qrprt.sdk.qr386.android.QR386Android;

import java.io.UnsupportedEncodingException;

class Printer {

  private QR386Android qr386;
  private V215BluetoothPort port;


  private int PortTimeOut = 3000;
  private int Family = 24;
  private int defaultFamily = 55;
  private int offsetdots = 40;
  private boolean isValidPrinter = false;
  private int PrinterName = 0;
  public static int pkgs;
  public static final int STATE_NOPAPER_UNMASK = 1;
  public static final int STATE_OVERHEAT_UNMASK = 2;
  public static final int STATE_BATTERYLOW_UNMASK = 4;
  public static final int STATE_PRINTING_UNMASK = 8;
  public static final int STATE_COVEROPEN_UNMASK = 16;

  public Printer(QR386Android qr386) {
    this.qr386 = qr386;
    this.port = (V215BluetoothPort) qr386.bluetoothPort();
  }

  public boolean connect(String name, String address) {
    if (!isEmpty(address) && !isEmpty(name)) {
      if (this.port.isOpen()) {
        this.port.close();
      }

      this.port.open(address, this.PortTimeOut);
      if (this.qr386._CheckPrinter(name)) {
        if (this.checkValidPrinter(address)) {
          this.isValidPrinter = true;
          return true;
        } else {
          this.isValidPrinter = false;
          this.port.close();
          return false;
        }
      } else {
        this.isValidPrinter = false;
        this.port.close();
        return false;
      }
    } else {
      this.isValidPrinter = false;
      return false;
    }
  }

  public static boolean isEmpty(CharSequence str) {
    return str == null || str.length() == 0;
  }

  public void disconnect() {
    if (this.port.isOpen()) {
      try {
        Thread.sleep(100L);
      } catch (InterruptedException var2) {
        var2.printStackTrace();
      }

      this.port.close();
    }

  }

  public boolean isConnected() {
    return this.port.isOpen();
  }

  public Bitmap GetBitmap() {
    return null;
  }

//  boolean portSendCmd() {
//    if (cmd != null) {
//      byte[] data1;
//      try {
//        data1 = cmd.getBytes("GB2312");
//      } catch (UnsupportedEncodingException var4) {
//        return false;
//      }
//
//      return this.portSendCmd(data1);
//    } else {
//      return false;
//    }
//  }

  boolean portSendCmd() {
    try {
      if (this.port.isOpen()) {
        byte[] data1 = this.qr386.command().binary();
        int len = data1.length;
        while (true) {
          try {
            Thread.sleep(1L);
          } catch (InterruptedException var4) {
            var4.printStackTrace();
          }

          if (len <= 10) {
            return this.port.write(data1, data1.length - len, len);
          }

          if (!this.port.write(data1, data1.length - len, 10)) {
            return false;
          }

          len -= 10;
        }
      }
      return false;
    } finally {
      this.qr386.clear();
    }
  }

  public String print(int horizontal, int skip) {
    if (!this.isValidPrinter) {
      return "Invalid Device";
    } else {
      String cmd;
      if (skip > 0) {
        cmd = "GAP-SENSE\r\nFORM\r\n";
//        this.portSendCmd(cmd);
        this.qr386.raw(ARaw.builder().command(cmd).newline(false).build());
      }

      if (horizontal == 0) {
        cmd = "PRINT\r\n";
//        this.portSendCmd(cmd);
        this.qr386.raw(ARaw.builder().command(cmd).newline(false).build());
      } else {
        cmd = "POPRINT\r\n";
//        this.portSendCmd(cmd);
        this.qr386.raw(ARaw.builder().command(cmd).newline(false).build());
      }

      return "Ok";
    }
  }

  public void pageSetup(int pageWidth, int pageHeight) {
    String cmd = "! 0 200 200 " + String.valueOf(pageHeight) + " " + "1" + "\r\n" + "PAGE-WIDTH" + " " + pageWidth + "\r\n";
//    this.portSendCmd(cmd);
    this.qr386.raw(ARaw.builder().command(cmd).newline(false).build());
  }

  public void drawBox(int lineWidth, int top_left_x, int top_left_y, int bottom_right_x, int bottom_right_y) {
    if (top_left_x > 575) {
      top_left_x = 575;
    }

    if (bottom_right_x > 575) {
      bottom_right_x = 575;
    }

    String CPCLCmd = "BOX " + String.valueOf(top_left_x) + " " + top_left_y + " " + bottom_right_x + " " + bottom_right_y + " " + lineWidth + "\r\n";
//    this.portSendCmd(CPCLCmd);
    this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
  }

  public void drawLine(int lineWidth, int start_x, int start_y, int end_x, int end_y, boolean fullline) {
    if (start_x > 575) {
      start_x = 575;
    }

    if (end_x > 575) {
      end_x = 575;
    }

    String CPCLCmd;
    if (fullline) {
      CPCLCmd = "LINE " + String.valueOf(start_x) + " " + start_y + " " + end_x + " " + end_y + " " + lineWidth + "\r\n";
    } else {
      CPCLCmd = "LPLINE " + String.valueOf(start_x) + " " + start_y + " " + end_x + " " + end_y + " " + lineWidth + "\r\n";
    }

//    this.portSendCmd(CPCLCmd);
    this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
  }

  public void drawText(int text_x, int text_y, String text, int fontSize, int rotate, int bold, boolean reverse, boolean underline) {
    String CPCLCmd;
    if (underline) {
      CPCLCmd = "UNDERLINE ON\r\n";
    } else {
      CPCLCmd = "UNDERLINE OFF\r\n";
    }

//    this.portSendCmd(CPCLCmd);
    this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
    CPCLCmd = "SETBOLD " + bold + "\r\n";
//    this.portSendCmd(CPCLCmd);
    this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
//    int family = false;
    int size = 0;
    int ex = 1;
    int ey = 1;
    int family;
    switch (fontSize) {
      case 1:
        family = 55;
        break;
      case 2:
        family = this.Family;
        break;
      case 3:
        family = 4;
        break;
      case 4:
        family = this.Family;
        ex = 2;
        ey = 2;
        break;
      case 5:
        family = 4;
        ex = 2;
        ey = 2;
        break;
      case 6:
        family = this.Family;
        ex = 3;
        ey = 3;
        break;
      case 7:
        family = 4;
        ex = 3;
        ey = 3;
        break;
      case 8:
        family = 3;
        size = 1;
        ex = 1;
        ey = 1;
        break;
      case 9:
        family = 4;
        size = 3;
        ex = 1;
        ey = 1;
        break;
      default:
        family = this.defaultFamily;
    }

    CPCLCmd = "SETMAG " + ex + " " + ey + "\r\n";
//    this.portSendCmd(CPCLCmd);
    this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
    switch (rotate) {
      case 1:
        CPCLCmd = "TEXT90 ";
        break;
      case 2:
        CPCLCmd = "TEXT180 ";
        break;
      case 3:
        CPCLCmd = "TEXT270 ";
        break;
      default:
        CPCLCmd = "TEXT ";
    }

    CPCLCmd = CPCLCmd + family + " " + size + " " + text_x + " " + text_y + " " + text + "\r\n";
//    this.portSendCmd(CPCLCmd);
    this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
    if (reverse) {
      int textLength = 0;

      try {
        textLength = text.getBytes("GB2312").length;
      } catch (UnsupportedEncodingException var16) {
        var16.printStackTrace();
      }

      if (fontSize == 1) {
        CPCLCmd = "INVERSE-LINE " + String.valueOf(text_x) + " " + text_y + " " + (text_x + textLength * 8) + " " + text_y + " " + 16 + "\r\n";
      }

      if (fontSize == 2) {
        CPCLCmd = "INVERSE-LINE " + String.valueOf(text_x) + " " + text_y + " " + (text_x + textLength * 12) + " " + text_y + " " + 24 + "\r\n";
      }

      if (fontSize == 3) {
        CPCLCmd = "INVERSE-LINE " + String.valueOf(text_x) + " " + text_y + " " + (text_x + textLength * 16) + " " + text_y + " " + 32 + "\r\n";
      }

      if (fontSize == 4) {
        CPCLCmd = "INVERSE-LINE " + String.valueOf(text_x) + " " + text_y + " " + (text_x + textLength * 24) + " " + text_y + " " + 48 + "\r\n";
      }

      if (fontSize == 5) {
        CPCLCmd = "INVERSE-LINE " + String.valueOf(text_x) + " " + text_y + " " + (text_x + textLength * 32) + " " + text_y + " " + 64 + "\r\n";
      }

      if (fontSize == 6) {
        CPCLCmd = "INVERSE-LINE " + String.valueOf(text_x) + " " + text_y + " " + (text_x + textLength * 48) + " " + text_y + " " + 96 + "\r\n";
      }

      if (fontSize == 7) {
        CPCLCmd = "INVERSE-LINE " + String.valueOf(text_x) + " " + text_y + " " + (text_x + textLength * 64) + " " + text_y + " " + 128 + "\r\n";
      }
    }

//    this.portSendCmd(CPCLCmd);
    this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
  }

  public void drawText(int text_x, int text_y, int width, int height, String text, int fontSize, int rotate, int bold, boolean underline, boolean reverse) {
    String CPCLCmd;
    if (underline) {
      CPCLCmd = "UNDERLINE ON\r\n";
    } else {
      CPCLCmd = "UNDERLINE OFF\r\n";
    }

//    this.portSendCmd(CPCLCmd);
    this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
    CPCLCmd = "SETBOLD " + bold + "\r\n";
//    this.portSendCmd(CPCLCmd);
    this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
//    int family = false;
    int size = 0;
    int ex = 1;
    int ey = 1;
//    int Height = false;
    int Width = 0;
    int family;
    byte Height;
    switch (fontSize) {
      case 1:
        family = 55;
        Height = 16;
        break;
      case 2:
        family = this.Family;
        Height = 24;
        break;
      case 3:
        family = 4;
        Height = 32;
        break;
      case 4:
        family = this.Family;
        Height = 48;
        ex = 2;
        ey = 2;
        break;
      case 5:
        family = 4;
        Height = 64;
        ex = 2;
        ey = 2;
        break;
      case 6:
        family = this.Family;
        Height = 72;
        ex = 3;
        ey = 3;
        break;
      case 7:
        family = 4;
        Height = 96;
        ex = 3;
        ey = 3;
        break;
      case 8:
        family = 3;
        Height = 20;
        size = 1;
        ex = 1;
        ey = 1;
        break;
      case 9:
        family = 4;
        Height = 56;
        size = 3;
        ex = 1;
        ey = 1;
        break;
      default:
        family = this.defaultFamily;
        Height = 16;
        size = 0;
        ex = 1;
        ey = 1;
    }

    CPCLCmd = "SETMAG " + ex + " " + ey + "\r\n";
//    this.portSendCmd(CPCLCmd);
    this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
    switch (rotate) {
      case 1:
        CPCLCmd = "TEXT90 ";
        break;
      case 2:
        CPCLCmd = "TEXT180 ";
        break;
      case 3:
        CPCLCmd = "TEXT270 ";
        break;
      default:
        CPCLCmd = "TEXT ";
    }

    char[] array = text.toCharArray();
    String str = "";
    String SendCmd = "";

    for (int i = 0; i < array.length; ++i) {
      if ((char) ((byte) array[i]) != array[i]) {
        Width += Height;
        if (Width > width) {
          SendCmd = CPCLCmd + family + " " + size + " " + text_x + " " + text_y + " " + str + "\r\n";
//          this.portSendCmd(SendCmd);
          this.qr386.raw(ARaw.builder().command(SendCmd).newline(false).build());
          text_y += Height + 6;
          Width = Height;
          str = String.valueOf(array[i]);
        } else {
          str = str + String.valueOf(array[i]);
        }
      } else {
        Width += Height / 2;
        if (Width > width) {
          SendCmd = CPCLCmd + family + " " + size + " " + text_x + " " + text_y + " " + str + "\r\n";
//          this.portSendCmd(SendCmd);
          this.qr386.raw(ARaw.builder().command(SendCmd).newline(false).build());
          text_y += Height + 6;
          Width = Height;
          str = String.valueOf(array[i]);
        } else {
          str = str + String.valueOf(array[i]);
        }
      }
    }

    SendCmd = CPCLCmd + family + " " + size + " " + text_x + " " + text_y + " " + str + "\r\n";
//    this.portSendCmd(SendCmd);
    this.qr386.raw(ARaw.builder().command(SendCmd).newline(false).build());
    if (reverse) {
      CPCLCmd = "INVERSE-LINE " + String.valueOf(text_x) + " " + text_y + " " + (text_x + width) + " " + (text_y + Height) + " " + height + "\r\n";
//      this.portSendCmd(CPCLCmd);
      this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
    }

  }

  public void drawBarCode(int start_x, int start_y, String text, int type, int rotate, int linewidth, int height) {
    String command = "";
    String bartype = "";
    if (rotate != 0 && rotate != 2) {
      if (rotate != 1 && rotate != 3) {
        command = "B";
      } else {
        command = "VB";
      }
    } else {
      command = "B";
    }

    switch (type) {
      case 0:
        bartype = "39";
        break;
      case 1:
        bartype = "128";
        break;
      case 2:
        bartype = "93";
        break;
      case 3:
        bartype = "CODABAR";
        break;
      case 4:
        bartype = "EAN8";
        break;
      case 5:
        bartype = "EAN13";
        break;
      case 6:
        bartype = "UPCA";
        break;
      case 7:
        bartype = "UPCE";
        break;
      case 8:
        bartype = "I2OF5";
        break;
      default:
        bartype = "128";
    }

    String stringData = command + " " + bartype + " " + (linewidth - 1) + " " + "2" + " " + height + " " + start_x + " " + start_y + " " + text + "\r\n";
//    this.portSendCmd(stringData);
    this.qr386.raw(ARaw.builder().command(stringData).newline(false).build());
  }

  public void drawQrCode(int start_x, int start_y, String text, int rotate, int ver, int lel) {
    String command = "";
    String levelText = "";
    if (rotate != 0 && rotate != 2) {
      if (rotate != 1 && rotate != 3) {
        command = "B";
      } else {
        command = "VB";
      }
    } else {
      command = "B";
    }

    if (ver < 2) {
      ver = 2;
    }

    if (ver > 6) {
      ver = 6;
    }

    switch (lel) {
      case 0:
        levelText = "L";
        break;
      case 1:
        levelText = "M";
        break;
      case 2:
        levelText = "Q";
        break;
      default:
        levelText = "H";
    }

    String stringData = command + " " + "QR" + " " + start_x + " " + start_y + " " + "M" + " " + "2" + " " + "U" + " " + ver + " " + levelText + "A," + " " + text + "\r\n" + "ENDQR\r\n\r\n";
//    this.portSendCmd(stringData);
    this.qr386.raw(ARaw.builder().command(stringData).newline(false).build());
  }

  private String printHexString(byte[] b) {
    String a = "";

    for (int i = 0; i < b.length; ++i) {
      String hex = Integer.toHexString(b[i] & 255);
      if (hex.length() == 1) {
        hex = '0' + hex;
      }

      a = a + hex;
    }

    return a;
  }

  public void drawGraphic(int start_x, int start_y, int bmp_size_x, int bmp_size_y, Bitmap bmp) {
    pkgs = 0;
    int ByteWidth = (bmp_size_x - 1) / 8 + 1;
    int ByteHeight = bmp_size_y;
    String Data = "";
    byte[] DataByte = new byte[ByteWidth * bmp_size_y];
    int offset = 0;
    int cur_idx = 0;
    int last_idx = 0;
    int i = 0;

    while (true) {
      do {
        if (i >= bmp_size_y) {
          return;
        }

        for (int j = 0; j < bmp_size_x; ++j) {
          cur_idx = i * ByteWidth + j / 8;
          if ((bmp.getPixel(j, i) & 16777215) < 3092271) {
            DataByte[cur_idx] = (byte) (DataByte[cur_idx] | 128 >> j % 8);
          } else {
            DataByte[cur_idx] = (byte) (DataByte[cur_idx] & ~(128 >> j % 8));
          }
        }

        ++i;
      } while ((i - offset) * ByteWidth < 1024 && i != ByteHeight);

      Data = this.printHexString(this.getByte(DataByte, last_idx, cur_idx - last_idx + 1));
      last_idx = cur_idx + 1;
      String CPCLCmd = "EG " + ByteWidth + " " + (i - offset) + " " + start_x + " " + (start_y + offset) + " " + Data + "\r\n";
//      this.portSendCmd(CPCLCmd);
      this.qr386.raw(ARaw.builder().command(CPCLCmd).newline(false).build());
      offset = i;
      ++pkgs;
    }
  }

  public void drawGraphic2(int start_x, int start_y, int bmp_size_x, int bmp_size_y, Bitmap bmp) {
    String command = "";
    int bytesWidth;
    if (bmp_size_x % 8 == 0) {
      bytesWidth = bmp_size_x / 8;
    } else {
      bytesWidth = bmp_size_x / 8 + 1;
    }

    if (!(bytesWidth > 999 | bmp_size_y > 65535)) {
      byte[] bmpData = this.imageProcess(bmp, bmp_size_x, bmp_size_y);
      command = "CG " + bytesWidth + " " + bmp_size_y + " " + start_x + " " + start_y + " ";
      String back = "\r\n\r\n";
//      this.portSendCmd(command);
//      this.portSendCmd(bmpData);
//      this.portSendCmd(back);
      this.qr386.raw(ARaw.builder().command(command).newline(false).build());
      this.qr386.raw(ARaw.builder().command(bmpData).newline(false).build());
      this.qr386.raw(ARaw.builder().command(back).newline(false).build());
    }
  }

  private byte[] imageProcess(Bitmap bitmap, int width, int height) {
//    int bytesWidth = false;
    int j = 0;
    int index = 0;

    try {
      int bytesWidth = width % 8 == 0 ? width / 8 : width / 8 + 1;
      int byteSize = height * bytesWidth;
      byte[] bmpData = new byte[byteSize];

      for (int i = 0; i < byteSize; ++i) {
        bmpData[i] = 0;
      }

      while (j < height) {
        int[] tempArray = new int[width];
        bitmap.getPixels(tempArray, 0, width, 0, j, width, 1);
        int indexLine = 0;

        for (int i = 0; i < width; ++i) {
          ++indexLine;
          int pixel = tempArray[i];
          if (indexLine > 8) {
            indexLine = 1;
            ++index;
          }

          if (pixel != -1) {
            int temp = 1 << 8 - indexLine;
            int red = Color.red(pixel);
            int green = Color.green(pixel);
            int blue = Color.blue(pixel);
            if ((red + green + blue) / 3 < 128) {
              bmpData[index] = (byte) (bmpData[index] | temp);
            }
          }
        }

        index = bytesWidth * (j + 1);
        ++j;
      }

      return bmpData;
    } catch (Exception var17) {
      var17.printStackTrace();
      return null;
    }
  }

  private byte[] getByte(byte[] dataByte, int start, int length) {
    byte[] newDataByte = new byte[length];

    for (int i = 0; i < length; ++i) {
      newDataByte[i] = dataByte[start + i];
    }

    return newDataByte;
  }

  public String printerStatus() {
    if (this.port.isOpen()) {
      byte[] Cmd = new byte[]{16, 4, 5};
      this.port.flushReadBuffer();
      if (!this.port.write(Cmd, 0, 3)) {
        return "Print Write Error";
      } else {
        byte[] Rep = new byte[2];
        if (!this.port.read(Rep, 2, 2000)) {
          return "Print Read Error";
        } else if (Rep[0] == 0) {
          return "OK";
        } else if (Rep[0] == 79 && Rep[1] == 75) {
          return "OK";
        } else if ((Rep[0] & 16) != 0) {
          return "CoverOpened ";
        } else if ((Rep[0] & 1) != 0) {
          return "NoPaper";
        } else if ((Rep[0] & 8) != 0) {
          return "Printing";
        } else {
          return (Rep[0] & 4) != 0 ? "BatteryLow" : "OK";
        }
      }
    } else {
      return "Printer is disconnect";
    }
  }

  public String printerType() {
    return "QR";
  }

  public void feed() {
    if (this.port.isOpen()) {
      String stringData = "! 0 200 200 0 1\r\nPAGE-WIDTH 576\r\nGAP-SENSE\r\nFORM\r\nPRINT\r\n";
//      this.portSendCmd(stringData);
      this.qr386.raw(ARaw.builder().command(stringData).newline(false).build());
    }

  }

  public static String Byte2Hex(Byte inByte) {
    return String.format("%02x", inByte).toUpperCase();
  }

  public static String ByteArrToHex(byte[] inBytArr) {
    StringBuilder strBuilder = new StringBuilder();
    int j = inBytArr.length;

    for (int i = 0; i < j; ++i) {
      strBuilder.append(Byte2Hex(inBytArr[i]));
      strBuilder.append(" ");
    }

    return strBuilder.toString();
  }

  public boolean checkValidPrinter(String address) {
    boolean result = false;
    if (this.port.isOpen()) {
      byte[] Rep = new byte[4];
      this.port.flushReadBuffer();
      int[] data = this.qr386._CheckValidPrinter(77175792);
      byte[] test = new byte[8];

      for (int i = 0; i < 8; ++i) {
        test[i] = (byte) (data[i] & 255);
      }

      byte[] sdata = new byte[]{30, 101, 0, 0, 0, 0};

      for (int i = 0; i < 4; ++i) {
        sdata[2 + i] = (byte) data[i];
      }

      boolean writeResult = this.port.write(sdata, 0, sdata.length);
      if (writeResult && this.port.read(Rep, 4, 3000)) {
        int[] revdata = new int[4];

        for (int j = 0; j < 4; ++j) {
          revdata[j] = Rep[j] & 255;
          if (data[4 + j] != revdata[j]) {
            Log.e("checkValidPrinter", "false");
            return false;
          }
        }

        Log.e("checkValidPrinter", "success");
        result = true;
      } else {
        Log.e("checkValidPrinter", "false");
      }
    }

    return result;
  }

  public boolean checkValidPrinter2(String address) {
    boolean result = false;
    if (this.port.isOpen()) {
      byte[] Rep = new byte[1];
      this.port.flushReadBuffer();
      byte[] cmdJni = new byte[]{30, 100, 67};
      boolean writeResult = this.port.write(cmdJni, 0, cmdJni.length);
      if (writeResult && this.port.read(Rep, 1, 3000) && Rep[0] >= 48 && Rep[0] <= 57) {
        result = true;
      }
    }

    return result;
  }
}
