//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.qrprt.sdk.qr386.android.v2_2_2_compatible;

import android.graphics.Bitmap;
import com.qrprt.sdk.qr386.android.QR386Android;

class PrintPP_CPCL {
  public static final String Version = "2.2.2_20180320";
  private Printer printPP;

  public PrintPP_CPCL(QR386Android qr386) {
    this.printPP = new Printer(qr386);
  }

  public boolean connect(String var1) {
    return this.printPP.a(var1);
  }

  public boolean connect(String var1, String var2) {
    return this.printPP.a(var1, var2);
  }

  public boolean connectEX(String var1, String var2) {
    return this.printPP.b(var1, var2);
  }

  public void disconnect() {
    this.printPP.a();
  }

  public boolean isConnected() {
    return this.printPP.b();
  }

//  boolean portSendCmd(String var1) {
//    return this.printPP.b(var1);
//  }
//
//  boolean portSendCmd(byte[] var1) {
//    return this.printPP.a(var1);
//  }

  public boolean send() {
    return this.printPP.portSendCmd();
  }

  public String print(int var1, int var2) {
    return this.printPP.a(var1, var2);
  }

  public void pageSetup(int var1, int var2) {
    this.printPP.b(var1, var2);
  }

  public void drawBox(int var1, int var2, int var3, int var4, int var5) {
    this.printPP.a(var1, var2, var3, var4, var5);
  }

  public void drawLine(int var1, int var2, int var3, int var4, int var5, boolean var6) {
    this.printPP.a(var1, var2, var3, var4, var5, var6);
  }

  public void drawText(int var1, int var2, String var3, int var4, int var5, int var6, boolean var7, boolean var8) {
    this.printPP.a(var1, var2, var3, var4, var5, var6, var7, var8);
  }

  public void drawText(int var1, int var2, int var3, int var4, String var5, int var6, int var7, int var8, boolean var9, boolean var10) {
    this.printPP.a(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
  }

  public void drawBarCode(int var1, int var2, String var3, int var4, int var5, int var6, int var7) {
    this.printPP.a(var1, var2, var3, var4, var5, var6, var7);
  }

  public void drawQrCode(int var1, int var2, String var3, int var4, int var5, int var6) {
    this.printPP.a(var1, var2, var3, var4, var5, var6);
  }

  public void drawGraphic(int var1, int var2, int var3, int var4, Bitmap var5) {
    this.printPP.a(var1, var2, var3, var4, var5);
  }

  public void drawGraphic2(int var1, int var2, int var3, int var4, Bitmap var5) {
    this.printPP.b(var1, var2, var3, var4, var5);
  }

  public String printerStatus() {
    return this.printPP.c();
  }

  public String printerType() {
    return this.printPP.d();
  }

  public void feed() {
    this.printPP.e();
  }
}
