package com.qrprt.sdk.qr386.android;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.SystemClock;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

public class BluetoothPort implements QRBluetoothPort {

  private BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
  private BluetoothSocket mmBtSocket;
  private byte[] _cmd = new byte[]{0};
  public boolean isOpen = false;
  private String btDeviceString;
  private OutputStream mmOutStream = null;
  private InputStream mmInStream = null;
  private String deviceName = "";

  public BluetoothPort() {
  }

  public boolean isOpen() {
    return this.isOpen;
  }

  public String deviceName() {
    return this.deviceName;
  }

  protected void finalize() throws Throwable {
    super.finalize();
    this.close();
  }

  public boolean getBluetoothStateON(int var1) {
    for (int var2 = var1 / 50; var2 > 0; --var2) {
      int var3 = this.btAdapter.getState();
      if (var3 == 12) {
        return true;
      }

      try {
        Thread.sleep(50L);
      } catch (InterruptedException var5) {
      }
    }

    return false;
  }

  public boolean open(String var1, int var2) {
    this.isOpen = false;
    if (var1 == null) {
      return false;
    } else {
      this.btAdapter = BluetoothAdapter.getDefaultAdapter();
      this.btDeviceString = var1;
      if (var2 < 1000) {
        var2 = 1000;
      }

      if (var2 > 6000) {
        var2 = 6000;
      }

      long var3 = SystemClock.elapsedRealtime();

      while (12 != this.btAdapter.getState()) {
        if (SystemClock.elapsedRealtime() - var3 > (long) var2) {
          Log.e("PP", "adapter state on timeout");
          return false;
        }

        try {
          Thread.sleep(200L);
        } catch (InterruptedException var13) {
          var13.printStackTrace();
          return false;
        }
      }

      BluetoothSocket var5 = null;

      try {
        BluetoothDevice var6 = this.btAdapter.getRemoteDevice(this.btDeviceString);
        var5 = var6.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
        this.deviceName = var6.getName();
      } catch (Exception var12) {
        var5 = null;
        Log.e("PP", "createRfcommSocketToServiceRecord exception");
        this.isOpen = false;
        return false;
      }

      this.mmBtSocket = var5;
      var3 = SystemClock.elapsedRealtime();

      try {
        this.mmBtSocket.connect();
      } catch (Exception var14) {
        var14.printStackTrace();
        Log.e("PP", "connect exception");
        if (SystemClock.elapsedRealtime() - var3 > (long) var2) {
          try {
            this.mmBtSocket.close();
          } catch (IOException var8) {
            var8.printStackTrace();
          }

          this.isOpen = false;
          Log.e("PP", "connet timeout");
          return false;
        }

        try {
          Thread.sleep(200L);
        } catch (InterruptedException var9) {
          var9.printStackTrace();
        }

        return false;
      }

      try {
        this.mmOutStream = this.mmBtSocket.getOutputStream();
      } catch (IOException var11) {
        var11.printStackTrace();
        return false;
      }

      try {
        this.mmInStream = this.mmBtSocket.getInputStream();
      } catch (IOException var10) {
        var10.printStackTrace();
        return false;
      }

      this.isOpen = true;
      Log.e("PP", "connect ok");
      return true;
    }
  }

  public boolean close() {
    if (this.mmBtSocket == null) {
      this.isOpen = false;
      Log.e("PP", "mmBtSocket null");
      return false;
    } else {
      if (this.isOpen) {
        try {
          if (this.mmOutStream != null) {
            this.mmOutStream.close();
            this.mmOutStream = null;
          }

          if (this.mmInStream != null) {
            this.mmInStream.close();
            this.mmOutStream = null;
          }

          this.mmBtSocket.close();
          Log.e("PP", "close success");
        } catch (Exception var2) {
          this.isOpen = false;
          Log.e("PP", "close exception");
          return false;
        }
      }

      this.isOpen = false;
      this.mmBtSocket = null;
      return true;
    }
  }

  public boolean flushReadBuffer() {
    byte[] var1 = new byte[64];
    if (!this.isOpen) {
      return false;
    } else {
      while (true) {
        boolean var2 = false;

        try {
          int var3 = this.mmInStream.available();
          if (var3 == 0) {
            return true;
          }

          if (var3 > 0) {
            if (var3 > 64) {
              var3 = 64;
            }

            this.mmInStream.read(var1, 0, var3);
          }
        } catch (IOException var5) {
        }

        try {
          Thread.sleep(10L);
        } catch (InterruptedException var4) {
        }
      }
    }
  }

  public boolean write(byte[] var1, int var2, int var3) {
    if (!this.isOpen) {
      return false;
    } else if (this.mmBtSocket == null) {
      Log.e("PP", "mmBtSocket null");
      return false;
    } else if (this.mmOutStream == null) {
      Log.e("PP", "mmOutStream null");
      return false;
    } else {
      try {
        this.mmOutStream.write(var1, var2, var3);
        return true;
      } catch (Exception var5) {
        return false;
      }
    }
  }

  public boolean writeNULL() {
    this._cmd[0] = 0;
    return this.write(this._cmd, 0, 1);
  }

  public boolean write(char[] var1, int var2, int var3, int var4) {
    int var5;
    if (var4 == 1) {
      for (var5 = var2; var5 < var2 + var3; ++var5) {
        this._cmd[0] = (byte) var1[var5];
        this.write(this._cmd, 0, 1);
      }
    } else {
      for (var5 = var2; var5 < var2 + var3; ++var5) {
        this._cmd[0] = (byte) var1[var5];
        this._cmd[1] = (byte) (var1[var5] >> 8);
        this.write(this._cmd, 0, 2);
      }
    }

    return true;
  }

  public boolean write(String var1) {
    Object var2 = null;

    byte[] var3;
    try {
      var3 = var1.getBytes("GBK");
    } catch (UnsupportedEncodingException var5) {
      Log.e("PP", "Sting getBytes('GBK') failed");
      return false;
    }

    return !this.write(var3, 0, var3.length) ? false : this.writeNULL();
  }

  public boolean write(byte[] var1) {
    byte[] var2 = new byte[4];
    boolean var3 = true;
    int var4 = 0;

    for (int var5 = 0; var5 < 72; ++var5) {
      var4 += (short) var1[var5];
    }

    var2[0] = 31;
    var2[1] = -103;
    var2[2] = (byte) (var4 & 255);
    var2[3] = (byte) ((var4 & '\uff00') >> 8);
    var3 = this.write(var2, 0, 4);
    if (!var3) {
      return false;
    } else {
      var3 = this.write(var1, 0, 72);
      if (!var3) {
        return false;
      } else {
        byte[] var6 = new byte[3];
        return this.read(var6, 3, 500) ? (var6[0] == 31 && var6[1] == 153 && var6[2] == 0 ? true : (var6[0] == 31 && var6[1] == 153 && var6[2] == 1 ? false : false)) : false;
      }
    }
  }

  public boolean read(byte[] var1, int var2, int var3, int var4) {
    if (!this.isOpen) {
      return false;
    } else {
      if (var4 < 200) {
        var4 = 200;
      }

      if (var4 > 5000) {
        var4 = 5000;
      }

      try {
        long var5 = SystemClock.elapsedRealtime();
        long var7 = 0L;
        int var9 = var3;
        boolean var10 = false;

        while (true) {
          if (this.mmInStream.available() > 0) {
            int var12 = this.mmInStream.read(var1, var2, var9);
            var2 += var12;
            var9 -= var12;
          }

          if (var9 == 0) {
            return true;
          }

          var7 = SystemClock.elapsedRealtime();
          if (var7 - var5 > (long) var4) {
            return false;
          }

          Thread.sleep(20L);
        }
      } catch (Exception var11) {
        return false;
      }
    }
  }

  public boolean read(byte[] var1, int var2, int var3) {
    return this.read((byte[]) var1, 0, var2, var3);
  }

  public int readLength() {
    try {
      return this.mmInStream.available();
    } catch (IOException var2) {
      Log.e("PP", "read exception");
      this.close();
      return 0;
    }
  }

}
