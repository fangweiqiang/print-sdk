package com.qrprt.sdk.qr386.android;

public class QR386Android extends AbstractQR386Android<QR386Android> {

  private QRBluetoothPort port;
  private boolean isValidPrinter = false;

  private QR386Android(QRBluetoothPort port) {
    this.port = port;
  }

//  public static QR386Android use() {
//    return new QR386Android();
//  }

  public static QR386Android with(QRBluetoothPort port) {
    return new QR386Android(port);
  }

  public QRBluetoothPort bluetoothPort() {
    return this.port;
  }

  public int _SoPrinterInit() {
    return QR386SO.PrinterInit();
  }

  public boolean _CheckPrinter(String var0) {
    return QR386SO.CheckPrinter(var0);
  }

  public int[] _CheckValidPrinter(int var0) {
    return QR386SO.CheckValidPrinter(var0);
  }


//  private boolean checkValidPrinter(String address) {
//    if (this.port.isOpen()) {
//      byte[] Rep = new byte[4];
//      this.port.flushReadBuffer();
//      int[] data = QR386SO.CheckValidPrinter(77175792);
//      byte[] test = new byte[8];
//
//      for (int i = 0; i < 8; ++i) {
//        test[i] = (byte) (data[i] & 255);
//      }
//
//      byte[] sdata = new byte[]{30, 101, 0, 0, 0, 0};
//
//      for (int i = 0; i < 4; ++i) {
//        sdata[2 + i] = (byte) data[i];
//      }
//
//      boolean writeResult = this.port.write(sdata, 0, sdata.length);
//      if (writeResult && this.port.read(Rep, 4, 3000)) {
//        int[] revdata = new int[4];
//
//        for (int j = 0; j < 4; ++j) {
//          revdata[j] = Rep[j] & 255;
//          if (data[4 + j] != revdata[j]) {
//            Log.e("checkValidPrinter", "false");
//            return false;
//          }
//        }
//
//        Log.e("checkValidPrinter", "success");
//        return true;
//      }
//      Log.e("checkValidPrinter", "false");
//    }
//    return false;
//  }
//
//
//  public boolean connect(String name, String address) {
//    if ($QRKIT$TextKit.blanky(name) || $QRKIT$TextKit.blanky(address)) {
//      this.isValidPrinter = false;
//      return false;
//    }
//    if (this.port.isOpen()) {
//      this.port.close();
//    }
//    int portTimeOut = 3000;
//    this.port.open(address, portTimeOut);
//
//    if (QR386SO.CheckPrinter(name)) {
//      if (this.checkValidPrinter(address)) {
//        this.isValidPrinter = true;
//        return true;
//      }
//
//      this.isValidPrinter = false;
//      this.port.close();
//      return false;
//    }
//    this.isValidPrinter = false;
//    this.port.close();
//    return false;
//  }
//
//  public void disconnect() {
//    if (!this.port.isOpen()) {
//      return;
//    }
//
//    try {
//      Thread.sleep(100L);
//    } catch (InterruptedException var2) {
//      var2.printStackTrace();
//    }
//
//    this.port.close();
//  }
//
//  public boolean connected() {
//    return this.port.isOpen();
//  }
//
//  public String status() {
//    if (!this.port.isOpen()) {
//      return "Printer is disconnect";
//    }
//    byte[] Cmd = new byte[]{16, 4, 5};
//    this.port.flushReadBuffer();
//    if (!this.port.write(Cmd, 0, 3)) {
//      return "Print Write Error";
//    }
//
//    byte[] Rep = new byte[2];
//    if (!this.port.read(Rep, 2, 2000)) {
//      return "Print Read Error";
//    }
//    if (Rep[0] == 0) {
//      return "OK";
//    }
//    if (Rep[0] == 79 && Rep[1] == 75) {
//      return "OK";
//    }
//    if ((Rep[0] & 16) != 0) {
//      return "CoverOpened ";
//    }
//    if ((Rep[0] & 1) != 0) {
//      return "NoPaper";
//    }
//    if ((Rep[0] & 8) != 0) {
//      return "Printing";
//    }
//    return (Rep[0] & 4) != 0 ? "BatteryLow" : "OK";
//  }
//
//  public boolean send() {
//    try {
//      if (!this.port.isOpen()) {
//        super.clear();
//      }
//      if (!this.isValidPrinter) {
//        return false;
//      }
//
//      byte[] binary = super.command().binary();
//      int len = binary.length;
//
//      while (true) {
//        try {
//          Thread.sleep(1L);
//        } catch (InterruptedException ignored) {
//        }
//
//        if (len <= 10) {
//          return this.port.write(binary, binary.length - len, len);
//        }
//
//        if (!this.port.write(binary, binary.length - len, 10)) {
//          return false;
//        }
//
//        len -= 10;
//      }
//    } finally {
//      super.clear();
//    }
//  }

}
