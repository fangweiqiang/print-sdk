package com.qrprt.sdk.thp.qr386.snbc;

import com.snbc.sdk.barcode.BarInstructionImpl.BarPrinter;
import com.snbc.sdk.barcode.IBarInstruction.ILabelEdit;
import com.snbc.sdk.barcode.enumeration.InstructionType;
import com.snbc.sdk.barcode.enumeration.QRLevel;
import com.snbc.sdk.barcode.enumeration.QRMode;
import com.snbc.sdk.barcode.enumeration.Rotation;
import com.snbc.sdk.connect.connectImpl.USBConnect;
import org.junit.Ignore;
import org.junit.Test;

public class QR386SNBCTest {


  @Test
  @Ignore
  public void test0() throws Exception {
    // USBConnect c = new USBConnect(null);
    USBConnect connect = null;
    final BarPrinter printer = QR386SNBC.with(connect).build(InstructionType.BPLC);
    ILabelEdit labelEdit = printer.labelEdit();

    labelEdit.setColumn(1, 0);
    labelEdit.setLabelSize(640, 1000);

    labelEdit.printRectangle(0, 0, 640, 1000, 2);

    labelEdit.printImage(10, 923, "/tmp/a.bmp");

    labelEdit.printBarcodeQR(447, 876, Rotation.Rotation0, "LK002325883CN", QRLevel.QR_LEVEL_H.getLevel(), 4, QRMode.QR_MODE_ENHANCED.getMode());

    labelEdit.printText(10, 890, "1", "IMPORTANT:", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(10, 860, "1", "The item/parcel may be", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(10, 830, "1", "opened officially.", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(10, 800, "1", "Please print in English.", Rotation.Rotation0, 1, 1, 0);

    labelEdit.printRectangle(270, 802, 72, 72, 2);
    labelEdit.printText(286, 812, "3", "2", Rotation.Rotation0, 2, 2, 0);

    labelEdit.printText(8, 760, "2", "FROM: HANK HUANG", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(8, 735, "2", "4/F DONG HAI COMMERCIAL", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(8, 710, "2", "BUILDING 618 YAN AN ", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(8, 685, "2", "ROAD SHANGHAI", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(8, 660, "2", "CHINA 190893", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(8, 635, "2", "PHONE:13908473278", Rotation.Rotation0, 1, 1, 0);

    labelEdit.printLine(327, 798, 640, 798, 1);
    labelEdit.printLine(327, 798, 327, 526, 1);
    labelEdit.printLine(0, 603, 327, 603, 1);
    labelEdit.printLine(0, 563, 327, 563, 1);
    labelEdit.printLine(0, 524, 640, 524, 1);
    labelEdit.printLine(0, 479, 640, 479, 1);
    labelEdit.printLine(0, 219, 640, 219, 1);
    labelEdit.printLine(0, 191, 640, 191, 1);

    labelEdit.printLine(36, 524, 33, 191, 1);
    labelEdit.printLine(84, 524, 80, 191, 1);
    labelEdit.printLine(346, 524, 346, 191, 1);
    labelEdit.printLine(408, 524, 408, 191, 1);
    labelEdit.printLine(501, 524, 501, 191, 1);

    labelEdit.printText(343, 760, "2", "SHIP TO: QIAUBUYER ", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(343, 735, "2", "JIM CARRY", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(343, 710, "2", "2125 HAMILTON AVE", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(343, 685, "2", "SAN JOSE CA", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(343, 660, "2", "UNITED STATES OF ", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(343, 635, "2", "AMERICA 95125-5905", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(343, 610, "2", "PHONE:4083672341", Rotation.Rotation0, 1, 1, 0);

    labelEdit.printText(8, 568, "2", "Fees(US $):", Rotation.Rotation0, 1, 1, 0);

    labelEdit.printText(8, 568, "2", "Certificate No.", Rotation.Rotation0, 1, 1, 0);

    labelEdit.printText(8, 488, "2", "No", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(40, 488, "2", "Qty", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(102, 488, "2", "Description", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(358, 488, "2", "Kg", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(416, 488, "2", "Val($)", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(516, 488, "2", "Origin", Rotation.Rotation0, 1, 1, 0);

    labelEdit.printText(8, 450, "2", "1", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(40, 450, "2", "2", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(102, 450, "2", "Pages", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(350, 450, "2", "0.20", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(420, 450, "2", "10.0", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(516, 450, "2", "China", Rotation.Rotation0, 1, 1, 0);

    labelEdit.printText(40, 196, "2", "2", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(102, 196, "2", "Total Weight (Kg):", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(350, 196, "2", "0.40", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(420, 196, "2", "20.0", Rotation.Rotation0, 1, 1, 0);

    labelEdit.printText(4, 180, "1", "I certify the particulars given in this customs declaration ", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(4, 165, "1", "are correct. This item does not contain any dangerous ", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(4, 150, "1", "article, or articles prohibited by legislation or by postal or ", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(4, 135, "1", "customs regulations. I have met all applicable export ", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(4, 120, "1", "filing requirements underthe Foreign Trade ", Rotation.Rotation0, 1, 1, 0);
    labelEdit.printText(4, 105, "1", "Regulations.", Rotation.Rotation0, 1, 1, 0);

    labelEdit.printText(4, 80, "2", "Sender's Signature & Date Signed:", Rotation.Rotation0, 1, 1, 0);

    labelEdit.printText(536, 40, "3", "CN22", Rotation.Rotation0, 1, 1, 0);

    printer.labelControl().print(1, 1);

  }

}
