package com.snbc.sdk.barcode.BarInstructionImpl;

import com.snbc.sdk.barcode.IBarInstruction.*;

public class _SNBCCPI {

  public static void setLabelConfig(BarPrinter printer, ILabelConfig config) {
    printer.setLabelConfig(config);
  }

  public static void setLabelControl(BarPrinter printer, ILabelControl control) {
    printer.setLabelControl(control);
  }

  public static void setLabelEdit(BarPrinter printer, ILabelEdit edit) {
    printer.setLabelEdit(edit);
  }

  public static void setLabelFormat(BarPrinter printer, ILabelFormat format) {
    printer.setLabelFormat(format);
  }

  public static void setLabelImageAndFont(BarPrinter printer, ILabelImageAndFont iaf) {
    printer.setLabelImageAndFont(iaf);
  }

  public static void setLabelQuery(BarPrinter printer, ILabelQuery query) {
    printer.setLabelQuery(query);
  }

}
