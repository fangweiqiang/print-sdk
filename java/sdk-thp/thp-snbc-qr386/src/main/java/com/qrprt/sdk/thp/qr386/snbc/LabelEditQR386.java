package com.qrprt.sdk.thp.qr386.snbc;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.qrprt.sdk.api.Command;
import com.qrprt.sdk.qr386.QR386;
import com.qrprt.sdk.qr386.arg.*;
import com.qrprt.sdk.qr386.arg.ii.BinaryImage;
import com.qrprt.sdk.toolkit.$TextKit;
import com.snbc.sdk.barcode.BarInstructionImpl.BarPrintQuery;
import com.snbc.sdk.barcode.IBarInstruction.ILabelEdit;
import com.snbc.sdk.barcode.enumeration.BarCodeType;
import com.snbc.sdk.barcode.enumeration.HRIPosition;
import com.snbc.sdk.barcode.enumeration.Rotation;
import com.snbc.sdk.connect.IConnect.DeviceConnect;
import com.snbc.sdk.exception.BarFunctionNoSupportException;
import com.snbc.sdk.imageproc.GRFCompress;
import com.snbc.sdk.unit.PrnUnit;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

class LabelEditQR386 implements ILabelEdit {

  private DeviceConnect mConnect;
  private int mDPI = 203;
  private PrnUnit mUnit;
  private int mColumn;
  private int mGap;
  private int mLabelWidth;
  private BarPrintQuery pHSPara;

  LabelEditQR386(DeviceConnect connect) {
    this.mUnit = PrnUnit.Dot;
    this.mColumn = 1;
    this.mGap = 0;
    this.mLabelWidth = 0;
    this.pHSPara = new BarPrintQuery();
    this.mConnect = connect;
  }

  @Override
  public void setLabelSize(int width, int height) throws IOException, InterruptedException {
    Command command = QR386.use().init(AInit.builder().weight(width).height(height).build()).command();
    this.mConnect.write(command.binary());
    QR386.use().clear();
  }

  @Override
  public void printText(int x, int y, String fontName, String content, Rotation angle, int horiSize, int vertSize, int reverse) throws IllegalArgumentException, IOException, InterruptedException {
    if ($TextKit.blanky(content))
      throw new IllegalArgumentException("Content can not be null");
    QR386.use().clear();
    Command command = QR386.use().text(AText.builder()
      .x(x)
      .y(y)
      .rotate(angle.getRotation())
      .text(content)
      .reverse(reverse == 1)
      .build())
      .command();
    this.mConnect.write(command.binary());
    QR386.use().clear();
  }

  @Override
  public void printBarcode1D(int x, int y, BarCodeType barcodeType, Rotation rotate, byte[] data, int height, HRIPosition hriPositon, int narrowbarWidth, int wideBarwidth) throws IllegalArgumentException, IOException, InterruptedException, BarFunctionNoSupportException {
    if (data == null)
      throw new IllegalArgumentException("data can not be null");
    int _type;
    switch (barcodeType) {
      case Code128:
        _type = 1;
        break;
      case Code39:
        _type = 0;
        break;
      case Code93:
        _type = 2;
        break;
      case CodeEAN8:
        _type = 4;
        break;
      case CodeEAN13:
        _type = 5;
        break;
      case CODABAR:
        _type = 3;
        break;
      case ITF25:
        _type = 8;
        break;
      case UPCA:
        _type = 6;
        break;
      case UPCE:
        _type = 7;
        break;
      default:
        _type = -1;
        break;
    }
    Command command = QR386.use().barcode(ABarcode.builder()
      .x(x)
      .y(y)
      .type(_type)
      .width(wideBarwidth)
      .height(height)
      .rotate(rotate.getRotation())
      .build())
      .command();
    this.mConnect.write(command.binary());
    QR386.use().clear();
  }

  @Override
  public void printBarcodeQR(int x, int y, Rotation rotate, String content, String ECCLever, int cellWidth, int model) throws IllegalArgumentException, IOException, InterruptedException {
    if (content == null)
      throw new IllegalArgumentException("content can not be null");
    if ($TextKit.blanky(ECCLever))
      throw new IllegalArgumentException("ECCLever can not be null");
    int lel;
    char ch = ECCLever.charAt(0);
    switch (ch) {
      case 'L':
        lel = 0;
        break;
      case 'M':
        lel = 1;
        break;
      case 'Q':
        lel = 2;
        break;
      case 'H':
        lel = 3;
        break;
      default:
        lel = -1;
        break;
    }
    Command command = QR386.use().qrcode(AQrCode.builder()
      .x(x)
      .y(y)
      .rotate(rotate.getRotation())
      .text(content)
      .lel(lel)
      .ver(model)
      .build())
      .command();
    this.mConnect.write(command.binary());
    QR386.use().clear();
  }

  @Override
  public void printBarcodePDF417(int x, int y, Rotation rotate, String content, int securityEvel, int width, int height, int rows, int columns) throws IllegalArgumentException, IOException, InterruptedException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void printBarcodeMaxiCode(int x, int y, String content, int mode) throws IllegalArgumentException, IOException, InterruptedException, BarFunctionNoSupportException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void printBarcodeDataMatrix(int x, int y, Rotation rotate, String content, int row, int col, int moduleSize) throws IllegalArgumentException, IOException, InterruptedException, BarFunctionNoSupportException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void printStoredImage(int x, int y, String imageName) throws IllegalArgumentException, IOException, InterruptedException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void printImage(int x, int y, String imagePath) throws FileNotFoundException, IOException, InterruptedException {
    FileInputStream temp_stream = new FileInputStream(imagePath);
    Bitmap image = BitmapFactory.decodeStream(temp_stream);
    if (image == null)
      throw new RuntimeException("Can not get image");
    temp_stream.close();
    this.printImage(x, y, image);
  }

  @Override
  public void printImage(int x, int y, android.graphics.Bitmap bitmap) throws IllegalArgumentException, IOException, InterruptedException {
    if (bitmap == null)
      throw new IllegalArgumentException("bitmap can not be null");

    boolean error_code = false;
    int width = bitmap.getWidth();
    int height = bitmap.getHeight();
    int WidthBytes = (width + 31) / 32 * 32 / 8;
    int image_data_len = height * WidthBytes;
    byte[] bitampDatax = new byte[image_data_len];
    Arrays.fill(bitampDatax, (byte) 0);
    GRFCompress grf = new GRFCompress();
    int error_codex = grf.ImageFormatConvert(bitmap, bitampDatax);
    if (error_codex < 0) {
      throw new IllegalArgumentException();
    } else {
      int row;
      for (row = 0; row < bitampDatax.length; ++row) {
        bitampDatax[row] = (byte) (~bitampDatax[row]);
      }

      byte[] BitMask1 = new byte[]{-128, 64, 32, 16, 8, 4, 2, 1};
      byte[] var10000 = new byte[]{127, -65, -33, -17, -9, -5, -3, -2};

      for (row = 0; row < height; ++row) {
        for (int col = width; col < WidthBytes * 8; ++col) {
          bitampDatax[row * WidthBytes + col / 8] &= BitMask1[col & 7];
        }
      }

      for (row = 0; row < height / 2; ++row) {
        byte[] temp = new byte[WidthBytes];
        System.arraycopy(bitampDatax, (height - row - 2) * WidthBytes, temp, 0, WidthBytes);
        System.arraycopy(bitampDatax, row * WidthBytes, bitampDatax, (height - row - 2) * WidthBytes, WidthBytes);
        System.arraycopy(temp, 0, bitampDatax, row * WidthBytes, WidthBytes);
      }
    }

    Thread.sleep(50L);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    baos.write(new byte[]{(byte) (WidthBytes * 8 % 256), (byte) (WidthBytes * 8 / 256), (byte) (height % 256), (byte) (height / 256)});
    baos.write(bitampDatax, 0, bitampDatax.length);
    byte[] binary = baos.toByteArray();
    Command command = QR386.use().image(AImage.builder()
        .x(x)
        .y(y)
        .width(width)
        .height(height)
        .build(),
      BinaryImage.create(binary))
      .command();
    baos.close();
    this.mConnect.write(command.binary());
    QR386.use().clear();
  }

  @Override
  public void printRectangle(int x, int y, int width, int height, int thickness) throws IOException, InterruptedException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void printEllipse(int x, int y, int horiRadius, int vertRadios, int thickness) throws IOException, InterruptedException, BarFunctionNoSupportException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void printLine(int startX, int startY, int endX, int endY, int thickness) throws IOException, InterruptedException {
    Command command = QR386.use().line(ALine.builder()
      .startx(startX)
      .starty(startY)
      .endx(endX)
      .endy(endY)
      .width(thickness)
      .build())
      .command();
    this.mConnect.write(command.binary());
    QR386.use().clear();
  }

  @Override
  public void printTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int thickness) throws IOException, InterruptedException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void setColumn(int column, int gap) throws IOException {
//    throw new RuntimeException("No Support");
    this.mColumn = column;
    this.mGap = gap;
  }

  @Override
  public void setMeasuringUnit(PrnUnit unit) throws IOException {
//    throw new RuntimeException("No Support");
  }

  @Override
  public void clearPrintBuffer() throws IOException, InterruptedException, BarFunctionNoSupportException {
//    throw new RuntimeException("No Support");
  }

  @Override
  public void selectPrinterCodepage(int codepage) throws IOException, InterruptedException, BarFunctionNoSupportException {
//    throw new RuntimeException("No Support");
  }

  @Override
  public int setEnglishFont(String enfont_name, int yAddjust) throws IOException, InterruptedException, BarFunctionNoSupportException {
    throw new BarFunctionNoSupportException("No Support");
  }

  @Override
  public int getColumn() {
    return this.mColumn;
  }

  @Override
  public int getGap() {
    return this.mGap;
  }

  @Override
  public int getLabelWidth() {
    return this.mLabelWidth;
  }

}
