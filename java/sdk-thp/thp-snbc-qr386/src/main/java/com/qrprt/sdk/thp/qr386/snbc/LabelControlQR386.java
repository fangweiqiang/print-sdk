package com.qrprt.sdk.thp.qr386.snbc;

import com.qrprt.sdk.api.Command;
import com.qrprt.sdk.qr386.QR386;
import com.snbc.sdk.barcode.IBarInstruction.ILabelControl;
import com.snbc.sdk.connect.IConnect.DeviceConnect;
import com.snbc.sdk.exception.BarFunctionNoSupportException;

import java.io.IOException;

class LabelControlQR386 implements ILabelControl {

  private DeviceConnect mConnect;

  LabelControlQR386(DeviceConnect mConnect) {
    this.mConnect = mConnect;
  }

  @Override
  public void print(int labelNum, int sameNum) throws IllegalArgumentException, IOException, InterruptedException {
    Command command = QR386.use().print().command();
    this.mConnect.write(command.binary());
    QR386.use().clear();
  }

  @Override
  public void feedLabel() throws IOException, InterruptedException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void cut() throws IOException, InterruptedException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void calibrate() throws IOException, InterruptedException, BarFunctionNoSupportException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void reboot() throws IOException, InterruptedException, BarFunctionNoSupportException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void printSelfCheckingPaper() throws IOException, InterruptedException, BarFunctionNoSupportException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void PrintConfiguration() throws IOException, InterruptedException, BarFunctionNoSupportException {
    throw new RuntimeException("No Support");
  }

  @Override
  public void selfCheck() throws IOException, BarFunctionNoSupportException {
    throw new RuntimeException("No Support");
  }
}
