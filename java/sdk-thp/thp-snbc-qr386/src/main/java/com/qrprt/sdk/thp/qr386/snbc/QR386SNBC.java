package com.qrprt.sdk.thp.qr386.snbc;

import com.snbc.sdk.barcode.BarInstructionImpl.BarPrinter;
import com.snbc.sdk.barcode.BarInstructionImpl._SNBCCPI;
import com.snbc.sdk.barcode.enumeration.InstructionType;
import com.snbc.sdk.connect.connectImpl.USBConnect;

public class QR386SNBC {

  private USBConnect connect;

  private QR386SNBC(USBConnect connect) {
    this.connect = connect;
  }

  public static QR386SNBC with(USBConnect connect) {
    return new QR386SNBC(connect);
  }

  public BarPrinter build(InstructionType type) {
    BarPrinter.BarPrinterBuilder builder = new BarPrinter.BarPrinterBuilder();
    builder.buildDeviceConnenct(this.connect);
    builder.buildInstruction(type);
    BarPrinter printer = builder.getBarPrinter();
    _SNBCCPI.setLabelEdit(printer, new LabelEditQR386(this.connect));
    _SNBCCPI.setLabelControl(printer, new LabelControlQR386(this.connect));
    return printer;
  }

}
