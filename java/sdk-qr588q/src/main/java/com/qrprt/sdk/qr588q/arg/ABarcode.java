package com.qrprt.sdk.qr588q.arg;

import com.qrprt.sdk.api.CArg;

public class ABarcode implements CArg {

  private final int x;
  private final int y;
  private final String text;
  private final int height;
  private final int multiple;
  private final int rotation;
  private ABarcode(Builder builder) {
    this.x = builder.x;
    this.y = builder.y;
    this.text = builder.text;
    this.height = builder.height;
    this.multiple = builder.multiple;
    this.rotation = builder.rotation;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int x() {
    return x;
  }

  public int y() {
    return y;
  }

  public String text() {
    return text;
  }

  public int height() {
    return height;
  }

  public int multiple() {
    return multiple;
  }

  public int rotation() {
    return rotation;
  }
  public static class Builder {

    private Integer x;
    private Integer y;
    private String text;
    private Integer height;
    private Integer multiple;
    private Integer rotation;
    public ABarcode build() {
      if (this.x == null)
        throw new IllegalArgumentException("x coordinates cannot be null");
      if (this.y == null)
        throw new IllegalArgumentException("y coordinates cannot be null");
      if (this.text == null)
        throw new IllegalArgumentException("text cannot be null");
      if (this.height == null)
        throw new IllegalArgumentException("height cannot be null");
      if (this.multiple == null)
        throw new IllegalArgumentException("multiple cannot be null");
      if (this.rotation == null)
        throw new IllegalArgumentException("rotation cannot be null");
      return new ABarcode(this);
    }

    public Builder x(Integer x) {
      this.x = x;
      return this;
    }

    public Builder y(Integer y) {
      this.y = y;
      return this;
    }

    public Builder text(String text) {
      this.text = text;
      return this;
    }

    public Builder height(Integer height) {
      this.height = height;
      return this;
    }

    public Builder multiple(Integer multiple) {
      this.multiple = multiple;
      return this;
    }
    public Builder rotation(Integer rotation) {
      this.rotation = rotation;
      return this;
    }
  }

}
