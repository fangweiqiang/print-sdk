package com.qrprt.sdk.qr588q.arg;

import com.qrprt.sdk.api.CArg;

public class AInit implements CArg {

  private final int weight;
  private final int height;
  private final int speed;
  private final int density;

  private AInit(Builder builder) {
    this.weight = builder.weight;
    this.height = builder.height;
    this.speed = builder.speed;
    this.density = builder.speed;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int speed() {
    return speed;
  }

  public int density() {
    return density;
  }

  public int weight() {
    return weight;
  }

  public int height() {
    return height;
  }

  public static class Builder {
    private int speed;
    private int density;
    private int weight;
    private int height;

    public Builder() {
      this.speed = 7;
      this.density = 4;
    }

    public AInit build() {
      return new AInit(this);
    }

    public Builder speed(int speed) {
      this.speed = speed <= 0 || speed > 7 ? 7 : speed;
      return this;
    }

    public Builder density(int density) {
      this.density = density <= 0 || density > 4 ? 4 : density;
      return this;
    }

    public Builder weight(int weight) {
      this.weight = weight <= 0 ? 75 : weight;
      return this;
    }

    public Builder height(int height) {
      this.height = height <= 0 ? 130 : height;
      return this;
    }
  }
}
