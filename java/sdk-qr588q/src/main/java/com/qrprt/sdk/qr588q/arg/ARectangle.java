package com.qrprt.sdk.qr588q.arg;

import com.qrprt.sdk.api.CArg;

public class ARectangle implements CArg {

  private int x;
  private int y;
  private int width;
  private int height;
  private int thickness;

  private ARectangle(Builder builder) {
    this.x = builder.x;
    this.y = builder.y;
    this.width = builder.width;
    this.height = builder.height;
    this.thickness = builder.thickness;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int x() {
    return x;
  }

  public int y() {
    return y;
  }

  public int width() {
    return width;
  }

  public int height() {
    return height;
  }

  public int thickness() {
    return thickness;
  }

  public static class Builder {

    private int x;
    private int y;
    private int width;
    private int height;
    private int thickness;

    public Builder() {
      this.x = -1;
      this.y = -1;
      this.width = -1;
      this.height = -1;
      this.thickness = -1;
    }

    public ARectangle build() {
      if (this.x == -1)
        throw new IllegalArgumentException("x coordinates cannot be null");
      if (this.y == -1)
        throw new IllegalArgumentException("y coordinates cannot be null");
      if (this.width == -1)
        throw new IllegalArgumentException("width cannot be null");
      if (this.height == -1)
        throw new IllegalArgumentException("height cannot be null");
      if (this.thickness == -1)
        throw new IllegalArgumentException("thickness cannot be null");
      return new ARectangle(this);
    }

    public Builder x(int x) {
      this.x = x;
      return this;
    }

    public Builder y(int y) {
      this.y = y;
      return this;
    }

    public Builder width(int width) {
      this.width = width;
      return this;
    }

    public Builder height(int height) {
      this.height = height;
      return this;
    }

    public Builder thickness(int thickness) {
      this.thickness = thickness;
      return this;
    }
  }

}
