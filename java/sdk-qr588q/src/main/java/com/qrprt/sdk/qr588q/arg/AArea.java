package com.qrprt.sdk.qr588q.arg;

import com.qrprt.sdk.api.CArg;

public class AArea implements CArg {

  /**
   * 宽
   */
  private final int weight;
  /**
   * 高
   */
  private final int height;

  private AArea(Builder builder) {
    this.weight = builder.weight;
    this.height = builder.height;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int weight() {
    return weight;
  }

  public int height() {
    return height;
  }

  public static class Builder {
    private int weight;
    private int height;

    public AArea build() {
      if (this.weight <= 0)
        throw new IllegalArgumentException("Width must be greater than 0");
      if (this.height <= 0)
        throw new IllegalArgumentException("Height must be greater then 0");
      return new AArea(this);
    }

    public Builder weight(int weight) {
      this.weight = weight;
      return this;
    }

    public Builder height(int height) {
      this.height = height;
      return this;
    }
  }

}
