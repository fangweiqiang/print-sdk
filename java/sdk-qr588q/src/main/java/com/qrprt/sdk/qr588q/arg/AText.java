package com.qrprt.sdk.qr588q.arg;

import com.qrprt.sdk.api.CArg;

public class AText implements CArg {

  private final int x;
  private final int y;
  private final int language;
  private final String text;
  private final int rotation;
  private final int fontsizeGroup;
  private final int fontsize;
  private final int fontstyle;

  private AText(Builder builder) {
    this.x = builder.x;
    this.y = builder.y;
    this.language = builder.language;
    this.text = builder.text;
    this.rotation = builder.rotation;
    this.fontsizeGroup = builder.fontsizeGroup;
    this.fontsize = builder.fontsize;
    this.fontstyle = builder.fontstyle;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int x() {
    return x;
  }

  public int y() {
    return y;
  }

  public int language() {
    return language;
  }

  public String text() {
    return text;
  }

  public int rotation() {
    return rotation;
  }

  public int fontsizeGroup() {
    return fontsizeGroup;
  }

  public int fontsize() {
    return fontsize;
  }

  public int fontstyle() {
    return fontstyle;
  }

  public static class Builder {

    private Integer x;
    private Integer y;
    private Integer language;
    private String text;
    private Integer rotation;
    private Integer fontsizeGroup;
    private Integer fontsize;
    private Integer fontstyle;

    public Builder() {
      this.rotation = 0;
      this.language = -1;
    }

    public AText build() {
      if (this.x == null)
        throw new IllegalArgumentException("x coordinates cannot be null");
      if (this.y == null)
        throw new IllegalArgumentException("y coordinates cannot be null");
      if (this.fontsizeGroup == null)
        throw new IllegalArgumentException("font size group cannot be null");
      if (this.fontsize == null)
        throw new IllegalArgumentException("font size cannot be null");
      if (this.fontstyle == null)
        throw new IllegalArgumentException("font style cannot be null");
      return new AText(this);
    }

    public Builder x(int x) {
      this.x = x;
      return this;
    }

    public Builder y(int y) {
      this.y = y;
      return this;
    }

    public Builder language(int language) {
      this.language = language;
      return this;
    }

    public Builder text(String text) {
      this.text = text;
      return this;
    }

    public Builder rotation(int rotation) {
      this.rotation = rotation;
      return this;
    }

    public Builder fontsizeGroup(int fontsizeGroup) {
      this.fontsizeGroup = fontsizeGroup;
      return this;
    }

    public Builder fontsize(int fontsize) {
      this.fontsize = fontsize;
      return this;
    }

    public Builder fontstyle(int fontstyle) {
      this.fontstyle = fontstyle;
      return this;
    }
  }
}
