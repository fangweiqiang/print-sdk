package com.qrprt.sdk.qr588q.arg;

import com.qrprt.sdk.api.CArg;

public class ALine implements CArg {

  private int startx;
  private int starty;
  private int endx;
  private int endy;
  private int thickness;
  private int dotted;

  private ALine(Builder builder) {
    this.startx = builder.startx;
    this.starty = builder.starty;
    this.endx = builder.endx;
    this.endy = builder.endy;
    this.thickness = builder.thickness;
    this.dotted = builder.dotted;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int startx() {
    return startx;
  }

  public int starty() {
    return starty;
  }

  public int endx() {
    return endx;
  }

  public int endy() {
    return endy;
  }

  public int thickness() {
    return thickness;
  }

  public int dotted() {
    return dotted;
  }

  public static class Builder {

    private int startx;
    private int starty;
    private int endx;
    private int endy;
    private int thickness;
    private int dotted;

    public Builder() {
      this.startx = -1;
      this.starty = -1;
      this.endx = -1;
      this.endy = -1;
      this.thickness = -1;
      this.dotted = -1;
    }

    public ALine build() {
      if (this.startx == -1)
        throw new IllegalArgumentException("startx coordinates cannot be null");
      if (this.starty == -1)
        throw new IllegalArgumentException("starty coordinates cannot be null");
      if (this.endx == -1)
        throw new IllegalArgumentException("endx coordinates cannot be null");
      if (this.endy == -1)
        throw new IllegalArgumentException("endy coordinates cannot be null");
      if (this.thickness == -1)
        throw new IllegalArgumentException("thickness cannot be null");
      return new ALine(this);
    }

    public Builder startx(int startx) {
      this.startx = startx;
      return this;
    }

    public Builder starty(int starty) {
      this.starty = starty;
      return this;
    }

    public Builder endx(int endx) {
      this.endx = endx;
      return this;
    }

    public Builder endy(int endy) {
      this.endy = endy;
      return this;
    }

    public Builder thickness(int thickness) {
      this.thickness = thickness;
      return this;
    }

    public Builder dotted(int dotted) {
      this.dotted = dotted;
      return this;
    }
  }

}
