package com.qrprt.sdk.qr588q;

import com.qrprt.sdk.api.ARaw;
import com.qrprt.sdk.api.Command;
import com.qrprt.sdk.api.CommandBuilder;
import com.qrprt.sdk.api.QRSDK;
import com.qrprt.sdk.qr588q.arg.*;
import com.qrprt.sdk.toolkit.$TextKit;

public class QR588Q extends QRSDK<QR588Q> {

  private static class Holder {
    private static final QR588Q INSTANCE = new QR588Q();
  }

  public static QR588Q use() {
    return unsafe();
  }

  /**
   * 线程安全
   *
   * @return QR588Q
   */
  public static QR588Q safe() {
    return new QR588Q();
  }

  /**
   * 线程不安全
   *
   * @return QR588Q
   */
  public static QR588Q unsafe() {
    return Holder.INSTANCE;
  }

  private CommandBuilder builder;

  private QR588Q() {
    this.builder = CommandBuilder.create();
  }

  public QR588Q init() {
    return this.init(AInit.builder().build());
  }

  public QR588Q init(AInit arg) {
    this.clear();
    this.area(AArea.builder().weight(arg.weight()).height(arg.height()).build());
    this.builder
//      .push("SIZE 75 mm, 130 mm")
      .push("DIRECTION 0,0")
      .push("SET GAP ON")
//      .push("SET CUTTER 1")
//      .push($QRKIT$TextKit.union("SPEED ", arg.speed()))
//      .push($QRKIT$TextKit.union("DENSITY", arg.density()))
      .push("CLS");
    return this;
  }

  /**
   * 标签打印机进行打印
   *
   * @return QR588Q
   */
  public QR588Q print() {
    this.builder.push("PRINT 1,1");
//    this.builder.clear();
    return this;
  }

  /**
   * 设置标签打印机的打印区域
   */
  private QR588Q area(AArea arg) {
    //            arg.weight() / 8
    this.builder.push($TextKit.format("SIZE {0} mm, {1} mm", arg.weight(), arg.height()));
    return this;
  }

  /**
   * 标签打印机绘制文本
   */
  public QR588Q text(AText arg) {

    int multipleX = 0, multipleY = 0;
    String rotation;
    switch (arg.rotation()) {
      case 1:
        rotation = "90";
        break;
      case 2:
        rotation = "180";
        break;
      case 3:
        rotation = "270";
        break;
      default:
        rotation = "0";
        break;
    }

    String textType = "TSS16.BF2";
    switch (arg.fontsizeGroup()) {
      case 0:
        textType = "TSS16.BF2";
        multipleX = arg.fontsize();
        multipleY = arg.fontsize();
        break;
      case 1:
        textType = "TSS24.BF2";
        multipleX = arg.fontsize();
        multipleY = arg.fontsize();
        break;
      case 2:
        textType = "TSS32.BF2";
        multipleX = arg.fontsize();
        multipleY = arg.fontsize();
    }


    String cmdText = $TextKit.format("{0} {1},{2},\"{3}\",{4},{5},{6},\"{7}\"", "TEXT",
      arg.x(), arg.y(), textType, rotation, multipleX, multipleY, arg.text());

    switch (arg.fontstyle()) {
      case 1:                         //B1 加粗
        cmdText = $TextKit.format("{0} {1},{2},\"{3}\",{4},{5},{6},B1,\"{7}\"", "TEXT",
          arg.x(), arg.y(), textType, rotation, multipleX, multipleY, arg.text());
        break;
      case 2:
        cmdText = $TextKit.format("{0} {1},{2},\"{3}\",{4},{5},{6},I1,\"{7}\"", "TEXT",
          arg.x(), arg.y(), textType, rotation, multipleX, multipleY, arg.text());
        break;
    }

    this.builder.push(cmdText);
    return this;
  }

  public QR588Q barcode(ABarcode arg) {
    String _cmd = $TextKit.format("BARCODE {0},{1},\"{2}\",{3},0,{4},{5},{5},\"{6}\"",
      arg.x(), arg.y(), "128", arg.height(), arg.rotation(), arg.multiple(), arg.text());
    this.builder.push(_cmd);
    return this;
  }

  public QR588Q qrcode(AQrCode arg) {
    String _cmd = $TextKit.format("QRCODE {0},{1},Q,{2},A,1,M2,S7,\"{3}\"", arg.x(), arg.y(), arg.width(), arg.text());
    this.builder.push(_cmd);
    return this;
  }

  public QR588Q rectangle(ARectangle arg) {
    int endX = arg.width() + arg.x();
    int endY = arg.height() + arg.y();
    String _cmd = $TextKit.format("BOX {0},{1},{2},{3},{4},0", arg.x(), arg.y(), endX, endY, arg.thickness());
    this.builder.push(_cmd);
    return this;
  }

  public QR588Q line(ALine arg) {
    String _cmd = $TextKit.union("LINE ", arg.startx(), ",", arg.starty(), ",", arg.endx(), ",", arg.endy(), ",", arg.thickness());
    if (arg.dotted() >= 1 && arg.dotted() <= 4) {
      _cmd = $TextKit.union(_cmd, ",M", arg.dotted());
    }
    this.builder.push(_cmd);
    return this;
  }

  @Override
  public QR588Q raw(ARaw raw) {
    switch (raw.mode()) {
      case TEXT:
        this.builder.push(raw.string(), raw.charset(), raw.newline());
        break;
      case BINARY:
        this.builder.push(raw.binary(), raw.newline());
        break;
      default:
        return this;
    }
    return this;
  }

  @Override
  public QR588Q variable(String name, Object value) {
    this.builder.variable(name, value);
    return this;
  }

  @Override
  public Command command() {
    return this.builder.build();
  }

  @Override
  public void clear() {
    this.builder.clear();
  }
}
