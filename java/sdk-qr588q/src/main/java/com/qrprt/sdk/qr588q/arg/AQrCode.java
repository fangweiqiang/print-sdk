package com.qrprt.sdk.qr588q.arg;

import com.qrprt.sdk.api.CArg;

public class AQrCode implements CArg {

  private final int x;
  private final int y;
  private final String text;
  private final int width;

  private AQrCode(Builder builder) {
    this.x = builder.x;
    this.y = builder.y;
    this.text = builder.text;
    this.width = builder.width;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int x() {
    return x;
  }

  public int y() {
    return y;
  }

  public String text() {
    return text;
  }

  public int width() {
    return width;
  }

  public static class Builder {

    private int x;
    private int y;
    private String text;
    private int width;

    public Builder() {
      this.x = -1;
      this.y = -1;
      this.width = -1;
    }

    public AQrCode build() {
      if (this.x == -1)
        throw new IllegalArgumentException("x coordinates cannot be null");
      if (this.y == -1)
        throw new IllegalArgumentException("y coordinates cannot be null");
      if (this.text == null)
        throw new IllegalArgumentException("text cannot be null");
      if (this.width == -1)
        throw new IllegalArgumentException("width cannot be null");
      return new AQrCode(this);
    }

    public Builder x(int x) {
      this.x = x;
      return this;
    }

    public Builder y(int y) {
      this.y = y;
      return this;
    }

    public Builder text(String text) {
      this.text = text;
      return this;
    }

    public Builder width(int width) {
      this.width = width;
      return this;
    }
  }

}
