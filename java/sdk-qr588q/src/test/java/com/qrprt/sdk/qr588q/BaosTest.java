package com.qrprt.sdk.qr588q;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class BaosTest {


  @Test
  public void testClose() {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    baos.write(1);
    baos.write(2);
    baos.write(3);
    System.out.println(Arrays.toString(baos.toByteArray()));

    try {
      baos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    baos.reset();
    baos.write(4);
    baos.write(5);
    baos.write(6);
    System.out.println(Arrays.toString(baos.toByteArray()));
  }

}
