package com.qrprt.beeprt;


public class BeeprtCommand {
  private c command = new c();

  public byte[] Beeprt_CreatePage(int paramInt1, int paramInt2) {
    return this.command.a(paramInt1, paramInt2);
  }

  public byte[] Beeprt_PrintPage(int paramInt) {
    return this.command.a(paramInt);
  }

  public byte[] Beeprt_Text(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, int paramInt5, String paramString2) {
    return this.command.a(paramInt1, paramInt2, paramString1, paramInt3, paramInt4, paramInt5, paramString2);
  }

  public byte[] Beeprt_Text(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean, String paramString2) {
    return this.command.a(paramInt1, paramInt2, paramString1, paramInt3, paramInt4, paramInt5, paramBoolean, paramString2);
  }

  public byte[] Beeprt_Textbox(int paramInt1, int paramInt2, String paramString1, int paramInt3, boolean paramBoolean, int paramInt4, int paramInt5, int paramInt6, int paramInt7, String paramString2) {
    return this.command.a(paramInt1, paramInt2, paramString1, paramInt3, paramBoolean, paramInt4, paramInt5, paramInt6, paramInt7, paramString2);
  }

  public byte[] Beeprt_Textbox(int paramInt1, int paramInt2, String paramString1, int paramInt3, boolean paramBoolean1, int paramInt4, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean2, String paramString2) {
    return this.command.a(paramInt1, paramInt2, paramString1, paramInt3, paramBoolean1, paramInt4, paramInt5, paramInt6, paramInt7, paramBoolean2, paramString2);
  }

  public byte[] Beeprt_DrawLine(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    return this.command.a(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5);
  }

  public byte[] Beeprt_DrawLine(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    return this.command.a(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
  }

  public byte[] Beeprt_DrawBar(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, String paramString) {
    return this.command.a(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramInt7, paramString);
  }

  public byte[] Beeprt_DrawQRCode(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString) {
    return this.command.a(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramString);
  }

  public byte[] Beeprt_DrawQRCode(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, String paramString) {
    return this.command.a(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramString);
  }

  public byte[] Beeprt_DrawPic(int paramInt1, int paramInt2, Bitmap paramBitmap) {
    return this.command.a(paramInt1, paramInt2, paramBitmap);
  }

  public byte[] Beeprt_DrawBox(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    return this.command.b(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
  }

  public byte[] Beeprt_DrawCircle(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    return this.command.a(paramInt1, paramInt2, paramInt3, paramInt4);
  }

  public byte[] Beeprt_Density(int paramInt) {
    return this.command.b(paramInt);
  }

  public byte[] Beeprt_Speed(int paramInt) {
    return this.command.c(paramInt);
  }

  public byte[] Beeprt_Cls() {
    return this.command.a();
  }

  public byte[] Beeprt_Cut(boolean paramBoolean) {
    return this.command.a(paramBoolean);
  }

  public byte[] Beeprt_Direction(int paramInt1, int paramInt2) {
    return this.command.b(paramInt1, paramInt2);
  }

  public byte[] Beeprt_SetGap(boolean paramBoolean) {
    return this.command.b(paramBoolean);
  }

  public byte[] Beeprt_DrawDataMatrix(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString) {
    return this.command.b(paramInt1, paramInt2, paramInt3, paramInt4, paramString);
  }

  public int Beeprt_PrinterState(PrintPort paramPrintPort) {
    return this.command.a(paramPrintPort);
  }

  public int Beeprt_CheckPaper() {
    return this.command.b();
  }

  public boolean Beeprt_isPicked() {
    return this.command.c();
  }

  public boolean Beeprt_CheckCut() {
    return this.command.d();
  }

  public boolean Beeprt_isBusy() {
    return this.command.e();
  }
}


/* Location:              D:\dev\printer\lib\BeerptPrinter_BY_Ver1.1.0.jar!\com\beeprt\BeeprtCommand.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */
