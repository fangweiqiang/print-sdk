package com.qrprt.beeprt;

import java.util.ArrayList;

public class PrintPort {
  private d printPP = new d();

  public boolean connect(String paramString1, String paramString2) {
    return this.printPP.a(paramString1, paramString2);
  }

  public void disconnect() {
    this.printPP.a();
  }

  public boolean isConnected() {
    return this.printPP.b();
  }

  public boolean portSendCmd(String paramString) {
    return this.printPP.a(paramString);
  }

  public boolean portSendCmd(byte[] paramArrayOfbyte) {
    return this.printPP.a(paramArrayOfbyte);
  }

  public boolean portSendCmd(ArrayList<byte[]> paramArrayList) {
    return this.printPP.a(paramArrayList);
  }

  public byte[] read(int paramInt) {
    return this.printPP.a(paramInt);
  }

  public String printerType() {
    return this.printPP.c();
  }
}


/* Location:              D:\dev\printer\lib\BeerptPrinter_BY_Ver1.1.0.jar!\com\beeprt\PrintPort.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */
