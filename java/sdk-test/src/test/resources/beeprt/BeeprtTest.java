package com.qrprt.beeprt;

import java.util.ArrayList;

public class BeeprtTest {


  public void printebmpData() {
    if (isConnected) {

      Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.unititled);

      ArrayList<byte[]> data = new ArrayList<byte[]>();
      byte[] wakeup = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
      BeeprtCommand printer = new BeeprtCommand();
      data.add(wakeup);
      data.add(printer.Beeprt_CreatePage(100, 175));
      // 设置打印方向
      data.add(printer.Beeprt_Direction(0, 0));
      // 始能切刀，没打完一张自动切纸
      data.add(printer.Beeprt_Cut(true));
      // 设置缝隙定位
      data.add(printer.Beeprt_SetGap(true));
      // 设置速度3
      data.add(printer.Beeprt_Speed(6));
      // 设置浓度
      data.add(printer.Beeprt_Density(5));
      // 清除页面缓冲区
      data.add(printer.Beeprt_Cls());

      data.add(printer.Beeprt_DrawPic(0, 0, bitmap));
      data.add(printer.Beeprt_PrintPage(1));
      printPort.portSendCmd(data);

    }
  }


  public static void main(String[] args) {

  }
}
