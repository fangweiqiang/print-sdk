package com.qrprt.beeprt;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class c {
  public String a = "20180507_Ver1.1.2";

  private int b = 0;

  private boolean c = false;

  private boolean d = true;

  private boolean e = false;

  public byte[] a(int paramInt1, int paramInt2) {
    String str = "SIZE " + paramInt1 + " mm," + paramInt2 + " mm\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt) {
    String str = "PRINT " + paramInt + "\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, int paramInt5, String paramString2) {
    String str1 = paramString2.replace("\"", "\\[\"]");
    String str2 = "TEXT " + paramInt1 + "," + paramInt2 + ",\"" + paramString1 + "\"," + paramInt3 + "," + paramInt4 + "," + paramInt5 + ",\"" + str1 + "\"\r\n";
    try {
      return str2.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, String paramString1, int paramInt3, int paramInt4, int paramInt5, boolean paramBoolean, String paramString2) {
    String str1 = null;
    String str2 = paramString2.replace("\"", "\\[\"]");
    if (paramBoolean) {
      str1 = "TEXT " + paramInt1 + "," + paramInt2 + ",\"" + paramString1 + "\"," + paramInt3 + "," + paramInt4 + "," + paramInt5 + ",B1,\"" + str2 + "\"\r\n";
    } else {
      str1 = "TEXT " + paramInt1 + "," + paramInt2 + ",\"" + paramString1 + "\"," + paramInt3 + "," + paramInt4 + "," + paramInt5 + ",\"" + str2 + "\"\r\n";
    }
    try {
      return str1.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, String paramString1, int paramInt3, boolean paramBoolean, int paramInt4, int paramInt5, int paramInt6, int paramInt7, String paramString2) {
    String str1 = paramString2.replace("\"", "\\[\"]");
    String str2 = null;
    int i = 0;
    if (paramString1.equals("16"))
      i = 16 * paramInt5;
    if (paramString1.equals("24"))
      i = 24 * paramInt5;
    if (paramString1.equals("32"))
      i = 32 * paramInt5;
    str2 = "TEXTBOX " + paramInt1 + "," + paramInt2 + ",\"" + paramString1 + "\"," + paramInt3 + "," + paramInt4 + "," + paramInt5 + "," + paramInt6 + ",";
    if (paramInt7 >= i)
      str2 = str2 + "L" + paramInt7 + ",";
    if (paramBoolean)
      str2 = str2 + "D" + paramInt3 + ",";
    str2 = str2 + "\"" + str1 + "\"\r\n";
    try {
      return str2.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, String paramString1, int paramInt3, boolean paramBoolean1, int paramInt4, int paramInt5, int paramInt6, int paramInt7, boolean paramBoolean2, String paramString2) {
    String str1 = paramString2.replace("\"", "\\[\"]");
    String str2 = null;
    int i = 0;
    if (paramString1.equals("16"))
      i = 16 * paramInt5;
    if (paramString1.equals("24"))
      i = 24 * paramInt5;
    if (paramString1.equals("32"))
      i = 32 * paramInt5;
    str2 = "TEXTBOX " + paramInt1 + "," + paramInt2 + ",\"" + paramString1 + "\"," + paramInt3 + "," + paramInt4 + "," + paramInt5 + "," + paramInt6 + ",";
    if (paramInt7 >= i)
      str2 = str2 + "L" + paramInt7 + ",";
    if (paramBoolean2)
      str2 = str2 + "B1,";
    if (paramBoolean1)
      str2 = str2 + "D" + paramInt3 + ",";
    str2 = str2 + "\"" + str1 + "\"\r\n";
    try {
      return str2.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    String str = "BAR " + paramInt1 + "," + paramInt2 + "," + paramInt3 + "," + paramInt4 + "," + paramInt5 + "\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    String str = null;
    switch (paramInt6) {
      case 0:
        str = "LINE " + paramInt1 + "," + paramInt2 + "," + paramInt3 + "," + paramInt4 + "," + paramInt5 + "\r\n";
        break;
      case 1:
        str = "LINE " + paramInt1 + "," + paramInt2 + "," + paramInt3 + "," + paramInt4 + "," + paramInt5 + ",M1\r\n";
        break;
      case 2:
        str = "LINE " + paramInt1 + "," + paramInt2 + "," + paramInt3 + "," + paramInt4 + "," + paramInt5 + ",M2\r\n";
        break;
      case 3:
        str = "LINE " + paramInt1 + "," + paramInt2 + "," + paramInt3 + "," + paramInt4 + "," + paramInt5 + ",M3\r\n";
        break;
      case 4:
        str = "LINE " + paramInt1 + "," + paramInt2 + "," + paramInt3 + "," + paramInt4 + "," + paramInt5 + ",M4\r\n";
        break;
    }
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString) {
    Object object;
    String str = "BITMAP " + paramInt1 + "," + paramInt2 + "," + (paramInt3 / 8) + "," + paramInt4 + ",1,";
    byte[] arrayOfByte1 = new byte[paramString.length() / 2];
    for (byte b = 0; b < arrayOfByte1.length; b++) {
      try {
        arrayOfByte1[b] = (byte)(0xFF & Integer.parseInt(paramString.substring(b * 2, b * 2 + 2), 16) ^ 0xFFFFFFFF);
      } catch (Exception exception) {
        exception.printStackTrace();
      }
    }
    try {
      object = str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      object = null;
    }
    byte[] arrayOfByte2 = new byte[object.length + arrayOfByte1.length];
    System.arraycopy(object, 0, arrayOfByte2, 0, object.length);
    System.arraycopy(arrayOfByte1, 0, arrayOfByte2, object.length, arrayOfByte1.length);
    return arrayOfByte2;
  }

  public byte[] a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, String paramString) {
    String str1 = "128";
    switch (paramInt3) {
      case 0:
        str1 = "128";
        break;
      case 1:
        str1 = "39";
        break;
      case 2:
        str1 = "93";
        break;
      case 3:
        str1 = "ITF";
        break;
      case 4:
        str1 = "UPCA";
        break;
      case 5:
        str1 = "UPCE";
        break;
      case 6:
        str1 = "CODABAR";
        break;
      case 7:
        str1 = "EAN8";
        break;
      case 8:
        str1 = "EAN13";
        break;
    }
    String str2 = "BARCODE " + paramInt1 + "," + paramInt2 + ",\"" + str1 + "\"," + paramInt4 + "," + paramInt5 + "," + paramInt6 + "," + paramInt7 + "," + paramInt7 + ",\"" + paramString + "\"\r\n";
    try {
      return str2.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString) {
    String str1 = "Q";
    switch (paramInt3) {
      case 0:
        str1 = "L";
        break;
      case 1:
        str1 = "M";
        break;
      case 2:
        str1 = "Q";
        break;
      case 3:
        str1 = "H";
        break;
    }
    String str2 = "QRCODE " + paramInt1 + "," + paramInt2 + "," + str1 + "," + paramInt5 + ",A," + paramInt4 + ",M2,S7,\"" + paramString + "\"\r\n";
    try {
      return str2.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, String paramString) {
    String str1 = "Q";
    if (paramInt6 < 0 || paramInt6 > 40)
      return null;
    switch (paramInt3) {
      case 0:
        str1 = "L";
        break;
      case 1:
        str1 = "M";
        break;
      case 2:
        str1 = "Q";
        break;
      case 3:
        str1 = "H";
        break;
    }
    String str2 = "QRCODE " + paramInt1 + "," + paramInt2 + "," + str1 + "," + paramInt5 + ",A," + paramInt4 + ",M2,S7,V" + paramInt6 + ",\"" + paramString + "\"\r\n";
    try {
      return str2.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, Bitmap paramBitmap) {
    Object object;
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    int k = (i % 8 == 0) ? (i / 8) : (i / 8 + 1);
    byte[] arrayOfByte1 = Code.code(a(paramBitmap, i, j));
    String str1 = "BITMAP " + paramInt1 + "," + paramInt2 + "," + k + "," + j + ",3," + arrayOfByte1.length + ",";
    try {
      object = str1.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      object = null;
    }
    String str2 = "\r\n";
    byte[] arrayOfByte2 = new byte[0];
    try {
      arrayOfByte2 = str2.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
    }
    byte[] arrayOfByte3 = new byte[object.length + arrayOfByte1.length];
    System.arraycopy(object, 0, arrayOfByte3, 0, object.length);
    System.arraycopy(arrayOfByte1, 0, arrayOfByte3, object.length, arrayOfByte1.length);
    byte[] arrayOfByte4 = new byte[arrayOfByte3.length + arrayOfByte2.length];
    System.arraycopy(arrayOfByte3, 0, arrayOfByte4, 0, arrayOfByte3.length);
    System.arraycopy(arrayOfByte2, 0, arrayOfByte4, arrayOfByte3.length, arrayOfByte2.length);
    return arrayOfByte4;
  }

  private byte[] a(Bitmap paramBitmap, int weight, int height) {
    int eWeight = 0;
    byte currentHeight = 0;
    int index = 0;
    try {
      eWeight = (weight % 8 == 0) ? (weight / 8) : (weight / 8 + 1);
      int area = height * eWeight;
      byte[] bytes = new byte[area];
      for (byte b1 = 0; b1 < area; b1++)
        bytes[b1] = 0;
      while (currentHeight < height) {
        int[] arrayOfInt = new int[weight];
        /*
        void getPixels (int[] pixels,
                int offset,
                int stride,
                int x,
                int y,
                int width,
                int height)
         */
        paramBitmap.getPixels(arrayOfInt, 0, weight, 0, currentHeight, weight, 1);
        byte eightBitIndex = 0;
        for (byte b3 = 0; b3 < weight; b3++) {
          eightBitIndex++;
          int m = arrayOfInt[b3];
          if (eightBitIndex > 8) {
            eightBitIndex = 1;
            index++;
          }
          if (m != -1) {
            int n = 1 << 8 - eightBitIndex;
            int i1 = Color.red(m);
            int i2 = Color.green(m);
            int i3 = Color.blue(m);
            if ((i1 + i2 + i3) / 3 < 128)
              bytes[index] = (byte)(bytes[index] | n);
          }
        }
        index = eWeight * (currentHeight + 1);
        currentHeight++;
      }
      return bytes;
    } catch (Exception exception) {
      exception.printStackTrace();
      return null;
    }
  }

  public byte[] b(int paramInt1, int paramInt2, Bitmap paramBitmap) {
    int i;
    Object object;
    long l1 = System.currentTimeMillis();
    int j = paramBitmap.getWidth();
    int k = paramBitmap.getHeight();
    if (j % 8 != 0) {
      i = j / 8 + 1;
    } else {
      i = j / 8;
    }
    byte[] arrayOfByte1 = new byte[i * k];
    byte b1 = 0;
    for (byte b2 = 0; b2 < k; b2++) {
      for (byte b = 0; b < i; b++) {
        byte b3 = 0;
        for (byte b4 = 0; b4 < 8; b4++) {
          if (b * 8 + b4 < j) {
            int m = paramBitmap.getPixel(b * 8 + b4, b2);
            if (m != -1 && m != 0)
              b3 = (byte)(b3 | (byte)(128 >> b4));
          }
        }
        arrayOfByte1[b1++] = b3;
      }
    }
    ArrayList<byte[]> arrayList = new ArrayList();
    arrayList.add(arrayOfByte1);
    byte[] arrayOfByte2 = Code.code(arrayOfByte1);
    String str1 = "BITMAP " + paramInt1 + "," + paramInt2 + "," + i + "," + k + ",3," + arrayOfByte2.length + ",";
    try {
      object = str1.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      object = null;
    }
    String str2 = "\r\n";
    byte[] arrayOfByte3 = new byte[0];
    try {
      arrayOfByte3 = str2.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
    }
    byte[] arrayOfByte4 = new byte[object.length + arrayOfByte2.length];
    System.arraycopy(object, 0, arrayOfByte4, 0, object.length);
    System.arraycopy(arrayOfByte2, 0, arrayOfByte4, object.length, arrayOfByte2.length);
    byte[] arrayOfByte5 = new byte[arrayOfByte4.length + arrayOfByte3.length];
    System.arraycopy(arrayOfByte4, 0, arrayOfByte5, 0, arrayOfByte4.length);
    System.arraycopy(arrayOfByte3, 0, arrayOfByte5, arrayOfByte4.length, arrayOfByte3.length);
    long l2 = System.currentTimeMillis() - l1;
    return arrayOfByte5;
  }

  public byte[] c(int paramInt1, int paramInt2, Bitmap paramBitmap) {
    int i;
    Object object;
    long l1 = System.currentTimeMillis();
    int j = paramBitmap.getWidth();
    int k = paramBitmap.getHeight();
    if (j % 8 != 0) {
      i = j / 8 + 1;
    } else {
      i = j / 8;
    }
    String str = "BITMAP " + paramInt1 + "," + paramInt2 + "," + i + "," + k + ",1,";
    try {
      object = str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      object = null;
    }
    byte[] arrayOfByte1 = new byte[i * k];
    byte b1 = 0;
    for (byte b2 = 0; b2 < k; b2++) {
      for (byte b = 0; b < i; b++) {
        byte b3 = 0;
        for (byte b4 = 0; b4 < 8; b4++) {
          if (b * 8 + b4 < j) {
            int m = paramBitmap.getPixel(b * 8 + b4, b2);
            if (m == -1 || m == 0)
              b3 = (byte)(b3 | (byte)(128 >> b4));
          }
        }
        arrayOfByte1[b1++] = b3;
      }
    }
    byte[] arrayOfByte2 = new byte[object.length + arrayOfByte1.length];
    System.arraycopy(object, 0, arrayOfByte2, 0, object.length);
    System.arraycopy(arrayOfByte1, 0, arrayOfByte2, object.length, arrayOfByte1.length);
    long l2 = System.currentTimeMillis() - l1;
    return arrayOfByte2;
  }

  public byte[] b(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    String str = "BOX " + paramInt1 + "," + paramInt2 + "," + paramInt3 + "," + paramInt4 + "," + paramInt5 + "," + paramInt6 + "\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    String str = "CIRCLE " + paramInt1 + "," + paramInt2 + "," + paramInt3 + "," + paramInt4 + "\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(String paramString) {
    try {
      return paramString.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] b(int paramInt) {
    String str = "DENSITY " + paramInt + "\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] c(int paramInt) {
    String str = "SPEED " + paramInt + "\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a() {
    String str = "CLS\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] a(boolean paramBoolean) {
    if (paramBoolean) {
      String str1 = "SET CUTTER 1\r\n";
      try {
        return str1.getBytes("gb2312");
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        unsupportedEncodingException.printStackTrace();
        return null;
      }
    }
    String str = "SET CUTTER OFF\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] b(int paramInt1, int paramInt2) {
    String str = "DIRECTION " + paramInt1 + "," + paramInt2 + "\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] b(boolean paramBoolean) {
    if (paramBoolean) {
      String str1 = "SET GAP ON\r\n";
      try {
        return str1.getBytes("gb2312");
      } catch (UnsupportedEncodingException unsupportedEncodingException) {
        unsupportedEncodingException.printStackTrace();
        return null;
      }
    }
    String str = "SET GAP OFF\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public byte[] b(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString) {
    String str = "DMATRIX " + paramInt1 + "," + paramInt2 + "," + paramInt3 + "," + paramInt4 + ",\"" + paramString + "\"\r\n";
    try {
      return str.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      return null;
    }
  }

  public int a(PrintPort paramPrintPort) {
    this.b = 0;
    this.d = true;
    this.c = false;
    this.e = false;
    String str = "READSTA \r\n";
    try {
      paramPrintPort.portSendCmd(str.getBytes("gb2312"));
      Thread.sleep(300L);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    try {
      byte[] arrayOfByte = paramPrintPort.read(2000);
      String str1 = new String(arrayOfByte, "gb2312");
      String[] arrayOfString = str1.split("[ ,]");
      int i = arrayOfString.length;
      if (i == 2)
        b(arrayOfString[1]);
      if (i == 3) {
        b(arrayOfString[1]);
        c(arrayOfString[2]);
      }
      if (i == 4) {
        b(arrayOfString[1]);
        c(arrayOfString[2]);
        e(arrayOfString[3]);
      }
      if (i == 5) {
        b(arrayOfString[1]);
        c(arrayOfString[2]);
        e(arrayOfString[3]);
        d(arrayOfString[4]);
      }
      return 1;
    } catch (IOException iOException) {
      iOException.printStackTrace();
      return 0;
    }
  }

  private void b(String paramString) {
    if (paramString.equals("LIBOPEN")) {
      this.b = 3;
    } else if (paramString.equals("NOPAPER")) {
      this.b = 0;
    } else if (paramString.equals("PAPEREND")) {
      this.b = 2;
    } else if (paramString.equals("PAPER")) {
      this.b = 1;
    } else if (paramString.equals("PAPERERR")) {
      this.b = 4;
    }
  }

  private void c(String paramString) {
    if (paramString.equals("WAITPICK")) {
      this.c = false;
    } else if (paramString.equals("PICK")) {
      this.c = true;
    }
  }

  private void d(String paramString) {
    if (paramString.equals("CUTERERR")) {
      this.e = false;
    } else if (paramString.equals("CUTEROK")) {
      this.e = true;
    }
  }

  private void e(String paramString) {
    if (paramString.equals("IDLE")) {
      this.d = false;
    } else if (paramString.equals("BUSY")) {
      this.d = true;
    }
  }

  public int b() {
    return this.b;
  }

  public boolean c() {
    return this.c;
  }

  public boolean d() {
    return this.e;
  }

  public boolean e() {
    return this.d;
  }

  public String a(FileInputStream paramFileInputStream, FileOutputStream paramFileOutputStream) {
    try {
      int i = paramFileInputStream.available();
      byte[] arrayOfByte = new byte[i];
      int j;
      if (i <= 0 || (j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1);
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    String str = "READC PRDOCUTID\r\n";
    try {
      paramFileOutputStream.write(str.getBytes("gb2312"));
      Thread.sleep(1000L);
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } catch (InterruptedException interruptedException) {
      interruptedException.printStackTrace();
    }
    try {
      int i = paramFileInputStream.available();
      byte[] arrayOfByte = new byte[i];
      int j;
      if (i > 0 && (j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1)
        return new String(arrayOfByte, "gb2312");
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    return null;
  }

  public String b(FileInputStream paramFileInputStream, FileOutputStream paramFileOutputStream) {
    try {
      int i = paramFileInputStream.available();
      byte[] arrayOfByte = new byte[i];
      int j;
      if (i <= 0 || (j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1);
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    String str = "READC VERSION \r\n";
    try {
      paramFileOutputStream.write(str.getBytes("gb2312"));
      Thread.sleep(1000L);
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } catch (InterruptedException interruptedException) {
      interruptedException.printStackTrace();
    }
    try {
      int i = paramFileInputStream.available();
      if (i > 0) {
        byte[] arrayOfByte = new byte[i];
        int j;
        if ((j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1)
          return new String(arrayOfByte, "gb2312");
      } else {
        return null;
      }
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    return null;
  }

  public int c(FileInputStream paramFileInputStream, FileOutputStream paramFileOutputStream) {
    try {
      int i = paramFileInputStream.available();
      byte[] arrayOfByte = new byte[i];
      int j;
      if (i <= 0 || (j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1);
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    String str = "READSTA \r\n";
    try {
      paramFileOutputStream.write(str.getBytes("gb2312"));
      Thread.sleep(200L);
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } catch (InterruptedException interruptedException) {
      interruptedException.printStackTrace();
    }
    try {
      int i = paramFileInputStream.available();
      if (i > 0) {
        byte[] arrayOfByte = new byte[i];
        int j;
        while ((j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1) {
          String str1 = new String(arrayOfByte, "gb2312");
          String[] arrayOfString = str1.split("[ ,]");
          if (arrayOfString[1].equals("LIBOPEN"))
            return 3;
          if (arrayOfString[1].equals("NOPAPER"))
            return 0;
          if (arrayOfString[1].equals("PAPEREND"))
            return 2;
          if (arrayOfString[1].equals("PAPER"))
            return 1;
          if (arrayOfString[1].equals("PAPERERR"))
            return 4;
        }
      }
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    return 0;
  }

  public boolean d(FileInputStream paramFileInputStream, FileOutputStream paramFileOutputStream) {
    try {
      int i = paramFileInputStream.available();
      byte[] arrayOfByte = new byte[i];
      int j;
      if (i <= 0 || (j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1);
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    String str = "READSTA \r\n";
    try {
      paramFileOutputStream.write(str.getBytes("gb2312"));
      Thread.sleep(200L);
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } catch (InterruptedException interruptedException) {
      interruptedException.printStackTrace();
    }
    try {
      int i = paramFileInputStream.available();
      if (i > 0) {
        byte[] arrayOfByte = new byte[i];
        int j;
        while ((j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1) {
          String str1 = new String(arrayOfByte, "gb2312");
          String[] arrayOfString = str1.split("[ ,]");
          if (arrayOfString[2].equals("WAITPICK"))
            return false;
          if (arrayOfString[2].equals("PICK"))
            return true;
        }
      }
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    return false;
  }

  public boolean e(FileInputStream paramFileInputStream, FileOutputStream paramFileOutputStream) {
    try {
      int i = paramFileInputStream.available();
      byte[] arrayOfByte = new byte[i];
      int j;
      if (i <= 0 || (j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1);
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    String str = "READSTA \r\n";
    try {
      paramFileOutputStream.write(str.getBytes("gb2312"));
      Thread.sleep(200L);
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } catch (InterruptedException interruptedException) {
      interruptedException.printStackTrace();
    }
    try {
      int i = paramFileInputStream.available();
      if (i > 0) {
        byte[] arrayOfByte = new byte[i];
        int j;
        while ((j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1) {
          String str1 = new String(arrayOfByte, "gb2312");
          String[] arrayOfString = str1.split("[ ,]");
          if (arrayOfString[4].equals("CUTERERR"))
            return false;
          if (arrayOfString[4].equals("CUTEROK"))
            return true;
        }
      }
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    return false;
  }

  public boolean f(FileInputStream paramFileInputStream, FileOutputStream paramFileOutputStream) {
    try {
      int i = paramFileInputStream.available();
      byte[] arrayOfByte = new byte[i];
      int j;
      if (i <= 0 || (j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1);
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    String str = "READSTA \r\n";
    try {
      paramFileOutputStream.write(str.getBytes("gb2312"));
      Thread.sleep(200L);
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
    } catch (IOException iOException) {
      iOException.printStackTrace();
    } catch (InterruptedException interruptedException) {
      interruptedException.printStackTrace();
    }
    try {
      int i = paramFileInputStream.available();
      if (i > 0) {
        byte[] arrayOfByte = new byte[i];
        int j;
        while ((j = paramFileInputStream.read(arrayOfByte, 0, arrayOfByte.length)) != -1) {
          String str1 = new String(arrayOfByte, "gb2312");
          String[] arrayOfString = str1.split("[ ,]");
          if (arrayOfString[3].equals("IDLE"))
            return false;
          if (arrayOfString[3].equals("BUSY"))
            return true;
        }
      }
    } catch (IOException iOException) {
      iOException.printStackTrace();
    }
    return false;
  }

  private String a(byte[] paramArrayOfbyte) {
    char[] arrayOfChar1 = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'A', 'B', 'C', 'D', 'E', 'F' };
    char[] arrayOfChar2 = new char[paramArrayOfbyte.length * 2];
    for (byte b = 0; b < paramArrayOfbyte.length; b++) {
      int i = paramArrayOfbyte[b] & 0xFF;
      arrayOfChar2[b * 2] = arrayOfChar1[i >>> 4];
      arrayOfChar2[b * 2 + 1] = arrayOfChar1[i & 0xF];
    }
    return new String(arrayOfChar2);
  }

  private void a(String paramString, ArrayList<byte[]> paramArrayList) {
    File file = new File(Environment.getExternalStorageDirectory(), paramString);
    FileOutputStream fileOutputStream = new FileOutputStream(file);
    byte[] arrayOfByte = new byte[0];
    for (byte b = 0; b < paramArrayList.size(); b++)
      arrayOfByte = d.a(arrayOfByte, paramArrayList.get(b));
    fileOutputStream.write(arrayOfByte);
    fileOutputStream.close();
  }
}


/* Location:              D:\dev\printer\lib\BeerptPrinter_BY_Ver1.1.0.jar!\com\beeprt\c.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */
