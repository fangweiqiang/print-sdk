package com.qrprt.beeprt;

public class Code {
  public static native byte[] code(byte[] paramArrayOfbyte);

  public static native byte[] decode(byte[] paramArrayOfbyte);

  static {
    System.loadLibrary("Code");
  }
}


/* Location:              D:\dev\printer\lib\BeerptPrinter_BY_Ver1.1.0.jar!\com\beeprt\Code.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */
