package com.qrprt.ebeeprt;

import com.qrprt.lib.ZLibUtils;
import io.enoa.http.Http;
import io.enoa.toolkit.file.FileKit;
import io.enoa.toolkit.number.HexKit;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class EBeeprtTeest {


  public static void main(String[] args) throws IOException {
    EBeeprtTeest test = new EBeeprtTeest();
    test.testBufferImage();
  }

  private void testBufferImage() throws IOException {
    byte[] bytes = Http.request("https://mio.kahub.in/open/waybill-usps.png")
      .emit()
      .body()
      .bytes();
//    FileKit.write(Paths.get("D:/dev/printer/assets/image/test.jpg"), ByteBuffer.wrap(bytes));
    InputStream is = new ByteArrayInputStream(bytes);
    BufferedImage image = ImageIO.read(is);
//    ImageIO.write(image, "jpg", new File("D:/dev/printer/assets/image/test.jpg"));


//    BufferedImage image = ImageIO.read(new File("D:/dev/printer/assets/image/timg4.png"));

    byte[] prtdata = this.imageCommand(5, 5, image);

    String page = "SIZE 100 mm,180 mm\r\n";
    String gap = "GAP 0,0\r\n";
    String cls = "CLS\r\n";
    String print = "PRINT 1\r\n";
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    baos.write(new byte[]{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
    baos.write(page.getBytes(Charset.forName("gbk")));
    baos.write(gap.getBytes(Charset.forName("gbk")));
    baos.write(cls.getBytes(Charset.forName("gbk")));
    baos.write(prtdata);
    baos.write("\r\n".getBytes(Charset.forName("gbk")));
    baos.write(print.getBytes(Charset.forName("gbk")));

//    System.out.println(HexKit.hex(prtdata));
    HexKit.HexToStringOptions options = new HexKit.HexToStringOptions();
    options.setMaxLineLength(64);
    System.out.println(HexKit.hex(baos.toByteArray(), options));
  }

  public byte[] imageCommand(int _x, int _y, BufferedImage paramBitmap) {
    byte[] object;
    int width = paramBitmap.getWidth();
    int height = paramBitmap.getHeight();
    int eWeight = (width % 8 == 0) ? (width / 8) : (width / 8 + 1);
    byte[] arrayOfByte1 = ZLibUtils.compress(handleImage(paramBitmap, width, height));
    String str1 = "BITMAP " + _x + "," + _y + "," + eWeight + "," + height + ",3," + arrayOfByte1.length + ",";


//    byte[] arrayOfByte1 = handleImage(paramBitmap, i, j);
//    String str1 = "BITMAP " + _x + "," + _y + "," + k + "," + j + ",0,";
    try {
      object = str1.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
      object = null;
    }
    String str2 = "\r\n";
    byte[] arrayOfByte2 = new byte[0];
    try {
      arrayOfByte2 = str2.getBytes("gb2312");
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      unsupportedEncodingException.printStackTrace();
    }
    byte[] arrayOfByte3 = new byte[object.length + arrayOfByte1.length];
    System.arraycopy(object, 0, arrayOfByte3, 0, object.length);
    System.arraycopy(arrayOfByte1, 0, arrayOfByte3, object.length, arrayOfByte1.length);
    byte[] arrayOfByte4 = new byte[arrayOfByte3.length + arrayOfByte2.length];
    System.arraycopy(arrayOfByte3, 0, arrayOfByte4, 0, arrayOfByte3.length);
    System.arraycopy(arrayOfByte2, 0, arrayOfByte4, arrayOfByte3.length, arrayOfByte2.length);
    return arrayOfByte4;
  }

  private byte[] handleImage(BufferedImage paramBitmap, int weight, int height) {
    int currentHeight = 0;
    int index = 0;
    try {
      int eWeight = (weight % 8 == 0) ? (weight / 8) : (weight / 8 + 1);
      int area = height * eWeight;
      byte[] bytes = new byte[area];
      for (int b1 = 0; b1 < area; b1++) {
        bytes[b1] = 0;
      }
      while (currentHeight < height) {
        int eightBitIndex = 0;
        for (int x = 0; x < weight; x++) {
          eightBitIndex++;
          if (eightBitIndex > 8) {
            eightBitIndex = 1;
            index++;
          }
          int _pixel = paramBitmap.getRGB(x, currentHeight);
          Color pixel = new Color(_pixel);
          int n = 1 << 8 - eightBitIndex;

          int r = (_pixel >> 16) & 0xFF;
          int g = (_pixel >> 8) & 0xFF;
          int b = (_pixel >> 0) & 0xFF;

//          int r = pixel.getRed();
//          int g = pixel.getGreen();
//          int b = pixel.getBlue();

          if ((r + g + b) / 3 < 128) {
            bytes[index] = (byte) (bytes[index] | n);
          }
        }
        index = eWeight * (currentHeight + 1);
        currentHeight += 1;
      }

      return bytes;
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException(e.getMessage(), e);
    }
  }
}
