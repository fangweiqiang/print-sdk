package com.qrprt.sdk.qr386;

import com.qrprt.sdk.api.Command;
import com.qrprt.sdk.qr386.arg.AInit;
import com.qrprt.sdk.qr386.arg.AText;
import org.junit.Test;

public class QR386Test {


  @Test
  public void testCommand() {
    Command command = QR386.use()
      .init(AInit.builder().build())
      .text(AText.builder().x(1).y(2).text("My name is{{name}} and age is {{age}} .{{test}} penetrate \\{{name}}").build())
      .print(Boolean.FALSE)
      .variable("name", "Jack")
      .variable("age", 5)
      .command();
    System.out.println(command.string());
  }

}
