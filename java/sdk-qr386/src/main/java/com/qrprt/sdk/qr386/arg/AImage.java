package com.qrprt.sdk.qr386.arg;

import com.qrprt.sdk.api.CArg;
import com.qrprt.sdk.qr386.arg.ii.IImage;

public class AImage implements CArg {

  /**
   * x 座标
   */
  private int x;
  /**
   * y 座标
   */
  private int y;
  /**
   * width 宽度
   */
  private int width;
  /**
   * height 高度
   */
  private int height;
//  /**
//   * bitmap hex image
//   */
//  private T bitmap;

  private AImage(Builder builder) {
    this.x = builder.x;
    this.y = builder.y;
    this.width = builder.width;
    this.height = builder.height;
//    this.bitmap = builder.bitmap;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int x() {
    return x;
  }

  public int y() {
    return y;
  }

  public int width() {
    return width;
  }

  public int height() {
    return height;
  }

//  public T bitmap() {
//    return bitmap;
//  }

  public static class Builder {

    private int x;
    private int y;
    private int width;
    private int height;
//    private T bitmap;

    public Builder() {
      this.x = -1;
      this.y = -1;
      this.width = -1;
      this.height = -1;
    }

    public AImage build() {
      if (this.x == -1)
        throw new IllegalArgumentException("x cannot be null");
      if (this.y == -1)
        throw new IllegalArgumentException("y cannot be null");
      if (this.width == -1)
        throw new IllegalArgumentException("width cannot be null");
      if (this.height == -1)
        throw new IllegalArgumentException("height cannot be null");
//      if (this.bitmap == null)
//        throw new IllegalArgumentException("bitmap cannot be null");
      return new AImage(this);
    }

    public Builder x(int x) {
      this.x = x;
      return this;
    }

    public Builder y(int y) {
      this.y = y;
      return this;
    }

    public Builder width(int width) {
      this.width = width;
      return this;
    }

    public Builder height(int height) {
      this.height = height;
      return this;
    }

//    public Builder<T> bitmap(T bitmap) {
//      this.bitmap = bitmap;
//      return this;
//    }
  }

}
