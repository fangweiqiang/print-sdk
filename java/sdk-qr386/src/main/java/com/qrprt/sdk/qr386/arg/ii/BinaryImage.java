package com.qrprt.sdk.qr386.arg.ii;

public class BinaryImage extends IImage<byte[]> {

  private byte[] bytes;

  public static BinaryImage create(byte[] bytes) {
    return new BinaryImage(bytes);
  }

  private BinaryImage(byte[] bytes) {
    this.bytes = bytes;
  }

  @Override
  public byte[] image() {
    return this.bytes;
  }

}
