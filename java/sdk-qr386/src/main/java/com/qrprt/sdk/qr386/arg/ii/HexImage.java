package com.qrprt.sdk.qr386.arg.ii;

public class HexImage extends IImage<String> {

  private String hex;

  public static HexImage create(String hex) {
    return new HexImage(hex);
  }

  private HexImage(String hex) {
    this.hex = hex;
  }

  @Override
  public String image() {
    return this.hex;
  }

}
