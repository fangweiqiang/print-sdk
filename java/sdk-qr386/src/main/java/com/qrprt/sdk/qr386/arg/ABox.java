package com.qrprt.sdk.qr386.arg;

import com.qrprt.sdk.api.CArg;

public class ABox implements CArg {

  // width, top_left_x, top_left_y, bottom_right_x, bottom_right_y
  /**
   * 边框线条宽度
   */
  private final int width;
  /**
   * 矩形框左上角x坐标
   */
  private final int topleftx;
  /**
   * 矩形框左上角y坐标
   */
  private final int toplefty;
  /**
   * 矩形框右下角x坐标
   */
  private final int bottomleftx;
  /**
   * 矩形框右下角y坐标
   */
  private final int bottomlefty;

  private ABox(Builder builder) {
    this.width = builder.width;
    this.topleftx = builder.topleftx;
    this.toplefty = builder.toplefty;
    this.bottomleftx = builder.bottomleftx;
    this.bottomlefty = builder.bottomlefty;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int width() {
    return width;
  }

  public int topleftx() {
    return topleftx;
  }

  public int toplefty() {
    return toplefty;
  }

  public int bottomleftx() {
    return bottomleftx;
  }

  public int bottomlefty() {
    return bottomlefty;
  }

  public static class Builder {
    private int width;
    private int topleftx;
    private int toplefty;
    private int bottomleftx;
    private int bottomlefty;

    public Builder() {
      this.width = -1;
      this.topleftx = -1;
      this.toplefty = -1;
      this.bottomleftx = -1;
      this.bottomlefty = -1;
    }

    public ABox build() {
      if (this.width == -1)
        throw new IllegalArgumentException("width cannot be null");
      if (this.topleftx == -1)
        throw new IllegalArgumentException("top left x cannot be null");
      if (this.toplefty == -1)
        throw new IllegalArgumentException("top left y cannot be null");
      if (this.bottomlefty == -1)
        throw new IllegalArgumentException("bottom left y cannot be null");
      if (this.bottomleftx == -1)
        throw new IllegalArgumentException("bottom left x cannot be null");
      return new ABox(this);
    }

    public Builder width(int width) {
      this.width = width;
      return this;
    }

    public Builder topleftx(int topleftx) {
      this.topleftx = topleftx > 575 ? 575 : topleftx;
      return this;
    }

    public Builder toplefty(int toplefty) {
      this.toplefty = toplefty;
      return this;
    }

    public Builder bottomleftx(int bottomleftx) {
      this.bottomleftx = bottomleftx > 575 ? 575 : bottomleftx;
      return this;
    }

    public Builder bottomlefty(int bottomlefty) {
      this.bottomlefty = bottomlefty;
      return this;
    }
  }

}
