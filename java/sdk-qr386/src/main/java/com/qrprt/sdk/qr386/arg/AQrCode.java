package com.qrprt.sdk.qr386.arg;

import com.qrprt.sdk.api.CArg;

public class AQrCode implements CArg {

  // start_x, start_y, text, rotate, ver, lel
  /**
   * 二维码起始横坐标
   */
  private int x;
  /**
   * 二维码起始纵坐标
   */
  private int y;
  /**
   * 二维码内容
   */
  private String text;
  /**
   * 旋转
   */
  private int rotate;
  private int ver;
  private int lel;

  private AQrCode(Builder builder) {
    this.x = builder.x;
    this.y = builder.y;
    this.text = builder.text;
    this.rotate = builder.rotate;
    this.ver = builder.ver;
    this.lel = builder.lel;
  }


  public static Builder builder() {
    return new Builder();
  }

  public int x() {
    return x;
  }

  public int y() {
    return y;
  }

  public String text() {
    return text;
  }

  public int rotate() {
    return rotate;
  }

  public int ver() {
    return ver;
  }

  public int lel() {
    return lel;
  }

  public static class Builder {
    private int x;
    private int y;
    private String text;
    private int rotate;
    private int ver;
    private int lel;

    public Builder() {
      this.x = -1;
      this.y = -1;
      this.rotate = -1;
      this.ver = -1;
      this.lel = -1;
    }

    public AQrCode build() {
      if (this.x == -1)
        throw new IllegalArgumentException("x cannot be null");
      if (this.y == -1)
        throw new IllegalArgumentException("y cannot be null");
      if (this.text == null)
        throw new IllegalArgumentException("text cannot be null");
      return new AQrCode(this);
    }

    public Builder x(int x) {
      this.x = x;
      return this;
    }

    public Builder y(int y) {
      this.y = y;
      return this;
    }

    public Builder text(String text) {
      this.text = text;
      return this;
    }

    public Builder rotate(int rotate) {
      this.rotate = rotate;
      return this;
    }

    public Builder ver(int ver) {
      this.ver = ver;
      return this;
    }

    public Builder lel(int lel) {
      this.lel = lel;
      return this;
    }
  }

}
