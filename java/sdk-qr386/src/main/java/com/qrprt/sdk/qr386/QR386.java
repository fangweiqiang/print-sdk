package com.qrprt.sdk.qr386;

import com.qrprt.sdk.api.*;
import com.qrprt.sdk.qr386.arg.*;
import com.qrprt.sdk.qr386.arg.ii.BinaryImage;
import com.qrprt.sdk.qr386.arg.ii.HexImage;
import com.qrprt.sdk.toolkit.$TextKit;

public class QR386 extends QRSDK<QR386> {


  private static final int FAMLIY_NORMAL = 24;
  private static final int FAMLIY_DEFAULT = 55;
  private static final int[] INVERSE_ARR = {8, 12, 16, 24, 32, 48, 64};

  private static class Holder {
    private static final QR386 INSTANCE = new QR386();
  }

  public static QR386 use() {
    return new QR386();
  }

//  public static QR386 safe() {
//    return new QR386();
//  }
//
//  public static QR386 unsafe() {
//    return Holder.INSTANCE;
//  }

  private CommandBuilder builder;

  public QR386() {
    this.builder = CommandBuilder.create();
  }

  public QR386 init(AInit arg) {
    this.builder.clear();
    this.builder.push($TextKit.format("! 0 200 200 {0} 1", arg.height()))
      .push($TextKit.format("PW {0}", arg.weight()));
    if (!arg.sense())
      return this;
    return this.gapsense();
  }

  /**
   * 走出整页纸
   *
   * @return QR386
   */
  public QR386 gapsense() {
    this.builder.push("GAP-SENSE")
      .push("FORM");
    return this;
  }

  public QR386 print() {
    return this.print(Boolean.TRUE);
  }

  /**
   * @param horizontal 横向打印
   * @return QR386
   */
  public QR386 print(boolean horizontal) {
    this.builder.push(horizontal ? "PRINT" : "POPRINT");
    return this;
  }

  public QR386 box(ABox arg) {
    String _cmd = $TextKit.format("BOX {0} {1} {2} {3} {4}",
      arg.topleftx(), arg.toplefty(), arg.bottomleftx(), arg.bottomlefty(), arg.width());
    this.builder.push(_cmd);
    return this;
  }


  public QR386 line(ALine arg) {
    String head = arg.fullline() ? "LINE" : "LPLINE";
    String _cmd = $TextKit.format("{0} {1} {2} {3} {4} {5}",
      head, arg.startx(), arg.starty(), arg.endx(), arg.endy(), arg.width());
    this.builder.push(_cmd);
    return this;
  }

  public QR386 text(AText arg) {
    String _underline = arg.underline() ? "UNDERLINE ON" : "UNDERLINE OFF";

    this.builder.push(_underline);

    if (arg.bold()) {
      String _bold = $TextKit.format("SETBOLD {0}", arg.bold());
      this.builder.push(_bold);
    }

    int size = 0;
    int ex = 1;
    int ey = 1;
    int family;
    switch (arg.fontsize()) {
      case 1:
        family = 55;
        break;
      case 2:
        family = FAMLIY_NORMAL;
        break;
      case 3:
        family = 4;
        break;
      case 4:
        family = FAMLIY_NORMAL;
        ex = 2;
        ey = 2;
        break;
      case 5:
        family = 4;
        ex = 2;
        ey = 2;
        break;
      case 6:
        family = FAMLIY_NORMAL;
        ex = 3;
        ey = 3;
        break;
      case 7:
        family = 4;
        ex = 3;
        ey = 3;
        break;
      case 8:
        family = 3;
        size = 1;
        ex = 1;
        ey = 1;
        break;
      case 9:
        family = 4;
        size = 3;
        ex = 1;
        ey = 1;
        break;
      default:
        family = FAMLIY_DEFAULT;
    }

    String _mag = $TextKit.format("SETMAG {0} {1}", ex, ey);
    this.builder.push(_mag);

    String _text;
    switch (arg.rotate()) {
      case 1:
        _text = "TEXT90";
        break;
      case 2:
        _text = "TEXT180";
        break;
      case 3:
        _text = "TEXT270";
        break;
      default:
        _text = "TEXT";
    }

    _text = $TextKit.format("{0} {1} {2} {3} {4} {5}", _text, family, size, arg.x(), arg.y(), arg.text());
    this.builder.push(_text);

    if (arg.reverse()) {
      String _inverse;
      int textLength = 0;

      try {
        textLength = arg.text().getBytes(QRConst.CHARSET_GBK).length;
      } catch (Exception var16) {
        var16.printStackTrace();
      }
      int fse = arg.fontsize() - 1;
      if (fse >= 0 && fse < INVERSE_ARR.length) {
        int iea = INVERSE_ARR[fse];
        _inverse = $TextKit.format("INVERSE-LINE {0} {1} {2} {3} {4}",
          arg.x(), arg.y(), (arg.x() + textLength * iea), arg.y(), iea * 2);
        this.builder.push(_inverse);
      }
    }

    return this;
  }

  public QR386 barcode(ABarcode arg) {
    String head, bartype;
    if (arg.rotate() != 0 && arg.rotate() != 2) {
      head = arg.rotate() != 1 && arg.rotate() != 3 ? "B" : "VB";
    } else {
      head = "B";
    }

    switch (arg.type()) {
      case 0:
        bartype = "39";
        break;
      case 1:
        bartype = "128";
        break;
      case 2:
        bartype = "93";
        break;
      case 3:
        bartype = "CODABAR";
        break;
      case 4:
        bartype = "EAN8";
        break;
      case 5:
        bartype = "EAN13";
        break;
      case 6:
        bartype = "UPCA";
        break;
      case 7:
        bartype = "UPCE";
        break;
      case 8:
        bartype = "I2OF5";
        break;
      default:
        bartype = "128";
    }
    String _cmd = $TextKit.format("{0} {1} {2} {3} {4} {5} {6} {7}",
      head, bartype, arg.width() - 1, 2, arg.height(), arg.x(), arg.y(), arg.text());
    this.builder.push(_cmd);
    return this;
  }


  public QR386 qrcode(AQrCode arg) {
    String head, bartype;
    if (arg.rotate() != 0 && arg.rotate() != 2) {
      head = arg.rotate() != 1 && arg.rotate() != 3 ? "B" : "VB";
    } else {
      head = "B";
    }

    int ver = arg.ver() > 6 ? 6 : 2;

    char levelText;
    switch (arg.lel()) {
      case 0:
        levelText = 'L';
        break;
      case 1:
        levelText = 'M';
        break;
      case 2:
        levelText = 'Q';
        break;
      case 3:
        levelText = 'H';
        break;
      default:
        levelText = 'M';
        break;
    }

    String _cmd = $TextKit.format("{0} QR {1} {2} M 2 U {3} {4}A, {5}",
      head, arg.x(), arg.y(), ver, levelText, arg.text());
    this.builder.push(_cmd)
      .push("ENDQR");
    return this;
  }

  public QR386 image(AImage arg, HexImage bitmap) {
    String _cmd = $TextKit.format("EG {0} {1} {2} {3} {4}",
      arg.x(), arg.y(), arg.width(), arg.height(), bitmap.image());
    this.builder.push(_cmd);
    return this;
  }

  public QR386 image(AImage arg, BinaryImage bitmap) {
    String _cmd = $TextKit.format("EG {0} {1} {2} {3} ",
      arg.x(), arg.y(), arg.width(), arg.height());
    this.builder.push(_cmd, Boolean.FALSE);
    this.builder.push(bitmap.image());
    this.builder.newline();
    return this;
  }

  @Override
  public QR386 raw(ARaw raw) {
    switch (raw.mode()) {
      case TEXT:
        this.builder.push(raw.string(), raw.charset(), raw.newline());
        break;
      case BINARY:
        this.builder.push(raw.binary(), raw.newline());
        break;
      default:
        return this;
    }
    return this;
  }

  @Override
  public QR386 variable(String name, Object value) {
    this.builder.variable(name, value);
    return this;
  }

  @Override
  public Command command() {
    return this.builder.build();
  }

  @Override
  public void clear() {
    this.builder.clear();
  }
}
