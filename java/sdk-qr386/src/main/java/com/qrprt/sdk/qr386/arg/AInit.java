package com.qrprt.sdk.qr386.arg;

import com.qrprt.sdk.api.CArg;

public class AInit implements CArg {

  private final int weight;
  private final int height;
  private final boolean sense;

  private AInit(Builder builder) {
    this.weight = builder.weight;
    this.height = builder.height;
    this.sense = builder.sense;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int weight() {
    return weight;
  }

  public int height() {
    return height;
  }

  public boolean sense() {
    return sense;
  }

  public static class Builder {

    private int weight;
    private int height;
    private boolean sense;

    public Builder() {
      this.weight = 592;
      this.height = 475;
      this.sense = Boolean.FALSE;
    }

    public AInit build() {
      return new AInit(this);
    }

    public Builder weight(int weight) {
      this.weight = weight;
      return this;
    }

    public Builder height(int height) {
      this.height = height;
      return this;
    }

    public Builder sense(boolean sense) {
      this.sense = sense;
      return this;
    }
  }

}
