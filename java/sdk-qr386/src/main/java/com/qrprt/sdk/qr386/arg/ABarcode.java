package com.qrprt.sdk.qr386.arg;

import com.qrprt.sdk.api.CArg;

public class ABarcode implements CArg {

  /**
   * 一维码起始横坐标
   */
  private int x;
  /**
   * 一维码起始纵坐标
   */
  private int y;
  /**
   * 文字内容
   */
  private String text;
  /**
   * 条码类型
   * 0: 39;        1: 128;
   * 2: 93;        3: CODABAR;
   * 4: EAN8;      5: EAN13;
   * 6: UPCA;      7: UPCE;
   * 8: I2OF5      default: 128
   */
  private int type;
  private int rotate;
  /**
   * 条码宽度
   */
  private int width;
  /**
   * 条码高度
   */
  private int height;

  private ABarcode(Builder builder) {
    this.x = builder.x;
    this.y = builder.y;
    this.text = builder.text;
    this.type = builder.type;
    this.rotate = builder.rotate;
    this.width = builder.width;
    this.height = builder.height;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int x() {
    return x;
  }

  public int y() {
    return y;
  }

  public String text() {
    return text;
  }

  public int type() {
    return type;
  }

  public int rotate() {
    return rotate;
  }

  public int width() {
    return width;
  }

  public int height() {
    return height;
  }

  public static class Builder {

    private int x;
    private int y;
    private String text;
    private int type;
    private int rotate;
    private int width;
    private int height;


    public Builder() {
      this.x = -1;
      this.y = -1;
      this.type = -1;
      this.rotate = -1;
      this.width = -1;
      this.height = -1;
    }

    public ABarcode build() {
      if (this.x == -1)
        throw new IllegalArgumentException("x cannot be null");
      if (this.y == -1)
        throw new IllegalArgumentException("y cannot be null");
      if (this.width == -1)
        throw new IllegalArgumentException("width cannot be null");
      if (this.height == -1)
        throw new IllegalArgumentException("height cannot be null");
      return new ABarcode(this);
    }

    public Builder x(int x) {
      this.x = x;
      return this;
    }

    public Builder y(int y) {
      this.y = y;
      return this;
    }

    public Builder text(String text) {
      this.text = text;
      return this;
    }

    public Builder type(int type) {
      this.type = type;
      return this;
    }

    public Builder rotate(int rotate) {
      this.rotate = rotate;
      return this;
    }

    public Builder width(int width) {
      this.width = width;
      return this;
    }

    public Builder height(int height) {
      this.height = height;
      return this;
    }
  }

}
