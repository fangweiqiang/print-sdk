package com.qrprt.sdk.qr386.arg;

import com.qrprt.sdk.api.CArg;

public class ALine implements CArg {

  /**
   * 线条宽度
   */
  private final int width;
  /**
   * 线条起始点x坐标
   */
  private final int startx;
  /**
   * 线条起始点y坐标
   */
  private final int starty;
  /**
   * 线条结束点x坐标
   */
  private final int endx;
  /**
   * 线条结束点y坐标
   */
  private final int endy;
  /**
   * 是否实线
   */
  private final boolean fullline;

  private ALine(Builder builder) {
    this.width = builder.width;
    this.startx = builder.startx;
    this.starty = builder.starty;
    this.endx = builder.endx;
    this.endy = builder.endy;
    this.fullline = builder.fullline;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int width() {
    return width;
  }

  public int startx() {
    return startx;
  }

  public int starty() {
    return starty;
  }

  public int endx() {
    return endx;
  }

  public int endy() {
    return endy;
  }

  public boolean fullline() {
    return fullline;
  }

  public static class Builder {

    private int width;
    private int startx;
    private int starty;
    private int endx;
    private int endy;
    private boolean fullline;

    public Builder() {
      this.width = -1;
      this.startx = -1;
      this.starty = -1;
      this.endx = -1;
      this.endy = 1;
      this.fullline = Boolean.TRUE;
    }

    public ALine build() {
      if (this.width == -1)
        throw new IllegalArgumentException("width cannot be null");
      if (this.startx == -1)
        throw new IllegalArgumentException("start x cannot be null");
      if (this.starty == -1)
        throw new IllegalArgumentException("start y cannot be null");
      if (this.endx == -1)
        throw new IllegalArgumentException("end x cannot be null");
      if (this.endy == -1)
        throw new IllegalArgumentException("end y cannot be null");
      return new ALine(this);
    }

    public Builder width(int width) {
      this.width = width;
      return this;
    }

    public Builder startx(int startx) {
      this.startx = startx > 575 ? 575 : startx;
      return this;
    }

    public Builder starty(int starty) {
      this.starty = starty;
      return this;
    }

    public Builder endx(int endx) {
      this.endx = endx > 575 ? 575 : endx;
      return this;
    }

    public Builder endy(int endy) {
      this.endy = endy;
      return this;
    }

    public Builder fullline() {
      return this.fullline(Boolean.TRUE);
    }

    public Builder fullline(boolean fullline) {
      this.fullline = fullline;
      return this;
    }
  }

}
