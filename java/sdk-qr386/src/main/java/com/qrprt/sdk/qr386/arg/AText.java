package com.qrprt.sdk.qr386.arg;

import com.qrprt.sdk.api.CArg;

public class AText implements CArg {

  // text_x, text_y, text, fontSize, rotation, bold, underline

  /**
   * 起始横坐标
   */
  private final int x;
  /**
   * 起始纵坐标
   */
  private final int y;
  /**
   * 文字内容
   */
  private final String text;
  /**
   * 文字大小
   */
  private final int fontsize;
  /**
   * 旋转
   */
  private final int rotate;
  /**
   * 加粗
   */
  private final boolean bold;

  private final boolean reverse;
  /**
   * 下划线
   */
  private final boolean underline;

  private AText(Builder builder) {
    this.x = builder.x;
    this.y = builder.y;
    this.text = builder.text;
    this.fontsize = builder.fontsize;
    this.rotate = builder.rotate;
    this.bold = builder.bold;
    this.reverse = builder.reverse;
    this.underline = builder.underline;
  }

  public static Builder builder() {
    return new Builder();
  }

  public int x() {
    return x;
  }

  public int y() {
    return y;
  }

  public String text() {
    return text;
  }

  public int fontsize() {
    return fontsize;
  }

  public int rotate() {
    return rotate;
  }

  public boolean bold() {
    return bold;
  }

  public boolean reverse() {
    return reverse;
  }

  public boolean underline() {
    return underline;
  }

  public static class Builder {

    private int x;
    private int y;
    private String text;
    private int fontsize;
    private int rotate;
    private boolean bold;
    private boolean reverse;
    private boolean underline;


    public Builder() {
      this.x = -1;
      this.y = -1;
      this.fontsize = -1;
      this.rotate = -1;
      this.bold = Boolean.FALSE;
      this.reverse = Boolean.FALSE;
      this.underline = Boolean.FALSE;
    }

    public AText build() {
      if (this.x == -1)
        throw new IllegalArgumentException("x cannot be null");
      if (this.y == -1)
        throw new IllegalArgumentException("y cannot be null");
      if (this.text == null)
        throw new IllegalArgumentException("text cannot be null");
      return new AText(this);
    }

    public Builder x(int x) {
      this.x = x;
      return this;
    }

    public Builder y(int y) {
      this.y = y;
      return this;
    }

    public Builder text(String text) {
      this.text = text;
      return this;
    }

    public Builder fontsize(int fontsize) {
      this.fontsize = fontsize;
      return this;
    }

    public Builder rotate(int rotate) {
      this.rotate = rotate;
      return this;
    }

    public Builder bold() {
      return this.bold(Boolean.TRUE);
    }

    public Builder bold(boolean bold) {
      this.bold = bold;
      return this;
    }

    public Builder reverse() {
      return this.reverse(Boolean.TRUE);
    }

    public Builder reverse(boolean reverse) {
      this.reverse = reverse;
      return this;
    }

    public Builder underline() {
      return this.underline(Boolean.TRUE);
    }

    public Builder underline(boolean underline) {
      this.underline = underline;
      return this;
    }
  }

}
