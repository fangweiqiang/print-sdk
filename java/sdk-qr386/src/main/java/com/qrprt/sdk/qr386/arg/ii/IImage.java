package com.qrprt.sdk.qr386.arg.ii;

public abstract class IImage<T> {

  public static BinaryImage binary(byte[] bytes) {
    return BinaryImage.create(bytes);
  }

  public static HexImage hex(String hex) {
    return HexImage.create(hex);
  }

  public abstract T image();

}
