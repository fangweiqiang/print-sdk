package com.qrprt.sdk.support.graph2d;

import com.qrprt.sdk.support.graph2d.font.JavaAwtNamedFont;
import com.qrprt.sdk.support.graph2d.types.text2img.Text2ImageParameter;
import com.qrprt.sdk.support.graph2d.types.text2img.Text2ImageResult;
import io.enoa.toolkit.file.FileKit;

import java.awt.*;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.List;

public class Graph2dTest {


  public static void main(String[] arg) {
    Graph2dTest gt = new Graph2dTest();
    gt.testGraph2dSupport();
  }

  private void testGraph2dSupport() {
    try {
      Path savePath = FileKit.mkdirs("d:/tmp/graph2d");
      String text = "下麼藝她，常風故土夜依足評力足良不作不指In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.弟，身數人正金前！個富不是隊天照吸施只，第演細升子來學什由的完已";

      Text2ImageResult singleImage = IGraph2dSupport.java()
        .textToImage(Text2ImageParameter.<Font>builder()
          .width(390)
          .height(140)
          .font(new JavaAwtNamedFont("宋体", 16))
          .text(text)
          .fontSize(16)
          .imageFormat("jpeg")
          .enableAutoNewLine(true)
          .enableOneImagePerLine(false)
          .build());
      List<byte[]> singleImages = singleImage.images();
      byte[] singleBinary = singleImages.get(0);
      FileKit.write(savePath.resolve("single.jpg"), ByteBuffer.wrap(singleBinary));


      Text2ImageResult multiImage = IGraph2dSupport.java()
        .textToImage(Text2ImageParameter.<Font>builder()
          .width(390)
          .height(140)
          .font(new JavaAwtNamedFont("黑体", 16))
          .text(text)
          .fontSize(16)
          .imageFormat("jpeg")
          .enableAutoNewLine(true)
          .enableOneImagePerLine(true)
          .paintX(3)
          .build());
      List<byte[]> multiImages = multiImage.images();
      int i = 0;
      for (byte[] multiBinary : multiImages) {
        i += 1;
        FileKit.write(savePath.resolve("multi-" + i + ".jpg"), ByteBuffer.wrap(multiBinary));
      }


    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
