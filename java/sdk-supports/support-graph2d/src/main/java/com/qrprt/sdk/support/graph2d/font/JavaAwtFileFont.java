package com.qrprt.sdk.support.graph2d.font;

import com.qrprt.sdk.support.graph2d.IFont;

import java.awt.*;
import java.io.File;

public class JavaAwtFileFont implements IFont<Font> {

  private final File file;
  private final int size;

  public JavaAwtFileFont(File file, int size) {
    this.file = file;
    this.size = size;
  }

  @Override
  public Font font() throws Exception {
    return Font.createFont(Font.TRUETYPE_FONT, this.file).deriveFont(this.size);
  }
}
