package com.qrprt.sdk.support.graph2d.font;

import android.graphics.Typeface;
import com.qrprt.sdk.support.graph2d.IFont;

public class AndroidNamedFont implements IFont<Typeface> {

  private final String name;

  public AndroidNamedFont(String name) {
    this.name = name;
  }

  @Override
  public Typeface font() throws Exception {
    return Typeface.create(this.name, Typeface.NORMAL);
  }
}
