package com.qrprt.sdk.support.graph2d.android;

import android.graphics.*;
import android.text.TextPaint;
import com.qrprt.sdk.support.graph2d.IGraph2dSupport;
import com.qrprt.sdk.support.graph2d.types.text2img.Text2ImageParameter;
import com.qrprt.sdk.support.graph2d.types.text2img.Text2ImageResult;

import java.util.ArrayList;
import java.util.List;

public class AndroidGraph2dSupportProvider implements IGraph2dSupport<Typeface> {

  private static final AndroidGraph2dSupportProvider INSTANCE =  new AndroidGraph2dSupportProvider();

  public static AndroidGraph2dSupportProvider instance() {
    return INSTANCE;
  }

  @Override
  public Text2ImageResult textToImage(Text2ImageParameter<Typeface> parameter) throws Exception {
    if (parameter.enableAutoNewLine() && parameter.enableOneImagePerLine()) {
      return this.multiImage(parameter);
    }
    return this.singleImage(parameter);
  }

  private Bitmap.CompressFormat convertFormat(String format) {
    format = format.toLowerCase();
    switch (format) {
      case "png":
        return Bitmap.CompressFormat.PNG;
      case "webp":
        return Bitmap.CompressFormat.WEBP;
      case "jpg":
      case "jpeg":
      default:
        return Bitmap.CompressFormat.JPEG;
    }
  }

  private Text2ImageResult multiImage(Text2ImageParameter<Typeface> parameter) throws Exception {
    Bitmap bitmapBlackhole = Bitmap.createBitmap(parameter.width(), parameter.height(), Bitmap.Config.RGB_565);
    Canvas canvasBlackhole = new Canvas(bitmapBlackhole);

    Typeface typeface = parameter.font().font();
    Bitmap.CompressFormat compressFormat = this.convertFormat(parameter.imageFormat());

    TextPaint mTextPaint = new TextPaint();
    mTextPaint.setAntiAlias(true);
    mTextPaint.setTextSize(parameter.fontSize());
    mTextPaint.setTypeface(typeface);
    mTextPaint.setColor(Color.BLACK);

    String text = parameter.text();
    Rect bounds = new Rect();
    mTextPaint.getTextBounds(text, 0, text.length(), bounds);
    int fontHeight = bounds.height();
    int textPixelWidth = bounds.width();


    if (parameter.width() > textPixelWidth) {
      canvasBlackhole.drawText(parameter.text(), parameter.paintX(), fontHeight, mTextPaint);
      return Text2ImageResult.with($BitmapKit.binary(bitmapBlackhole, compressFormat));
    }

    int lineSize = (int) Math.ceil(textPixelWidth * 1.0 / parameter.width());

    StringBuilder builder = new StringBuilder();
    int lineNumber = 0;
    int lineStart = 0;
    String[] lineText = new String[lineSize];
    int textLength = text.length();

    for (int positionText = 0; positionText < textLength; positionText++) {
      char ch = parameter.text().charAt(positionText);
      builder.append(ch);
      Rect _bounds = new Rect();
      mTextPaint.getTextBounds(builder.toString(), 0, builder.length(), _bounds);
      int tempTextPixelWidth = _bounds.width();
      if (tempTextPixelWidth > parameter.width()) {
        lineText[lineNumber++] = parameter.text().substring(lineStart, positionText);
        lineStart = positionText;
        builder.delete(0, builder.length());
        builder.append(ch);
      }
      if (positionText == textLength - 1) {// end line
        lineText[lineNumber] = parameter.text().substring(lineStart);
      }
    }


    List<Bitmap> images = new ArrayList<>(lineText.length);
    for (String s : lineText) {
      Bitmap bitmap = Bitmap.createBitmap(
        parameter.width(),
        fontHeight + parameter.paintY(),
        Bitmap.Config.RGB_565
      );
      Canvas canvas = new Canvas(bitmap);
      canvas.drawText(
        s,
        parameter.paintX(),
        (fontHeight),
        mTextPaint
      );
      images.add(bitmap);
    }
    List<byte[]> binarys = $BitmapKit.binarys(images, compressFormat);
    return Text2ImageResult.with(binarys);
  }

  private Text2ImageResult singleImage(Text2ImageParameter<Typeface> parameter) throws Exception {
    Bitmap bitmap = Bitmap.createBitmap(parameter.width(), parameter.height(), Bitmap.Config.RGB_565);
    Canvas canvas = new Canvas(bitmap);

    Typeface typeface = parameter.font().font();
    Bitmap.CompressFormat compressFormat = this.convertFormat(parameter.imageFormat());

    TextPaint mTextPaint = new TextPaint();
    mTextPaint.setAntiAlias(true);
    mTextPaint.setTextSize(parameter.fontSize());
    mTextPaint.setTypeface(typeface);
    mTextPaint.setColor(Color.BLACK);

    String text = parameter.text();
    Rect bounds = new Rect();
    mTextPaint.getTextBounds(text, 0, text.length(), bounds);
    int fontHeight = bounds.height();
    int textPixelWidth = bounds.width();

    if (!parameter.enableAutoNewLine()) {
      canvas.drawText(
        text,
        (float) parameter.paintX(),
        (fontHeight + parameter.paintY()),
        mTextPaint
      );
      return Text2ImageResult.with($BitmapKit.binary(bitmap, compressFormat));
    }

    if (parameter.width() > textPixelWidth) {
      canvas.drawText(text, (float) parameter.paintX(), fontHeight, mTextPaint);
      return Text2ImageResult.with($BitmapKit.binary(bitmap, compressFormat));
    }

    int lineSize = (int) Math.ceil(textPixelWidth * 1.0 / parameter.width());

    StringBuilder builder = new StringBuilder();
    int lineNumber = 0;
    int lineStart = 0;
    String[] lineText = new String[lineSize];
    int textLength = text.length();

    for (int positionText = 0; positionText < textLength; positionText++) {
      char ch = parameter.text().charAt(positionText);
      builder.append(ch);
      Rect _bounds = new Rect();
      mTextPaint.getTextBounds(builder.toString(), 0, builder.length(), _bounds);
      int tempTextPixelWidth = _bounds.width();
      if (tempTextPixelWidth > parameter.width()) {
        lineText[lineNumber++] = parameter.text().substring(lineStart, positionText);
        lineStart = positionText;
        builder.delete(0, builder.length());
        builder.append(ch);
      }
      if (positionText == textLength - 1) {// end line
        lineText[lineNumber] = parameter.text().substring(lineStart);
      }
    }

    for (int i = 0; i < lineText.length; i++) {
      canvas.drawText(
        lineText[i],
        parameter.paintX(),
        (fontHeight + parameter.paintY()) * (i + 1),
        mTextPaint
      );
    }
    return Text2ImageResult.with($BitmapKit.binary(bitmap, compressFormat));
  }
}
