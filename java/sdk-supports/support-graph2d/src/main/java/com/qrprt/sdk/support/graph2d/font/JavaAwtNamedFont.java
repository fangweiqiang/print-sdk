package com.qrprt.sdk.support.graph2d.font;

import com.qrprt.sdk.support.graph2d.IFont;

import java.awt.*;

public class JavaAwtNamedFont implements IFont<Font> {

  private final String name;
  private final int size;

  public JavaAwtNamedFont(String name, int size) {
    this.name = name;
    this.size = size;
  }

  @Override
  public Font font() {
    return new Font(this.name, Font.PLAIN, this.size);
  }
}
