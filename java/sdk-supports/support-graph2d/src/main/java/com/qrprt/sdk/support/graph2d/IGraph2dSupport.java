package com.qrprt.sdk.support.graph2d;

import android.graphics.Typeface;
import com.qrprt.sdk.support.graph2d.android.AndroidGraph2dSupportProvider;
import com.qrprt.sdk.support.graph2d.java.JavaGraph2dSupportProvider;
import com.qrprt.sdk.support.graph2d.types.text2img.Text2ImageParameter;
import com.qrprt.sdk.support.graph2d.types.text2img.Text2ImageResult;

import java.awt.*;

public interface IGraph2dSupport<T> {

  static IGraph2dSupport<Font> java() {
    return JavaGraph2dSupportProvider.instance();
  }

  static IGraph2dSupport<Typeface> android() {
    return AndroidGraph2dSupportProvider.instance();
  }

  /**
   * Convert text to image
   *
   * @param parameter parameter
   * @return Text2ImageResult
   * @throws Exception Any exception
   */
  Text2ImageResult textToImage(Text2ImageParameter<T> parameter) throws Exception;

}
