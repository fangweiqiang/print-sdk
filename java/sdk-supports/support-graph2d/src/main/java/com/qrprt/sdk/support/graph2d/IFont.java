package com.qrprt.sdk.support.graph2d;

public interface IFont<R> {

  R font() throws Exception;

}
