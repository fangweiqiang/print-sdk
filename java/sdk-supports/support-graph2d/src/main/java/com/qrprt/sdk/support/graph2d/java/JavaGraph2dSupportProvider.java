package com.qrprt.sdk.support.graph2d.java;

import com.qrprt.sdk.support.graph2d.IGraph2dSupport;
import com.qrprt.sdk.support.graph2d.types.text2img.Text2ImageParameter;
import com.qrprt.sdk.support.graph2d.types.text2img.Text2ImageResult;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class JavaGraph2dSupportProvider implements IGraph2dSupport<Font> {

  private static final JavaGraph2dSupportProvider INSTANCE = new JavaGraph2dSupportProvider();

  public static JavaGraph2dSupportProvider instance() {
    return INSTANCE;
  }

  @Override
  public Text2ImageResult textToImage(Text2ImageParameter<Font> parameter) throws Exception {
    if (parameter.enableAutoNewLine() && parameter.enableOneImagePerLine()) {
      return this.multiImage(parameter);
    }
    return this.singleImage(parameter);
  }

  private Text2ImageResult multiImage(Text2ImageParameter<Font> parameter) throws Exception {
    BufferedImage blackhole = new BufferedImage(
      parameter.width(),
      parameter.height() == 0 ? 500 : parameter.height(),
      BufferedImage.TYPE_INT_RGB
    );
    Font font = parameter.font().font();
    Graphics graphcs = blackhole.getGraphics();
    graphcs.setColor(Color.white);
    graphcs.fillRect(0, 0, parameter.width(), parameter.height());
    graphcs.setFont(font);
    Color fontColor = new Color(0, 0, 0);
    graphcs.setColor(fontColor);

    int fontHeight = (int) font.getSize2D();
    FontMetrics metrics = graphcs.getFontMetrics();
    int textPixelWidth = metrics.stringWidth(parameter.text());
    int lineSize = (int) Math.ceil(textPixelWidth * 1.0 / parameter.width());

    if (parameter.width() > textPixelWidth) {
      graphcs.drawString(parameter.text(), parameter.paintX(), fontHeight);
      return Text2ImageResult.with(blackhole, parameter.imageFormat());
    }


    // Save line text
    StringBuilder builder = new StringBuilder();
    int lineNumber = 0;
    int lineStart = 0;
    String[] lineText = new String[lineSize];
    int textLength = parameter.text().length();
    for (int positionText = 0; positionText < textLength; positionText++) {
      char ch = parameter.text().charAt(positionText);
      builder.append(ch);
      Rectangle2D bounds2 = metrics.getStringBounds(builder.toString(), null);
      int tempTextPixellWidth = (int) bounds2.getWidth();
      if (tempTextPixellWidth > parameter.width()) {
        lineText[lineNumber++] = parameter.text().substring(lineStart, positionText);
        lineStart = positionText;
        builder.delete(0, builder.length());
        builder.append(ch);
      }
      if (positionText == textLength - 1) {// end line
        lineText[lineNumber] = parameter.text().substring(lineStart);
      }
    }

    List<BufferedImage> images = new ArrayList<>(lineText.length);
    for (String s : lineText) {
      BufferedImage multiImage = new BufferedImage(
        parameter.width(),
        fontHeight + parameter.paintY(),
        BufferedImage.TYPE_INT_RGB
      );

      Graphics multiGraphcs = multiImage.getGraphics();
      multiGraphcs.setColor(Color.white);
      multiGraphcs.fillRect(0, 0, parameter.width(), parameter.height());
      multiGraphcs.setFont(font);
      multiGraphcs.setColor(fontColor);

      multiGraphcs.drawString(s, parameter.paintX(), (fontHeight));
      images.add(multiImage);
    }
    return Text2ImageResult.with(images, parameter.imageFormat());
  }

  private Text2ImageResult singleImage(Text2ImageParameter<Font> parameter) throws Exception {
    BufferedImage image = new BufferedImage(
      parameter.width(),
      parameter.height(),
      BufferedImage.TYPE_INT_RGB
    );
    Font font = parameter.font().font();
    Graphics graphcs = image.getGraphics();
    graphcs.setColor(Color.white);
    graphcs.fillRect(0, 0, parameter.width(), parameter.height());
    graphcs.setFont(font);
    Color fontColor = new Color(0, 0, 0);
    graphcs.setColor(fontColor);

    int fontHeight = (int) font.getSize2D();

    if (!parameter.enableAutoNewLine()) {
      graphcs.drawString(parameter.text(), parameter.paintX(), fontHeight + parameter.paintY());
      return Text2ImageResult.with(image, parameter.imageFormat());
    }

    // get this font metrics
    FontMetrics metrics = graphcs.getFontMetrics();
    // get current text all width
    int textPixelWidth = metrics.stringWidth(parameter.text());
    // calculate each line text size
    int lineSize = (int) Math.ceil(textPixelWidth * 1.0 / parameter.width());


    // If the image width > All text pixel width
    if (parameter.width() > textPixelWidth) {
      graphcs.drawString(parameter.text(), parameter.paintX(), fontHeight);
      return Text2ImageResult.with(image, parameter.imageFormat());
    }


    // Save line text
    StringBuilder builder = new StringBuilder();
    int lineNumber = 0;
    int lineStart = 0;
    String[] lineText = new String[lineSize];
    int textLength = parameter.text().length();
    for (int positionText = 0; positionText < textLength; positionText++) {
      char ch = parameter.text().charAt(positionText);
      builder.append(ch);
      Rectangle2D bounds2 = metrics.getStringBounds(builder.toString(), null);
      int tempTextPixelWidth = (int) bounds2.getWidth();
      if (tempTextPixelWidth > parameter.width()) {
        lineText[lineNumber++] = parameter.text().substring(lineStart, positionText);
        lineStart = positionText;
        builder.delete(0, builder.length());
        builder.append(ch);
      }
      if (positionText == textLength - 1) {// end line
        lineText[lineNumber] = parameter.text().substring(lineStart);
      }
    }
    for (int i = 0; i < lineText.length; i++) {
      graphcs.drawString(lineText[i], parameter.paintX(), (fontHeight + parameter.paintY()) * (i + 1));
    }
    return Text2ImageResult.with(image, parameter.imageFormat());
  }

}
