package com.qrprt.sdk.support.graph2d.types.text2img;

import com.qrprt.sdk.support.graph2d.IFont;

import java.io.Serializable;

public class Text2ImageParameter<T> implements Serializable {

  // Draw text content
  private final String text;
  // Text content
  private final IFont<T> font;
  // The image width
  private final int width;
  // The image height;
  private final int height;
  // The font size, default is 14
  private final int fontSize;
  // Is enable auto new line when the text is long of image width, default is false
  private final boolean enableAutoNewLine;
  // Is enable one image per line, default is false, if you want enable this,
  // The `enableAutoNewline` is must set true.
  // If this is true, the `height` field will be invalid.
  private final boolean enableOneImagePerLine;
  // image format, default is jpeg
  private final String imageFormat;
  // Paint X point, default 5
  private final int paintX;
  // Paint Y point, default 5
  private final int paintY;

  public Text2ImageParameter(Builder<T> builder) {
    this.text = builder.text;
    this.font = builder.font;
    this.width = builder.width;
    this.height = builder.height;
    this.fontSize = builder.fontSize;
    this.enableAutoNewLine = builder.enableAutoNewLine;
    this.enableOneImagePerLine = builder.enableOneImagePerLine;
    this.imageFormat = builder.imageFormat;
    this.paintX = builder.paintX;
    this.paintY = builder.paintY;
  }

  public static <F> Builder<F> builder() {
    return new Builder<F>();
  }

  public String text() {
    return this.text;
  }

  public IFont<T> font() {
    return this.font;
  }

  public int width() {
    return this.width;
  }

  public int height() {
    return this.height;
  }

  public int fontSize() {
    return this.fontSize;
  }

  public boolean enableAutoNewLine() {
    return this.enableAutoNewLine;
  }

  public boolean enableOneImagePerLine() {
    return this.enableOneImagePerLine;
  }

  public String imageFormat() {
    return this.imageFormat;
  }

  public int paintX() {
    return this.paintX;
  }

  public int paintY() {
    return this.paintY;
  }

  public static class Builder<T> {

    private String text;
    private IFont<T> font;
    private int width;
    private int height;
    private int fontSize;
    private boolean enableAutoNewLine;
    private boolean enableOneImagePerLine;
    private String imageFormat;
    private int paintX;
    private int paintY;

    public Builder() {
      this.enableAutoNewLine = false;
      this.fontSize = 14;
      this.imageFormat = "jpeg";
      this.paintX = 5;
      this.paintY = 5;
    }

    public Text2ImageParameter<T> build() {
      return new Text2ImageParameter<T>(this);
    }

    public Builder<T> text(String text) {
      this.text = text;
      return this;
    }

    public Builder<T> font(IFont<T> font) {
      this.font = font;
      return this;
    }

    public Builder<T> width(int width) {
      this.width = width;
      return this;
    }

    public Builder<T> height(int height) {
      this.height = height;
      return this;
    }

    public Builder<T> fontSize(int fontSize) {
      this.fontSize = fontSize;
      return this;
    }

    public Builder<T> enableAutoNewLine(boolean enableAutoNewLine) {
      this.enableAutoNewLine = enableAutoNewLine;
      return this;
    }

    public Builder<T> enableOneImagePerLine(boolean enableOneImagePerLine) {
      this.enableOneImagePerLine = enableOneImagePerLine;
      return this;
    }

    public Builder<T> imageFormat(String imageFormat) {
      this.imageFormat = imageFormat;
      return this;
    }

    public Builder<T> paintX(int paintX) {
      this.paintX = paintX;
      return this;
    }

    public Builder<T> paintY(int paintY) {
      this.paintY = paintY;
      return this;
    }
  }

}
