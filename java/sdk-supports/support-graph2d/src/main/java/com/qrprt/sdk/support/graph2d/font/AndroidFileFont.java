package com.qrprt.sdk.support.graph2d.font;

import android.graphics.Typeface;
import com.qrprt.sdk.support.graph2d.IFont;

import java.io.File;

public class AndroidFileFont implements IFont<Typeface> {

  private final File path;

  public AndroidFileFont(File path) {
    this.path = path;
  }

  @Override
  public Typeface font() throws Exception {
    return Typeface.createFromFile(this.path);
  }
}
