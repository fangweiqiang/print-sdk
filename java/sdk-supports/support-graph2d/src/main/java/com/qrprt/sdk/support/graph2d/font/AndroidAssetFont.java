package com.qrprt.sdk.support.graph2d.font;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import com.qrprt.sdk.support.graph2d.IFont;

public class AndroidAssetFont implements IFont<Typeface> {

  private final AssetManager mgr;
  private final String path;

  public AndroidAssetFont(AssetManager mgr, String path) {
    this.mgr = mgr;
    this.path = path;
  }

  @Override
  public Typeface font() throws Exception {
    return Typeface.createFromAsset(this.mgr, this.path);
  }
}
