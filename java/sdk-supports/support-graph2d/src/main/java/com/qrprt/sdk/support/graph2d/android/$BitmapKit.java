package com.qrprt.sdk.support.graph2d.android;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.stream.Collectors;

public class $BitmapKit {

  public static byte[] binary(Bitmap bitmap, Bitmap.CompressFormat format) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    bitmap.compress(format, 100, stream);
    byte[] byteArray = stream.toByteArray();
    bitmap.recycle();
    return byteArray;
  }

  public static List<byte[]> binarys(List<Bitmap> bitmaps, Bitmap.CompressFormat format) {
    return bitmaps.stream()
      .map(item -> binary(item, format))
      .collect(Collectors.toList());
  }

}
