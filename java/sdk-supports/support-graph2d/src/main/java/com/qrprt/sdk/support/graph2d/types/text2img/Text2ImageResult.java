package com.qrprt.sdk.support.graph2d.types.text2img;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Text2ImageResult implements Serializable {

  private final List<byte[]> images;

  public Text2ImageResult(List<byte[]> images) {
    this.images = images;
  }

  public static Text2ImageResult with(byte[] image) {
    return with(Collections.singletonList(image));
  }

  public static Text2ImageResult with(List<byte[]> images) {
    return new Text2ImageResult(images);
  }

  public static Text2ImageResult with(BufferedImage image, String format) throws IOException {
    return with(Collections.singletonList(image), format);
  }

  public static Text2ImageResult with(List<BufferedImage> images, String format) throws IOException {
    List<byte[]> binaries = new ArrayList<>(images.size());
    for (BufferedImage image : images) {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ImageIO.write(image, format, baos);
      byte[] bytes = baos.toByteArray();
      binaries.add(bytes);
    }
    return with(binaries);
  }

  public List<byte[]> images() {
    return this.images;
  }

}
