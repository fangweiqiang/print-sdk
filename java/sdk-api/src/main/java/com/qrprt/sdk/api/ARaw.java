package com.qrprt.sdk.api;

import java.nio.charset.Charset;

/**
 * 直传文字指令
 */
public class ARaw implements CArg {

  private final String string;
  private final Charset charset;
  private final byte[] binary;
  private final boolean newline;
  private final Mode mode;


  private ARaw(Builder builder) {
    this.string = builder.string;
    this.charset = builder.charset;
    this.binary = builder.binary;
    this.newline = builder.newline;
    this.mode = builder.mode;
  }

  public static Builder builder() {
    return new Builder();
  }

  public byte[] binary() {
    return this.binary;
  }

  public boolean newline() {
    return this.newline;
  }

  public String string() {
    return this.string;
  }

  public Charset charset() {
    return this.charset;
  }

  public Mode mode() {
    return this.mode;
  }

  public static class Builder {
    private String string;
    private Charset charset;
    private byte[] binary;
    private boolean newline;
    private Mode mode;

    public Builder() {
    }

    public ARaw build() {
      return new ARaw(this);
    }

    public Builder newline() {
      return this.newline(true);
    }

    public Builder newline(boolean newline) {
      this.newline = newline;
      return this;
    }

    private void mode(Mode mode) {
      if (this.mode == null) {
        this.mode = mode;
        return;
      }
      if (this.mode != mode)
        throw new IllegalArgumentException("Raw command only support one mode. text or binary, can not change");
    }

    public Builder command(String command) {
      return this.command(command, QRConst.CHARSET_GBK);
    }

    public Builder command(String command, Charset charset) {
      this.mode(Mode.TEXT);
      this.string = command;
      this.charset = charset;
      return this;
    }

    public Builder command(byte[] binary) {
      this.mode(Mode.BINARY);
      this.binary = binary;
      return this;
    }
  }

  public enum Mode {
    BINARY,
    TEXT
  }

}
