package com.qrprt.sdk.api;

interface _QRSDK<T extends _QRSDK> {

  String version();

  T raw(ARaw raw);

  /**
   * 支持进行变量替换
   *
   * @param name  变量名
   * @param value 变量值
   * @return QRSDK
   */
  QRSDK variable(String name, Object value);

  /**
   * 获取生成的指令
   *
   * @return Command
   */
  Command command();

  /**
   * 为保证线程安全, 因此建议最终释放资源
   */
  void clear();

}
