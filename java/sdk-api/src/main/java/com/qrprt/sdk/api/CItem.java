package com.qrprt.sdk.api;

import java.nio.charset.Charset;

public class CItem {

  static final String SPLIT = "\r\n";
  private static final byte[] SPLIT_BINARY = SPLIT.getBytes(QRConst.CHARSET_GBK);

  public enum Type {
    TEXT,
    BINARY,
    NEWLINE,
  }

  private int ix;
  private byte[] binary;
  private String text;
  private Charset charset;
  private Type type;

  private CItem(int ix, byte[] binary) {
    this.ix = ix;
    this.binary = binary;
    this.type = Type.BINARY;
  }

  private CItem(int ix, String text, Charset charset) {
    this.ix = ix;
    this.text = text;
    this.charset = charset;
    this.type = Type.TEXT;
  }

  public static CItem text(int ix, String command) {
    return text(ix, command, QRConst.CHARSET_GBK);
  }

  public static CItem text(int ix, String command, Charset charset) {
    return new CItem(ix, command, charset);
  }

  public static CItem binary(int ix, byte[] binary) {
    return new CItem(ix, binary);
  }

  public static CItem newline(int ix) {
    return new CItem(ix, SPLIT_BINARY);
  }

  public int ix() {
    return ix;
  }

  public byte[] binary() {
    return binary;
  }

  public String string() {
    return this.text;
  }

  public Charset charset() {
    return this.charset;
  }

  public Type type() {
    return type;
  }

}

