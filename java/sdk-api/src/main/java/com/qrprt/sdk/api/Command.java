package com.qrprt.sdk.api;

import java.nio.charset.Charset;

/**
 * 返回指令
 */
public interface Command {

//  static CommandBuilder builder() {
//    return new CommandBuilder();
//  }
//
//  static Command create(byte[] binary) {
//    return new Command() {
//      @Override
//      public byte[] binary() {
//        return binary;
//      }
//
//      @Override
//      public String string(Charset charset) {
//        if (binary == null)
//          return null;
//
////        CharsetDecoder decoder = charset.newDecoder();
////        ByteBuffer byteBuffer = ByteBuffer.wrap(binary);
////        CharBuffer charBuffer = CharBuffer.allocate(byteBuffer.limit());
////        decoder.decode(byteBuffer, charBuffer, true);
////        charBuffer.flip();
////        String ret = charBuffer.toString();
////        charBuffer.clear();
////        byteBuffer.clear();
////        return ret;
//        return new String(binary, charset);
//      }
//
//      @Override
//      public String toString() {
//        return this.string(QRConst.CHARSET_PRINTER);
//      }
//    };
//  }


  /**
   * 二进制指令
   *
   * @return byte[]
   */
  byte[] binary();

  /**
   * 字符指令
   *
   * @return string
   */
  String string();

  /**
   * 字符指令
   *
   * @param charset 编码
   * @return string
   */
  String string(Charset charset);

}
