package com.qrprt.sdk.api;

import java.nio.charset.Charset;

public interface QRConst {

  Charset CHARSET_UTF_8 = Charset.forName("UTF-8");

  Charset CHARSET_GBK = Charset.forName("GBK");

}
