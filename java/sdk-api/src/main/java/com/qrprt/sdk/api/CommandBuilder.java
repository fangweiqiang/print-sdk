package com.qrprt.sdk.api;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandBuilder {


  private Map<String, Object> variable;
  private Set<CItem> citems;
  private int ix = 0;

//  private ByteArrayOutputStream commands;

  public CommandBuilder() {
//    this.stacks = new LinkedHashSet<>();
//    this.commands = new ByteArrayOutputStream();
    this.variable = new HashMap<String, Object>();
    this.citems = new LinkedHashSet<CItem>();
  }

  public static CommandBuilder create() {
    return new CommandBuilder();
  }

  public CommandBuilder push(String command) {
    return this.push(command, QRConst.CHARSET_GBK);
  }

  public CommandBuilder push(String command, Charset charset) {
    return this.push(command, charset, Boolean.TRUE);
  }

  public CommandBuilder push(String command, boolean newline) {
    return this.push(command, QRConst.CHARSET_GBK, newline);
  }

  public CommandBuilder push(String command, Charset charset, boolean newline) {
    if (command.startsWith(CItem.SPLIT))
      command = command.substring(CItem.SPLIT.length());
//    if (this.exists(command))
//      return this;
//    this.stacks.add(command);
//    byte[] bytes = command.getBytes(charset);
//    this.commands.write(bytes, 0, bytes.length);
//    if (newline)
//      this.newline();
//    return this;
    this.citems.add(CItem.text(this.ix, command, charset));
    this.ix += 1;
    if (newline) {
      this.newline();
    }
    return this;
  }

  public CommandBuilder push(byte[] binary) {
    return this.push(binary, Boolean.TRUE);
  }

  public CommandBuilder push(byte[] binary, boolean newline) {
//    this.commands.write(binary, 0, binary.length);
//    if (newline)
//      this.newline();
    this.citems.add(CItem.binary(this.ix, binary));
    this.ix += 1;
    if (newline)
      this.newline();
    return this;
  }

  public CommandBuilder newline() {
//    this.commands.write(SPLIT_BINARY, 0, SPLIT_BINARY.length);
//    this.textCommands.add(SPLIT);
    this.citems.add(CItem.newline(this.ix));
    this.ix += 1;
    return this;
  }

  public CommandBuilder variable(String name, Object value) {
    this.variable.put(name, value);
    return this;
  }

  public Command build() {
    ByteArrayOutputStream commands = new ByteArrayOutputStream();
    try {

//      if (this.commands == null)
//        throw new RuntimeException("Can not build command.");
//      byte[] binary = this.commands.toByteArray();

      for (CItem citem : this.citems) {
        switch (citem.type()) {
          case TEXT:
            String text = citem.string();

            if (text.contains("{{") && text.contains("}}")) {
              Pattern pattern = Pattern.compile("[^\\\\]\\{\\{(?<varx>.*?)}}");
              Matcher matcher = pattern.matcher(text);
              while (matcher.find()) {
                String varx = matcher.group("varx");
                Object val = this.variable.get(varx);
                if (val != null) {
                  text = text.replace("{{" + varx + "}}", val.toString());
                }
              }
              text = text.replaceAll("\\\\(\\{\\{.*?}})", "$1");
            }

            byte[] bytes = text.getBytes(citem.charset());
            commands.write(bytes, 0, bytes.length);
            break;
          case NEWLINE:
          case BINARY:
            byte[] binary = citem.binary();
            commands.write(binary, 0, binary.length);
            break;
        }
      }

      final byte[] binary = commands.toByteArray();
      return new Command() {
        @Override
        public byte[] binary() {
          return binary;
        }

        @Override
        public String string() {
          return this.string(QRConst.CHARSET_GBK);
        }

        @Override
        public String string(Charset charset) {
          return new String(binary, charset);
        }
      };
    } finally {
      try {
        commands.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
      this.clear();
    }
  }

  public void clear() {
    try {
//      if (this.stacks != null)
//        this.stacks.clear();
      this.ix = 0;
//      if (this.commands != null) {
//        this.commands.close();
//        this.commands.reset();
//      }
      if (this.citems != null) {
        this.citems.clear();
      }
      if (this.variable != null) {
        this.variable.clear();
      }
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  @Override
  public String toString() {
//    if (this.commands == null)
//      return null;
//    byte[] bytes = this.commands.toByteArray();
//    return new String(bytes, QRConst.CHARSET_PRINTER);

    StringBuilder builder = new StringBuilder();
    for (CItem citem : this.citems) {
      switch (citem.type()) {
        case TEXT:
          String text = citem.string();
          for (String var : this.variable.keySet()) {
            text = text.replaceAll("\\{\\{" + var + "}}", this.variable.get(var).toString());
          }
          builder.append(text);
          break;
        case NEWLINE:
          builder.append(CItem.SPLIT);
          break;
        case BINARY:
          builder.append("[BINARY]");
          break;
      }
    }
    return builder.toString();
  }

}
